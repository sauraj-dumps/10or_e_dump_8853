## msm8937_64-user 7.1.2 N2G47H 2545 release-keys
- Manufacturer: 10or
- Platform: msm8937
- Codename: E
- Brand: 10or
- Flavor: msm8937_64-user
- Release Version: 7.1.2
- Id: N2G47H
- Incremental: 2545
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-IN
- Screen Density: undefined
- Fingerprint: 10or/E/E:7.1.2/N2G47H/2545:user/release-keys
- OTA version: 
- Branch: msm8937_64-user-7.1.2-N2G47H-2545-release-keys
- Repo: 10or_e_dump_8853


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
