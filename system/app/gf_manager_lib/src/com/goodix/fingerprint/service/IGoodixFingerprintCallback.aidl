package com.goodix.fingerprint.service;

interface IGoodixFingerprintCallback {
    void onTestCmd(int cmdId, in byte[] result);
    void onHbdData(int heartBeatRate, int status, in int[] displayData);
    void onAuthenticateFido(int fingerId, in byte[] uvt);
}
