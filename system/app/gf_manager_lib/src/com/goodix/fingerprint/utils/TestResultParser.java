
package com.goodix.fingerprint.utils;

import java.util.Arrays;
import java.util.HashMap;

import android.util.Log;

public class TestResultParser {
    private static final String TAG = "TestResultParser";

    public static final int TEST_TOKEN_ERROR_CODE = 100;
    public static final int TEST_TOKEN_CHIP_TYPE = TEST_TOKEN_ERROR_CODE + 1;
    public static final int TEST_TOKEN_CHIP_SERIES = TEST_TOKEN_CHIP_TYPE + 1;

    public static final int TEST_TOKEN_ALGO_VERSION = 200;
    public static final int TEST_TOKEN_PREPROCESS_VERSION = TEST_TOKEN_ALGO_VERSION + 1;
    public static final int TEST_TOKEN_FW_VERSION = TEST_TOKEN_PREPROCESS_VERSION + 1;
    public static final int TEST_TOKEN_TEE_VERSION = TEST_TOKEN_FW_VERSION + 1;
    public static final int TEST_TOKEN_TA_VERSION = TEST_TOKEN_TEE_VERSION + 1;
    public static final int TEST_TOKEN_CHIP_ID = TEST_TOKEN_TA_VERSION + 1;
    public static final int TEST_TOKEN_VENDOR_ID = TEST_TOKEN_CHIP_ID + 1;
    public static final int TEST_TOKEN_SENSOR_ID = TEST_TOKEN_VENDOR_ID + 1;
    public static final int TEST_TOKEN_PRODUCTION_DATE = TEST_TOKEN_SENSOR_ID + 1;
    public static final int TEST_TOKEN_SENSOR_OTP_TYPE = TEST_TOKEN_PRODUCTION_DATE + 1;
    public static final int TEST_TOKEN_CODE_FW_VERSION = TEST_TOKEN_SENSOR_OTP_TYPE + 1;

    public static final int TEST_TOKEN_AVG_DIFF_VAL = 300;
    public static final int TEST_TOKEN_NOISE = TEST_TOKEN_AVG_DIFF_VAL + 1;

    public static final int TEST_TOKEN_BAD_PIXEL_NUM = TEST_TOKEN_NOISE + 1;
    public static final int TEST_TOKEN_FDT_BAD_AREA_NUM = TEST_TOKEN_BAD_PIXEL_NUM + 1;
    public static final int TEST_TOKEN_LOCAL_BAD_PIXEL_NUM = TEST_TOKEN_FDT_BAD_AREA_NUM + 1;

    public static final int TEST_TOKEN_FRAME_NUM = TEST_TOKEN_LOCAL_BAD_PIXEL_NUM + 1;
    public static final int TEST_TOKEN_MAX_FRAME_NUM = TEST_TOKEN_FRAME_NUM + 1;
    public static final int TEST_TOKEN_DATA_DEVIATION_DIFF = TEST_TOKEN_MAX_FRAME_NUM + 1;

    public static final int TEST_TOKEN_ALL_TILT_ANGLE = TEST_TOKEN_DATA_DEVIATION_DIFF + 1;
    public static final int TEST_TOKEN_BLOCK_TILT_ANGLE_MAX = TEST_TOKEN_ALL_TILT_ANGLE + 1;
    public static final int TEST_TOKEN_LOCAL_WORST = TEST_TOKEN_BLOCK_TILT_ANGLE_MAX + 1;
    public static final int TEST_TOKEN_SINGULAR = TEST_TOKEN_LOCAL_WORST + 1;
    public static final int TEST_TOKEN_IN_CIRCLE = TEST_TOKEN_SINGULAR + 1;
    public static final int TEST_TOKEN_BIG_BUBBLE = TEST_TOKEN_IN_CIRCLE + 1;
    public static final int TEST_TOKEN_LINE = TEST_TOKEN_BIG_BUBBLE + 1;
    public static final int TEST_TOKEN_LOCAL_SMALL_BAD_PIXEL_NUM = TEST_TOKEN_LINE +1;
    public static final int TEST_TOKEN_LOCAL_BIG_BAD_PIXEL_NUM = TEST_TOKEN_LOCAL_SMALL_BAD_PIXEL_NUM + 1;
    public static final int TEST_TOKEN_FLATNESS_BAD_PIXEL_NUM = TEST_TOKEN_LOCAL_BIG_BAD_PIXEL_NUM + 1;
    public static final int TEST_TOKEN_IS_BAD_LINE = TEST_TOKEN_FLATNESS_BAD_PIXEL_NUM + 1;

    public static final int TEST_TOKEN_GET_DR_TIMESTAMP_TIME = 400;
    public static final int TEST_TOKEN_GET_MODE_TIME = TEST_TOKEN_GET_DR_TIMESTAMP_TIME + 1;
    public static final int TEST_TOKEN_GET_CHIP_ID_TIME = TEST_TOKEN_GET_MODE_TIME + 1;
    public static final int TEST_TOKEN_GET_VENDOR_ID_TIME = TEST_TOKEN_GET_CHIP_ID_TIME + 1;
    public static final int TEST_TOKEN_GET_SENSOR_ID_TIME = TEST_TOKEN_GET_VENDOR_ID_TIME + 1;
    public static final int TEST_TOKEN_GET_FW_VERSION_TIME = TEST_TOKEN_GET_SENSOR_ID_TIME + 1;
    public static final int TEST_TOKEN_GET_IMAGE_TIME = TEST_TOKEN_GET_FW_VERSION_TIME + 1;
    public static final int TEST_TOKEN_RAW_DATA_LEN = TEST_TOKEN_GET_IMAGE_TIME + 1;
    public static final int TEST_TOKEN_CFG_DATA = TEST_TOKEN_RAW_DATA_LEN + 1;
    public static final int TEST_TOKEN_CFG_DATA_LEN = TEST_TOKEN_CFG_DATA + 1;
    public static final int TEST_TOKEN_FW_DATA = TEST_TOKEN_CFG_DATA_LEN + 1;
    public static final int TEST_TOKEN_FW_DATA_LEN = TEST_TOKEN_FW_DATA + 1;

    public static final int TEST_TOKEN_IMAGE_QUALITY = 500;
    public static final int TEST_TOKEN_VALID_AREA = TEST_TOKEN_IMAGE_QUALITY + 1;
    public static final int TEST_TOKEN_KEY_POINT_NUM = TEST_TOKEN_VALID_AREA + 1;
    public static final int TEST_TOKEN_INCREATE_RATE = TEST_TOKEN_KEY_POINT_NUM + 1;
    public static final int TEST_TOKEN_OVERLAY = TEST_TOKEN_INCREATE_RATE + 1;
    public static final int TEST_TOKEN_GET_RAW_DATA_TIME = TEST_TOKEN_OVERLAY + 1;
    public static final int TEST_TOKEN_PREPROCESS_TIME = TEST_TOKEN_GET_RAW_DATA_TIME + 1;
    public static final int TEST_TOKEN_ALGO_START_TIME = TEST_TOKEN_PREPROCESS_TIME + 1;
    public static final int TEST_TOKEN_GET_FEATURE_TIME = TEST_TOKEN_ALGO_START_TIME + 1;
    public static final int TEST_TOKEN_ENROLL_TIME = TEST_TOKEN_GET_FEATURE_TIME + 1;
    public static final int TEST_TOKEN_AUTHENTICATE_TIME = TEST_TOKEN_ENROLL_TIME + 1;
    public static final int TEST_TOKEN_AUTHENTICATE_ID = TEST_TOKEN_AUTHENTICATE_TIME + 1;
    public static final int TEST_TOKEN_AUTHENTICATE_UPDATE_FLAG = TEST_TOKEN_AUTHENTICATE_ID + 1;
    public static final int TEST_TOKEN_AUTHENTICATE_FINGER_COUNT = TEST_TOKEN_AUTHENTICATE_UPDATE_FLAG + 1;
    public static final int TEST_TOKEN_AUTHENTICATE_FINGER_ITME = TEST_TOKEN_AUTHENTICATE_FINGER_COUNT + 1;
    public static final int TEST_TOKEN_TOTAL_TIME = TEST_TOKEN_AUTHENTICATE_FINGER_ITME + 1;
    public static final int TEST_TOKEN_GET_GSC_DATA_TIME = TEST_TOKEN_TOTAL_TIME + 1;
    public static final int TEST_TOKEN_BIO_ASSAY_TIME = TEST_TOKEN_GET_GSC_DATA_TIME + 1;

    public static final int TEST_TOKEN_RESET_FLAG = 600;

    public static final int TEST_TOKEN_RAW_DATA = 700;
    public static final int TEST_TOKEN_BMP_DATA = TEST_TOKEN_RAW_DATA + 1;
    public static final int TEST_TOKEN_ALGO_INDEX = TEST_TOKEN_BMP_DATA + 1;
    public static final int TEST_TOKEN_SAFE_CLASS = TEST_TOKEN_ALGO_INDEX + 1;
    public static final int TEST_TOKEN_TEMPLATE_COUNT = TEST_TOKEN_SAFE_CLASS + 1;
    public static final int TEST_TOKEN_GSC_DATA = TEST_TOKEN_TEMPLATE_COUNT + 1;
    public static final int TEST_TOKEN_HBD_BASE = TEST_TOKEN_GSC_DATA + 1;
    public static final int TEST_TOKEN_HBD_AVG = TEST_TOKEN_HBD_BASE + 1;
    public static final int TEST_TOKEN_HBD_RAW_DATA = TEST_TOKEN_HBD_AVG + 1;
    public static final int TEST_TOKEN_ELECTRICITY_VALUE = TEST_TOKEN_HBD_RAW_DATA + 1;
    public static final int TEST_TOKEN_FINGER_EVENT = TEST_TOKEN_ELECTRICITY_VALUE + 1;
    public static final int TEST_TOKEN_GSC_FLAG = TEST_TOKEN_FINGER_EVENT + 1;

    public static final int TEST_TOKEN_MAX_FINGERS = 800;
    public static final int TEST_TOKEN_MAX_FINGERS_PER_USER = TEST_TOKEN_MAX_FINGERS + 1;
    public static final int TEST_TOKEN_SUPPORT_KEY_MODE = TEST_TOKEN_MAX_FINGERS_PER_USER + 1;
    public static final int TEST_TOKEN_SUPPORT_FF_MODE = TEST_TOKEN_SUPPORT_KEY_MODE + 1;
    public static final int TEST_TOKEN_SUPPORT_POWER_KEY_FEATURE = TEST_TOKEN_SUPPORT_FF_MODE + 1;
    public static final int TEST_TOKEN_FORBIDDEN_UNTRUSTED_ENROLL = TEST_TOKEN_SUPPORT_POWER_KEY_FEATURE + 1;
    public static final int TEST_TOKEN_FORBIDDEN_ENROLL_DUPLICATE_FINGERS = TEST_TOKEN_FORBIDDEN_UNTRUSTED_ENROLL + 1;
    public static final int TEST_TOKEN_SUPPORT_BIO_ASSAY = TEST_TOKEN_FORBIDDEN_ENROLL_DUPLICATE_FINGERS + 1;
    public static final int TEST_TOKEN_SUPPORT_PERFORMANCE_DUMP = TEST_TOKEN_SUPPORT_BIO_ASSAY + 1;
    public static final int TEST_TOKEN_SUPPORT_NAV_MODE = TEST_TOKEN_SUPPORT_PERFORMANCE_DUMP + 1;
    public static final int TEST_TOKEN_NAV_DOUBLE_CLICK_TIME = TEST_TOKEN_SUPPORT_NAV_MODE + 1;
    public static final int TEST_TOKEN_NAV_LONG_PRESS_TIME = TEST_TOKEN_NAV_DOUBLE_CLICK_TIME + 1;
    public static final int TEST_TOKEN_ENROLLING_MIN_TEMPLATES = TEST_TOKEN_NAV_LONG_PRESS_TIME + 1;
    public static final int TEST_TOKEN_VALID_IMAGE_QUALITY_THRESHOLD = TEST_TOKEN_ENROLLING_MIN_TEMPLATES + 1;
    public static final int TEST_TOKEN_VALID_IMAGE_AREA_THRESHOLD = TEST_TOKEN_VALID_IMAGE_QUALITY_THRESHOLD + 1;
    public static final int TEST_TOKEN_DUPLICATE_FINGER_OVERLAY_SCORE = TEST_TOKEN_VALID_IMAGE_AREA_THRESHOLD + 1;
    public static final int TEST_TOKEN_INCREASE_RATE_BETWEEN_STITCH_INFO = TEST_TOKEN_DUPLICATE_FINGER_OVERLAY_SCORE + 1;
    public static final int TEST_TOKEN_SCREEN_ON_AUTHENTICATE_FAIL_RETRY_COUNT = TEST_TOKEN_INCREASE_RATE_BETWEEN_STITCH_INFO + 1;
    public static final int TEST_TOKEN_SCREEN_OFF_AUTHENTICATE_FAIL_RETRY_COUNT = TEST_TOKEN_SCREEN_ON_AUTHENTICATE_FAIL_RETRY_COUNT + 1;
    public static final int TEST_TOKEN_AUTHENTICATE_ORDER = TEST_TOKEN_SCREEN_OFF_AUTHENTICATE_FAIL_RETRY_COUNT + 1;
    public static final int TEST_TOKEN_REISSUE_KEY_DOWN_WHEN_ENTRY_FF_MODE = TEST_TOKEN_AUTHENTICATE_ORDER + 1;
    public static final int TEST_TOKEN_REISSUE_KEY_DOWN_WHEN_ENTRY_IMAGE_MODE = TEST_TOKEN_REISSUE_KEY_DOWN_WHEN_ENTRY_FF_MODE + 1;
    public static final int TEST_TOKEN_SUPPORT_SENSOR_BROKEN_CHECK = TEST_TOKEN_REISSUE_KEY_DOWN_WHEN_ENTRY_IMAGE_MODE + 1;
    public static final int TEST_TOKEN_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_SENSOR = TEST_TOKEN_SUPPORT_SENSOR_BROKEN_CHECK + 1;
    public static final int TEST_TOKEN_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_STUDY = TEST_TOKEN_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_SENSOR + 1;
    public static final int TEST_TOKEN_BAD_POINT_TEST_MAX_FRAME_NUMBER = TEST_TOKEN_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_STUDY + 1;
    public static final int TEST_TOKEN_REPORT_KEY_EVENT_ONLY_ENROLL_AUTHENTICATE = TEST_TOKEN_BAD_POINT_TEST_MAX_FRAME_NUMBER + 1;
    public static final int TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_IMAGE_MODE = TEST_TOKEN_REPORT_KEY_EVENT_ONLY_ENROLL_AUTHENTICATE + 1;
    public static final int TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_FF_MODE = TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_IMAGE_MODE + 1;
    public static final int TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_KEY_MODE = TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_FF_MODE + 1;
    public static final int TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_NAV_MODE = TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_KEY_MODE + 1;
    public static final int TEST_TOKEN_SUPPORT_SET_SPI_SPEED_IN_TEE = TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_NAV_MODE + 1;
    public static final int TEST_TOKEN_SUPPORT_FRR_ANALYSIS = TEST_TOKEN_SUPPORT_SET_SPI_SPEED_IN_TEE + 1;

    public static final int TEST_TOKEN_SENSOR_VALIDITY = 900;

    public static final int TEST_TOKEN_DUMP_IS_ENCRYPTED = 1000;
    public static final int TEST_TOKEN_DUMP_ENCRYPTED_DATA = TEST_TOKEN_DUMP_IS_ENCRYPTED + 1;
    public static final int TEST_TOKEN_DUMP_OPERATION = TEST_TOKEN_DUMP_ENCRYPTED_DATA + 1;
    public static final int TEST_TOKEN_DUMP_TIMESTAMP = TEST_TOKEN_DUMP_OPERATION + 1;
    public static final int TEST_TOKEN_DUMP_YEAR = TEST_TOKEN_DUMP_TIMESTAMP + 1;
    public static final int TEST_TOKEN_DUMP_MONTH = TEST_TOKEN_DUMP_YEAR + 1;
    public static final int TEST_TOKEN_DUMP_DAY = TEST_TOKEN_DUMP_MONTH + 1;
    public static final int TEST_TOKEN_DUMP_HOUR = TEST_TOKEN_DUMP_DAY + 1;
    public static final int TEST_TOKEN_DUMP_MINUTE = TEST_TOKEN_DUMP_HOUR + 1;
    public static final int TEST_TOKEN_DUMP_SECOND = TEST_TOKEN_DUMP_MINUTE + 1;
    public static final int TEST_TOKEN_DUMP_MICROSECOND = TEST_TOKEN_DUMP_SECOND + 1;
    public static final int TEST_TOKEN_DUMP_VERSION_CODE = TEST_TOKEN_DUMP_MICROSECOND + 1;
    public static final int TEST_TOKEN_DUMP_WIDTH = TEST_TOKEN_DUMP_VERSION_CODE + 1;
    public static final int TEST_TOKEN_DUMP_HEIGHT = TEST_TOKEN_DUMP_WIDTH + 1;
    public static final int TEST_TOKEN_DUMP_PREPROCESS_VERSION = TEST_TOKEN_DUMP_HEIGHT + 1;
    public static final int TEST_TOKEN_DUMP_CHIP_ID = TEST_TOKEN_DUMP_PREPROCESS_VERSION + 1;
    public static final int TEST_TOKEN_DUMP_VENDOR_ID = TEST_TOKEN_DUMP_CHIP_ID + 1;
    public static final int TEST_TOKEN_DUMP_SENSOR_ID = TEST_TOKEN_DUMP_VENDOR_ID + 1;
    public static final int TEST_TOKEN_DUMP_FRAME_NUM = TEST_TOKEN_DUMP_SENSOR_ID + 1;
    public static final int TEST_TOKEN_DUMP_KR = TEST_TOKEN_DUMP_FRAME_NUM + 1;
    public static final int TEST_TOKEN_DUMP_B = TEST_TOKEN_DUMP_KR + 1;
    public static final int TEST_TOKEN_DUMP_RAW_DATA = TEST_TOKEN_DUMP_B + 1;
    public static final int TEST_TOKEN_DUMP_BROKEN_CHECK_RAW_DATA = TEST_TOKEN_DUMP_RAW_DATA + 1;
    public static final int TEST_TOKEN_DUMP_BROKEN_CHECK_FRAME_NUM = TEST_TOKEN_DUMP_BROKEN_CHECK_RAW_DATA + 1;
    public static final int TEST_TOKEN_DUMP_CALI_RES = TEST_TOKEN_DUMP_BROKEN_CHECK_FRAME_NUM + 1;
    public static final int TEST_TOKEN_DUMP_DATA_BMP = TEST_TOKEN_DUMP_CALI_RES + 1;
    public static final int TEST_TOKEN_DUMP_SITO_BMP = TEST_TOKEN_DUMP_DATA_BMP + 1;
    public static final int TEST_TOKEN_DUMP_SELECT_INDEX = TEST_TOKEN_DUMP_SITO_BMP + 1;
    public static final int TEST_TOKEN_DUMP_IMAGE_QUALITY = TEST_TOKEN_DUMP_SELECT_INDEX + 1;
    public static final int TEST_TOKEN_DUMP_VALID_AREA = TEST_TOKEN_DUMP_IMAGE_QUALITY + 1;
    public static final int TEST_TOKEN_DUMP_INCREASE_RATE_BETWEEN_STITCH_INFO = TEST_TOKEN_DUMP_VALID_AREA + 1;
    public static final int TEST_TOKEN_DUMP_OVERLAP_RATE_BETWEEN_LAST_TEMPLATE = TEST_TOKEN_DUMP_INCREASE_RATE_BETWEEN_STITCH_INFO + 1;
    public static final int TEST_TOKEN_DUMP_ENROLLING_FINGER_ID = TEST_TOKEN_DUMP_OVERLAP_RATE_BETWEEN_LAST_TEMPLATE + 1;
    public static final int TEST_TOKEN_DUMP_DUMPLICATED_FINGER_ID = TEST_TOKEN_DUMP_ENROLLING_FINGER_ID + 1;
    public static final int TEST_TOKEN_DUMP_MATCH_SCORE = TEST_TOKEN_DUMP_DUMPLICATED_FINGER_ID + 1;
    public static final int TEST_TOKEN_DUMP_MATCH_FINGER_ID = TEST_TOKEN_DUMP_MATCH_SCORE + 1;
    public static final int TEST_TOKEN_DUMP_STUDY_FLAG = TEST_TOKEN_DUMP_MATCH_FINGER_ID + 1;
    public static final int TEST_TOKEN_DUMP_NAV_TIMES = TEST_TOKEN_DUMP_STUDY_FLAG + 1;
    public static final int TEST_TOKEN_DUMP_NAV_FRAME_INDEX = TEST_TOKEN_DUMP_NAV_TIMES + 1;
    public static final int TEST_TOKEN_DUMP_NAV_FRAME_NUM = TEST_TOKEN_DUMP_NAV_FRAME_INDEX + 1;
    public static final int TEST_TOKEN_DUMP_NAV_FRAME_COUNT = TEST_TOKEN_DUMP_NAV_FRAME_NUM + 1;
    public static final int TEST_TOKEN_DUMP_FINGER_ID = TEST_TOKEN_DUMP_NAV_FRAME_COUNT + 1;
    public static final int TEST_TOKEN_DUMP_GROUP_ID = TEST_TOKEN_DUMP_FINGER_ID + 1;
    public static final int TEST_TOKEN_DUMP_TEMPLATE = TEST_TOKEN_DUMP_GROUP_ID + 1;
    public static final int TEST_TOKEN_DUMP_REMAINING_TEMPLATES = TEST_TOKEN_DUMP_TEMPLATE + 1;

    public static final int TEST_TOKEN_SPI_RW_CMD = 1100;
    public static final int TEST_TOKEN_SPI_RW_START_ADDR = TEST_TOKEN_SPI_RW_CMD + 1;
    public static final int TEST_TOKEN_SPI_RW_LENGTH =  TEST_TOKEN_SPI_RW_START_ADDR + 1;
    public static final int TEST_TOKEN_SPI_RW_CONTENT = TEST_TOKEN_SPI_RW_LENGTH + 1;

    public static final int TEST_TOKEN_PACKAGE_VERSION = 1200;
    public static final int TEST_TOKEN_PROTOCOL_VERSION = TEST_TOKEN_PACKAGE_VERSION + 1;
    public static final int TEST_TOKEN_CHIP_SUPPORT_BIO = TEST_TOKEN_PROTOCOL_VERSION + 1;
    public static final int TEST_TOKEN_IS_BIO_ENABLE = TEST_TOKEN_CHIP_SUPPORT_BIO + 1;
    public static final int TEST_TOKEN_AUTHENTICATED_WITH_BIO_SUCCESS_COUNT = TEST_TOKEN_IS_BIO_ENABLE + 1;
    public static final int TEST_TOKEN_AUTHENTICATED_WITH_BIO_FAILED_COUNT = TEST_TOKEN_AUTHENTICATED_WITH_BIO_SUCCESS_COUNT + 1;
    public static final int TEST_TOKEN_AUTHENTICATED_SUCCESS_COUNT = TEST_TOKEN_AUTHENTICATED_WITH_BIO_FAILED_COUNT + 1;
    public static final int TEST_TOKEN_AUTHENTICATED_FAILED_COUNT = TEST_TOKEN_AUTHENTICATED_SUCCESS_COUNT + 1;
    public static final int TEST_TOKEN_BUF_FULL = TEST_TOKEN_AUTHENTICATED_FAILED_COUNT + 1;
    public static final int TEST_TOKEN_UPDATE_POS = TEST_TOKEN_BUF_FULL + 1;
    public static final int TEST_TOKEN_METADATA = TEST_TOKEN_UPDATE_POS + 1;

    public static final int TEST_PARAM_TOKEN_FW_DATA = 5000;
    public static final int TEST_PARAM_TOKEN_CFG_DATA = TEST_PARAM_TOKEN_FW_DATA + 1;

    public static final int TEST_PARAM_TOKEN_DUMP_PATH = 5100;

    public static long decodeInt64(byte[] result, int offset) {
        return (result[offset] & 0xff) | ((long)(result[offset + 1] & 0xff) << 8)
                | ((long)(result[offset + 2] & 0xff) << 16)
                | ((long)(result[offset + 3] & 0xff) << 24)
                | ((long)(result[offset + 4] & 0xff) << 32)
                | ((long)(result[offset + 5] & 0xff) << 40)
                | ((long)(result[offset + 6] & 0xff) << 48)
                | ((long)(result[offset + 7] & 0xff) << 56);
    }

    public static int decodeInt32(byte[] result, int offset) {
        return (result[offset] & 0xff) | ((result[offset + 1] & 0xff) << 8)
                | ((result[offset + 2] & 0xff) << 16)
                | ((result[offset + 3] & 0xff) << 24);
    }

    private static short decodeInt16(byte[] result, int offset) {
        return (short) ((result[offset] & 0xff) | ((result[offset + 1] & 0xff) << 8));
    }

    private static byte decodeInt8(byte[] result, int offset) {
        return result[offset];
    }

    private static float decodeFloat(byte[] result, int offset, int size) {
        int value = (result[offset] & 0xff) | ((result[offset + 1] & 0xff) << 8)
                | ((result[offset + 2] & 0xff) << 16)
                | ((result[offset + 3] & 0xff) << 24);
        return Float.intBitsToFloat(value);
    }

    private static double decodeDouble(byte[] result, int offset, int size) {
        long value = (result[offset] & 0xff)
                | (((long) result[offset + 1] & 0xff) << 8)
                | (((long) result[offset + 2] & 0xff) << 16)
                | (((long) result[offset + 3] & 0xff) << 24)
                | (((long) result[offset + 4] & 0xff) << 32)
                | (((long) result[offset + 5] & 0xff) << 40)
                | (((long) result[offset + 6] & 0xff) << 48)
                | (((long) result[offset + 7] & 0xff) << 56);
        return Double.longBitsToDouble(value);
    }

    public static HashMap<Integer, Object> parser(byte[] result) {
        HashMap<Integer, Object> testResult = new HashMap<Integer, Object>();

        int len = 0;
        if (result != null) {
            len = result.length;
        }
        for (int offset = 0; offset < len;) {
            Log.d(TAG, "offset = " + offset);
            int token = decodeInt32(result, offset);
            offset += 4;
            Log.d(TAG, "token = " + token);

            switch (token) {
                case TEST_TOKEN_ERROR_CODE:
                case TEST_TOKEN_BAD_PIXEL_NUM:
                case TEST_TOKEN_FDT_BAD_AREA_NUM:
                case TEST_TOKEN_LOCAL_BAD_PIXEL_NUM:
                case TEST_TOKEN_GET_DR_TIMESTAMP_TIME:
                case TEST_TOKEN_GET_MODE_TIME:
                case TEST_TOKEN_GET_CHIP_ID_TIME:
                case TEST_TOKEN_GET_VENDOR_ID_TIME:
                case TEST_TOKEN_GET_SENSOR_ID_TIME:
                case TEST_TOKEN_GET_FW_VERSION_TIME:
                case TEST_TOKEN_GET_IMAGE_TIME:
                case TEST_TOKEN_RAW_DATA_LEN:
                case TEST_TOKEN_IMAGE_QUALITY:
                case TEST_TOKEN_VALID_AREA:
                case TEST_TOKEN_GSC_FLAG:
                case TEST_TOKEN_KEY_POINT_NUM:
                case TEST_TOKEN_INCREATE_RATE:
                case TEST_TOKEN_OVERLAY:
                case TEST_TOKEN_GET_RAW_DATA_TIME:
                case TEST_TOKEN_PREPROCESS_TIME:
                case TEST_TOKEN_ALGO_START_TIME:
                case TEST_TOKEN_GET_FEATURE_TIME:
                case TEST_TOKEN_ENROLL_TIME:
                case TEST_TOKEN_AUTHENTICATE_TIME:
                case TEST_TOKEN_AUTHENTICATE_ID:
                case TEST_TOKEN_AUTHENTICATE_UPDATE_FLAG:
                case TEST_TOKEN_AUTHENTICATE_FINGER_COUNT:
                case TEST_TOKEN_AUTHENTICATE_FINGER_ITME:
                case TEST_TOKEN_TOTAL_TIME:
                case TEST_TOKEN_RESET_FLAG:
                case TEST_TOKEN_SINGULAR:
                case TEST_TOKEN_CHIP_TYPE:
                case TEST_TOKEN_CHIP_SERIES:
                case TEST_TOKEN_MAX_FINGERS:
                case TEST_TOKEN_MAX_FINGERS_PER_USER:
                case TEST_TOKEN_SUPPORT_KEY_MODE:
                case TEST_TOKEN_SUPPORT_FF_MODE:
                case TEST_TOKEN_SUPPORT_POWER_KEY_FEATURE:
                case TEST_TOKEN_FORBIDDEN_UNTRUSTED_ENROLL:
                case TEST_TOKEN_FORBIDDEN_ENROLL_DUPLICATE_FINGERS:
                case TEST_TOKEN_SUPPORT_BIO_ASSAY:
                case TEST_TOKEN_SUPPORT_PERFORMANCE_DUMP:
                case TEST_TOKEN_SUPPORT_NAV_MODE:
                case TEST_TOKEN_NAV_DOUBLE_CLICK_TIME:
                case TEST_TOKEN_NAV_LONG_PRESS_TIME:
                case TEST_TOKEN_ENROLLING_MIN_TEMPLATES:
                case TEST_TOKEN_VALID_IMAGE_QUALITY_THRESHOLD:
                case TEST_TOKEN_VALID_IMAGE_AREA_THRESHOLD:
                case TEST_TOKEN_DUPLICATE_FINGER_OVERLAY_SCORE:
                case TEST_TOKEN_INCREASE_RATE_BETWEEN_STITCH_INFO:
                case TEST_TOKEN_SCREEN_ON_AUTHENTICATE_FAIL_RETRY_COUNT:
                case TEST_TOKEN_SCREEN_OFF_AUTHENTICATE_FAIL_RETRY_COUNT:
                case TEST_TOKEN_AUTHENTICATE_ORDER:
                case TEST_TOKEN_REISSUE_KEY_DOWN_WHEN_ENTRY_FF_MODE:
                case TEST_TOKEN_REISSUE_KEY_DOWN_WHEN_ENTRY_IMAGE_MODE:
                case TEST_TOKEN_SUPPORT_SENSOR_BROKEN_CHECK:
                case TEST_TOKEN_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_SENSOR:
                case TEST_TOKEN_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_STUDY:
                case TEST_TOKEN_BAD_POINT_TEST_MAX_FRAME_NUMBER:
                case TEST_TOKEN_REPORT_KEY_EVENT_ONLY_ENROLL_AUTHENTICATE:
                case TEST_TOKEN_SUPPORT_FRR_ANALYSIS:
                case TEST_TOKEN_GET_GSC_DATA_TIME:
                case TEST_TOKEN_BIO_ASSAY_TIME:
                case TEST_TOKEN_SENSOR_VALIDITY:
                case TEST_TOKEN_ALGO_INDEX:
                case TEST_TOKEN_SAFE_CLASS:
                case TEST_TOKEN_TEMPLATE_COUNT:
                case TEST_TOKEN_ELECTRICITY_VALUE:
                case TEST_TOKEN_FINGER_EVENT:
                case TEST_TOKEN_LOCAL_SMALL_BAD_PIXEL_NUM:
                case TEST_TOKEN_LOCAL_BIG_BAD_PIXEL_NUM:
                case TEST_TOKEN_FLATNESS_BAD_PIXEL_NUM:
                case TEST_TOKEN_DUMP_IS_ENCRYPTED:
                case TEST_TOKEN_DUMP_OPERATION:
                case TEST_TOKEN_DUMP_YEAR:
                case TEST_TOKEN_DUMP_MONTH:
                case TEST_TOKEN_DUMP_DAY:
                case TEST_TOKEN_DUMP_HOUR:
                case TEST_TOKEN_DUMP_MINUTE:
                case TEST_TOKEN_DUMP_SECOND:
                case TEST_TOKEN_DUMP_MICROSECOND:
                case TEST_TOKEN_DUMP_VERSION_CODE:
                case TEST_TOKEN_DUMP_WIDTH:
                case TEST_TOKEN_DUMP_HEIGHT:
                case TEST_TOKEN_DUMP_FRAME_NUM:
                case TEST_TOKEN_DUMP_BROKEN_CHECK_FRAME_NUM:
                case TEST_TOKEN_DUMP_SELECT_INDEX:
                case TEST_TOKEN_DUMP_IMAGE_QUALITY:
                case TEST_TOKEN_DUMP_VALID_AREA:
                case TEST_TOKEN_DUMP_INCREASE_RATE_BETWEEN_STITCH_INFO:
                case TEST_TOKEN_DUMP_OVERLAP_RATE_BETWEEN_LAST_TEMPLATE:
                case TEST_TOKEN_DUMP_ENROLLING_FINGER_ID:
                case TEST_TOKEN_DUMP_DUMPLICATED_FINGER_ID:
                case TEST_TOKEN_DUMP_MATCH_SCORE:
                case TEST_TOKEN_DUMP_MATCH_FINGER_ID:
                case TEST_TOKEN_DUMP_STUDY_FLAG:
                case TEST_TOKEN_DUMP_NAV_TIMES:
                case TEST_TOKEN_DUMP_NAV_FRAME_INDEX:
                case TEST_TOKEN_DUMP_NAV_FRAME_COUNT:
                case TEST_TOKEN_DUMP_FINGER_ID:
                case TEST_TOKEN_DUMP_GROUP_ID:
                case TEST_TOKEN_DUMP_REMAINING_TEMPLATES:
                case TEST_TOKEN_SPI_RW_CMD:
                case TEST_TOKEN_SPI_RW_START_ADDR:
                case TEST_TOKEN_SPI_RW_LENGTH:
                case TEST_TOKEN_FW_DATA_LEN:
                case TEST_TOKEN_CFG_DATA_LEN:
                case TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_IMAGE_MODE:
                case TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_FF_MODE:
                case TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_KEY_MODE:
                case TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_NAV_MODE:
                case TEST_TOKEN_SUPPORT_SET_SPI_SPEED_IN_TEE: 
                case TEST_TOKEN_FRAME_NUM:
                case TEST_TOKEN_MAX_FRAME_NUM: {
                    int value = decodeInt32(result, offset);
                    offset += 4;
                    testResult.put(token, value);
                    Log.d(TAG, "value = " + value);
                    break;
                }

                case TEST_TOKEN_ALGO_VERSION:
                case TEST_TOKEN_PREPROCESS_VERSION:
                case TEST_TOKEN_FW_VERSION:
                case TEST_TOKEN_TEE_VERSION:
                case TEST_TOKEN_TA_VERSION:
                case TEST_TOKEN_CODE_FW_VERSION:
                case TEST_TOKEN_DUMP_PREPROCESS_VERSION: {
                    int size = decodeInt32(result, offset);
                    offset += 4;
                    testResult.put(token, new String(result, offset, size));
                    offset += size;
                    break;
                }

                case TEST_TOKEN_CHIP_ID:
                case TEST_TOKEN_VENDOR_ID:
                case TEST_TOKEN_SENSOR_ID:
                case TEST_TOKEN_PRODUCTION_DATE:
                case TEST_TOKEN_RAW_DATA:
                case TEST_TOKEN_BMP_DATA:
                case TEST_TOKEN_HBD_RAW_DATA:
                case TEST_TOKEN_DUMP_ENCRYPTED_DATA:
                case TEST_TOKEN_DUMP_CHIP_ID:
                case TEST_TOKEN_DUMP_VENDOR_ID:
                case TEST_TOKEN_DUMP_SENSOR_ID:
                case TEST_TOKEN_DUMP_KR:
                case TEST_TOKEN_DUMP_B:
                case TEST_TOKEN_DUMP_RAW_DATA:
                case TEST_TOKEN_DUMP_BROKEN_CHECK_RAW_DATA:
                case TEST_TOKEN_DUMP_CALI_RES:
                case TEST_TOKEN_DUMP_DATA_BMP:
                case TEST_TOKEN_DUMP_SITO_BMP:
                case TEST_TOKEN_DUMP_TEMPLATE:
                case TEST_TOKEN_SPI_RW_CONTENT:
                case TEST_TOKEN_CFG_DATA:
                case TEST_TOKEN_FW_DATA:
                case TEST_TOKEN_GSC_DATA:
                case TEST_TOKEN_DUMP_NAV_FRAME_NUM:
                case TEST_TOKEN_DATA_DEVIATION_DIFF:{

                    int size = decodeInt32(result, offset);
                    offset += 4;
                    if (size > 0) {
                        testResult.put(token, Arrays.copyOfRange(result, offset, offset + size));
                        offset += size;
                    }
                    break;
                }

                case TEST_TOKEN_ALL_TILT_ANGLE:
                case TEST_TOKEN_BLOCK_TILT_ANGLE_MAX: {
                    int size = decodeInt32(result, offset);
                    offset += 4;
                    float value = decodeFloat(result, offset, size);
                    testResult.put(token, value);
                    offset += size;
                    Log.d(TAG, "value = " + value);
                    break;
                }

                case TEST_TOKEN_NOISE: {
                    int size = decodeInt32(result, offset);
                    offset += 4;
                    double value = decodeDouble(result, offset, size);
                    testResult.put(token, value);
                    offset += size;
                    Log.d(TAG, "value = " + value);
                    break;
                }

                case TEST_TOKEN_AVG_DIFF_VAL:
                case TEST_TOKEN_LOCAL_WORST:
                case TEST_TOKEN_IN_CIRCLE:
                case TEST_TOKEN_BIG_BUBBLE:
                case TEST_TOKEN_LINE:
                case TEST_TOKEN_HBD_BASE:
                case TEST_TOKEN_HBD_AVG: {
                    short value = decodeInt16(result, offset);
                    offset += 2;
                    testResult.put(token, value);
                    Log.d(TAG, "value = " + value);
                    break;
                }

                case TEST_TOKEN_SENSOR_OTP_TYPE:
                case TEST_TOKEN_IS_BAD_LINE: {
                    byte value = decodeInt8(result, offset);
                    offset += 1;
                    testResult.put(token, value);
                    Log.d(TAG, "value = " + value);
                    break;
                }

                case TEST_TOKEN_DUMP_TIMESTAMP: {
                    long value = decodeInt64(result, offset);
                    offset += 8;
                    testResult.put(token, value);
                    Log.d(TAG, "value = " + value);
                    break;
                }

                case TEST_TOKEN_PACKAGE_VERSION:
                case TEST_TOKEN_PROTOCOL_VERSION:
                case TEST_TOKEN_CHIP_SUPPORT_BIO:
                case TEST_TOKEN_IS_BIO_ENABLE:
                case TEST_TOKEN_AUTHENTICATED_WITH_BIO_SUCCESS_COUNT:
                case TEST_TOKEN_AUTHENTICATED_WITH_BIO_FAILED_COUNT:
                case TEST_TOKEN_AUTHENTICATED_SUCCESS_COUNT:
                case TEST_TOKEN_AUTHENTICATED_FAILED_COUNT:
                case TEST_TOKEN_BUF_FULL:
                case TEST_TOKEN_UPDATE_POS: {
                    int value = decodeInt32(result, offset);
                    offset += 4;
                    testResult.put(token, value);
                    Log.d(TAG, "value = " + value);
                    break;
                }

                case TEST_TOKEN_METADATA: {
                    int size = decodeInt32(result, offset);
                    offset += 4;
                    String value = new String(result, offset, size);
                    testResult.put(token, value);
                    offset += size;
                    break;
                }

                default:
                    break;
            }
        }

        return testResult;
    }
}
