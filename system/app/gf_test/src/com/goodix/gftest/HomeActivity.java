
package com.goodix.gftest;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.GFConfig;
import com.goodix.fingerprint.service.GoodixFingerprintManager;
import com.goodix.fingerprint.service.GoodixFingerprintManager.TestCmdCallback;
import com.goodix.fingerprint.utils.TestParamEncoder;
import com.goodix.fingerprint.utils.TestResultParser;
import com.goodix.gftest.utils.TestHistoryUtils;
import com.goodix.gftest.utils.TestResultChecker;
import com.goodix.gftest.utils.TestResultChecker.Checker;
import com.goodix.gftest.utils.TestResultChecker.MilanASeriesChecker;
import com.goodix.gftest.utils.TestResultChecker.TestResultCheckerFactory;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends Activity {

    private static final String TAG = "HomeActivity";

    private ListView mListView = null;
    private Button mDetailBtn = null;
    private AlertDialog mCountDownDialog = null;

    private static int[] TEST_ITEM = null;

    private static final int TEST_ITEM_STATUS_IDLE = 0;
    private static final int TEST_ITEM_STATUS_TESTING = 1;
    private static final int TEST_ITEM_STATUS_SUCCEED = 2;
    private static final int TEST_ITEM_STATUS_FAILED = 3;
    private static final int TEST_ITEM_STATUS_TIMEOUT = 4;
    private static final int TEST_ITEM_STATUS_CANCELED = 5;
    private static final int TEST_ITEM_STATUS_WAIT_FINGER_INPUT = 6;
    private static final int TEST_ITEM_STATUS_WAIT_BAD_POINT_INPUT = 7;
    private static final int TEST_ITEM_STATUS_WAIT_REAL_FINGER_INPUT = 9;

    private HashMap<Integer, Integer> mTestStatus = new HashMap<Integer, Integer>();

    private ProgressDialog mDialog;
    private Handler mHandler = new Handler();
    private MyAdapter mAdapter = new MyAdapter();

    private Toast mToast = null;

    private boolean mIsSensorValidityTested = false;
    private int mSensorValidityTestFlag = 1;
    private boolean mAutoTest = false;
    private int mAutoTestPosition = 0;
    private TextView mAutoTestingView = null;
    private TextView mAutoTestingTitleView = null;
    private long mAutoTestTimeout = Constants.TEST_TIMEOUT_MS;
    private long mMillisStart = 0;

    private long mAutoTestStartTime = 0;
    private long mAutoTestPrevTestEndTime = 0;

    private GoodixFingerprintManager mGoodixFingerprintManager = null;
    private GFConfig mConfig = null;
    private HashMap<Integer, Object> mPendingBioDetail = null;

    private Checker mTestResultChecker;
    // add fingerprint test for runtime 20170518 dingfan start
    Intent resultIntent = null;
    private final int RESULT_CODE_1 = 10;
    // add fingerprint test for runtime 20170518 dingfan end
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mGoodixFingerprintManager = new GoodixFingerprintManager(HomeActivity.this);
        mConfig = mGoodixFingerprintManager.getConfig();

        // default use Oswego items
        TEST_ITEM = MilanASeriesChecker.TEST_ITEM_MILANA;
        if (null != mConfig) {
            mTestResultChecker = TestResultCheckerFactory.getInstance(mConfig.mChipType, mConfig.mChipSeries);
            TEST_ITEM = mTestResultChecker.getTestItems(0);
        }
        Log.i(TAG,"TEST_ITEM ="+TEST_ITEM.length);
        initView();

        // save result to "/data/data/com.goodix.gftest/files/testtool.txt"
        TestHistoryUtils.init(getFilesDir().getPath(), "testtool.txt", "testdetail.txt");
        checkHardwareDetected();
    }

    private void checkHardwareDetected() {
        boolean isHardwareDetected = true;
        try {
            Object fingerprintManager = getSystemService("fingerprint");
            Method isHardwareDetectedMethod = fingerprintManager.getClass().getMethod(
                    "isHardwareDetected", new Class[]{});
            isHardwareDetectedMethod.setAccessible(true);
            isHardwareDetected = (Boolean) isHardwareDetectedMethod.invoke(fingerprintManager,
                    new Object[]{});
        } catch (IllegalAccessException e) {
        } catch (IllegalArgumentException e) {
        } catch (InvocationTargetException e) {
            isHardwareDetected = false;
        } catch (NoSuchMethodException e) {
        } catch (Exception e) {
        }

        Log.i(TAG, "isHardwareDetected = " + isHardwareDetected);
        if (!isHardwareDetected) {
            new AlertDialog.Builder(HomeActivity.this)
            .setTitle(HomeActivity.this.getString(R.string.sytem_info))
            .setMessage(HomeActivity.this.getString(R.string.no_hardware))
            .setPositiveButton(HomeActivity.this.getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            })
            .show();
        }

        if (null != mConfig && Constants.GF_MILAN_F_SERIES == mConfig.mChipSeries) {
            mDialog = new ProgressDialog(this);
            mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mDialog.setCancelable(true);
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.setMessage(HomeActivity.this.getString(R.string.sensor_checking));
            mDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {

                }

            });
            mDialog.show();
        }
    }

    public void initView() {

        if (TEST_ITEM == null) {
            return;
        }

        for (Integer test_item : TEST_ITEM) {
            mTestStatus.put(test_item, TEST_ITEM_STATUS_IDLE);
        }

        mListView = (ListView) findViewById(R.id.listview);
        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {

                if (0 == mSensorValidityTestFlag) {
                    return;
                }

                // header view
                if (position == 0) {
                    Log.d(TAG, "onItemClick mAutoTest = " + mAutoTest);
                    if (mAutoTest) {
                        if (mToast != null) {
                            mToast.cancel();
                        }
                        mToast = Toast.makeText(HomeActivity.this, R.string.busy,
                                Toast.LENGTH_SHORT);
                        mToast.show();
                    } else {
                        startAutoTest();
                    }
                    return;
                }

                if (position - 1 >= TEST_ITEM.length && mAutoTest) {
                    Log.d(TAG, "onItemClick mAutoTest = " + mAutoTest);
                    return;
                }

                startTest(TEST_ITEM[position - 1]);
            }

        });

        mListView.setAdapter(mAdapter);

        View header = LayoutInflater.from(HomeActivity.this)
                .inflate(R.layout.item_home, null);
        header.findViewById(R.id.testing).setVisibility(View.INVISIBLE);
        mAutoTestingTitleView = (TextView) header.findViewById(R.id.test_title);
        mAutoTestingTitleView.setText(R.string.test_auto);

        mAutoTestingView = (TextView) header.findViewById(R.id.test_result);
        mAutoTestingView.setText(R.string.testing);
        mAutoTestingView.setVisibility(View.INVISIBLE);
        mListView.addHeaderView(header);

        mDetailBtn = (Button) findViewById(R.id.test_detail);
        mDetailBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, DetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mGoodixFingerprintManager.registerTestCmdCallback(mTestCmdCallback);

        if (null != mConfig && Constants.GF_MILAN_F_SERIES == mConfig.mChipSeries) {
            Log.d(TAG, "TEST_CHECK_SENSOR_TEST_INFO start");
            if (!mIsSensorValidityTested) {
                mSensorValidityTestFlag = 0;
                mAutoTestingTitleView.setEnabled(false);
                mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_SENSOR_VALIDITY, null);
            }
        }
        getTimeout();
        mHandler.post(mAutoTestRunnable);
        // add fingerprint test for runtime 20170518 dingfan start
        startAutoTest();
        // add fingerprint test for runtime 20170518 dingfan end
    }

    @Override
    protected void onPause() {
        super.onPause();

        stopTest(TEST_ITEM_STATUS_CANCELED);

        mHandler.removeCallbacks(mTimeoutRunnable);
        mHandler.removeCallbacks(mAutoTestRunnable);

        mGoodixFingerprintManager.unregisterTestCmdCallback(mTestCmdCallback);
    }

    private void startAutoTest() {
        Log.d(TAG, "startAutoTest");
        mAutoTest = true;
        Log.d(TAG, "startAutoTest mAutoTest = " + mAutoTest);

        TestHistoryUtils.clearHistory();
        TestHistoryUtils.init(getFilesDir().getPath(), "testtool.txt", "testdetail.txt");

        for (Integer test_item : TEST_ITEM) {
            mTestStatus.put(test_item, TEST_ITEM_STATUS_IDLE);
        }
        mAdapter.notifyDataSetChanged();

        mAutoTestStartTime = System.currentTimeMillis();
        mAutoTestPrevTestEndTime = mAutoTestStartTime;
        mAutoTestPosition = 0;

        startTest(TEST_ITEM[mAutoTestPosition]);
        mAutoTestPosition++;
        mAutoTestingView.setVisibility(View.VISIBLE);
    }

    private void saveTestResult(int testId, int reason) {
        mTestStatus.put(testId, reason);
        if (reason == TEST_ITEM_STATUS_TIMEOUT) {
            TestHistoryUtils.addResult(testId, "result=TIMEOUT");
        } else if (reason == TEST_ITEM_STATUS_CANCELED) {
            TestHistoryUtils.addResult(testId, "result=CANCELED");
        } else if (reason == TEST_ITEM_STATUS_FAILED) {
            TestHistoryUtils.addResult(testId, "result=FAILED");
        } else if (reason == TEST_ITEM_STATUS_SUCCEED) {
            TestHistoryUtils.addResult(testId, "result=SUCCEED");
        }

        mAdapter.notifyDataSetChanged();
        mHandler.removeCallbacks(mTimeoutRunnable);

        autoNextTest();
    }

    private void saveTestDetail(int testId, HashMap<Integer, Object> result) {
        TestHistoryUtils.addDetail(testId, result);
        TestHistoryUtils.addDetail("time:"
                + (System.currentTimeMillis() - mAutoTestPrevTestEndTime)
                + "ms");
    }

    // dedicated patch function for TEST_BIO_ASSAY
    private void saveTestDetail(int testId, HashMap<Integer, Object> result1,
            HashMap<Integer, Object> result2) {
        if (testId == TestResultChecker.TEST_BIO_CALIBRATION
                || testId == TestResultChecker.TEST_HBD_CALIBRATION) {
            TestHistoryUtils.addDetail(testId, result1, result2);
            TestHistoryUtils
            .addDetail("time:" + (System.currentTimeMillis() - mAutoTestPrevTestEndTime)
                    + "ms");
        }
    }

    private void enableBioAssay() {
        if (mConfig.mChipType == Constants.GF_CHIP_5206
                || mConfig.mChipType == Constants.GF_CHIP_5208) {
            toggleBioAssay(true);
        }
    }

    private void disableBioAssay() {
        if (mConfig.mChipType == Constants.GF_CHIP_5206
                || mConfig.mChipType == Constants.GF_CHIP_5208) {
            toggleBioAssay(false);
        }
    }

    private void resetBioAssay() {
        Log.d(TAG, "support bio assay : " + mConfig.mSupportBioAssay);
        if (mConfig.mSupportBioAssay == 0) {
            disableBioAssay();
        } else {
            enableBioAssay();
        }
    }

    private void toggleBioAssay(boolean enabled) {
        byte[] byteArray = new byte[TestParamEncoder.TEST_ENCODE_SIZEOF_INT32];
        int offset = 0;
        int value = 0;

        if (true == enabled) {
            value = 1;
        }

        offset = TestParamEncoder.encodeInt32(byteArray, offset,
                TestResultParser.TEST_TOKEN_SUPPORT_BIO_ASSAY, value);
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_SET_CONFIG, byteArray);
    }

    private void stopTest(int reason) {
        Log.d(TAG, "stopTest reason: " + reason);

        stopCountDownForSwitchFinger();
        for (Integer test_item : TEST_ITEM) {
            if (mTestStatus.get(test_item) == TEST_ITEM_STATUS_TESTING
                    || mTestStatus.get(test_item) == TEST_ITEM_STATUS_WAIT_FINGER_INPUT
                    || mTestStatus.get(test_item) == TEST_ITEM_STATUS_WAIT_BAD_POINT_INPUT
                    || mTestStatus.get(test_item) == TEST_ITEM_STATUS_WAIT_REAL_FINGER_INPUT) {

                switch (test_item) {
                    case TestResultChecker.TEST_SPI:
                        Log.d(TAG, "TEST_SPI "
                                + (reason == TEST_ITEM_STATUS_TIMEOUT ? "timeout" : "canceled"));
                        break;

                    case TestResultChecker.TEST_PIXEL:
                        Log.d(TAG, "TEST_PIXEL "
                                + (reason == TEST_ITEM_STATUS_TIMEOUT ? "timeout" : "canceled"));
                        break;

                    case TestResultChecker.TEST_RESET_PIN:
                        Log.d(TAG, "TEST_RESET_PIN "
                                + (reason == TEST_ITEM_STATUS_TIMEOUT ? "timeout" : "canceled"));
                        break;

                    case TestResultChecker.TEST_BAD_POINT:
                        Log.d(TAG, "TEST_BAD_POINT "
                                + (reason == TEST_ITEM_STATUS_TIMEOUT ? "timeout" : "canceled"));
                        break;

                    case TestResultChecker.TEST_PERFORMANCE:
                        Log.d(TAG, "TEST_PERFORMANCE "
                                + (reason == TEST_ITEM_STATUS_TIMEOUT ? "timeout" : "canceled"));
                        break;

                    case TestResultChecker.TEST_CAPTURE:
                        Log.d(TAG, "TEST_CAPTURE "
                                + (reason == TEST_ITEM_STATUS_TIMEOUT ? "timeout" : "canceled"));
                        break;

                    case TestResultChecker.TEST_ALGO:
                        Log.d(TAG, "TEST_ALGO "
                                + (reason == TEST_ITEM_STATUS_TIMEOUT ? "timeout" : "canceled"));
                        break;

                    case TestResultChecker.TEST_BIO_CALIBRATION:
                        Log.d(TAG, "TEST_BIO_CALIBRATION "
                                + (reason == TEST_ITEM_STATUS_TIMEOUT ? "timeout" : "canceled"));
                        break;

                    case TestResultChecker.TEST_HBD_CALIBRATION:
                        Log.d(TAG, "TEST_HBD_CALIBRATION "
                                + (reason == TEST_ITEM_STATUS_TIMEOUT ? "timeout" : "canceled"));
                        break;

                    case TestResultChecker.TEST_FW_VERSION:
                        Log.d(TAG, "TEST_FW_VERSION "
                                + (reason == TEST_ITEM_STATUS_TIMEOUT ? "timeout" : "canceled"));
                        break;
                }

                resetBioAssay();
                if(TestResultChecker.TEST_SPI == test_item) {
                    mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_PRIOR_CANCEL, null);
                }else {
                    mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);
                }
                saveTestResult(test_item, reason);
                saveTestDetail(test_item, null);
                break;
            }
        }
    }

    private boolean startTest(int testCmd) {
        Log.d(TAG, "stopTest cmd: " + testCmd);
        for (Integer test_item : TEST_ITEM) {
            if (mTestStatus.get(test_item) == TEST_ITEM_STATUS_TESTING
                    || mTestStatus.get(test_item) == TEST_ITEM_STATUS_WAIT_FINGER_INPUT
                    || mTestStatus.get(test_item) == TEST_ITEM_STATUS_WAIT_BAD_POINT_INPUT) {

                if (mToast != null) {
                    mToast.cancel();
                }
                mToast = Toast.makeText(HomeActivity.this, R.string.busy, Toast.LENGTH_SHORT);
                mToast.show();

                Log.d(TAG, "startTest " + test_item + " busy");
                return false;
            }
        }

        if (mAutoTestPrevTestEndTime == 0) {
            mAutoTestStartTime = System.currentTimeMillis();
        }

        switch (testCmd) {
            case TestResultChecker.TEST_SPI:
                Log.d(TAG, "TEST_SPI start");
                mTestStatus.put(testCmd, TEST_ITEM_STATUS_TESTING);
                mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_SPI, null);
                break;

            case TestResultChecker.TEST_PIXEL:
                Log.d(TAG, "TEST_PIXEL start");
                mTestStatus.put(testCmd, TEST_ITEM_STATUS_TESTING);
                mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_PIXEL_OPEN, null);
                break;

            case TestResultChecker.TEST_RESET_PIN:
                Log.d(TAG, "TEST_RESET_PIN start");
                mTestStatus.put(testCmd, TEST_ITEM_STATUS_TESTING);
                mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_RESET_PIN, null);
                break;

            case TestResultChecker.TEST_BAD_POINT:
                Log.d(TAG, "TEST_BAD_POINT start");
                mTestStatus.put(testCmd, TEST_ITEM_STATUS_WAIT_BAD_POINT_INPUT);
                mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_BAD_POINT, null);
                break;

            case TestResultChecker.TEST_PERFORMANCE:
                if (mAutoTest) {
                    TestHistoryUtils.addResult("fingerdown 0");
                }

                Log.d(TAG, "TEST_PERFORMANCE start");
                disableBioAssay();
                mTestStatus.put(testCmd, TEST_ITEM_STATUS_WAIT_FINGER_INPUT);
                mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_PERFORMANCE, null);
                break;

            case TestResultChecker.TEST_CAPTURE:
                Log.d(TAG, "TEST_CAPTURE start");
                disableBioAssay();
                mTestStatus.put(testCmd, TEST_ITEM_STATUS_WAIT_FINGER_INPUT);
                mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_PERFORMANCE, null);
                break;

            case TestResultChecker.TEST_ALGO:
                Log.d(TAG, "TEST_ALGO start");
                disableBioAssay();
                mTestStatus.put(testCmd, TEST_ITEM_STATUS_WAIT_FINGER_INPUT);
                mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_PERFORMANCE, null);
                break;

            case TestResultChecker.TEST_BIO_CALIBRATION:

                enableBioAssay();
                mTestStatus.put(TestResultChecker.TEST_BIO_CALIBRATION,
                        TEST_ITEM_STATUS_TESTING);
                mAdapter.notifyDataSetChanged();
                if (mTestResultChecker.getTestItems(1) == TEST_ITEM) {
                    startCountDownForSwitchFinger();
                } else {
                    startTestBioCalibration();
                }
                break;

            case TestResultChecker.TEST_HBD_CALIBRATION:

                enableBioAssay();
                mTestStatus.put(TestResultChecker.TEST_HBD_CALIBRATION,
                        TEST_ITEM_STATUS_WAIT_REAL_FINGER_INPUT);
                startCheckFingerDownStatus();
                break;
            case TestResultChecker.TEST_FW_VERSION:
                Log.d(TAG, "TEST_FW_VERSION start");
                mTestStatus.put(testCmd, TEST_ITEM_STATUS_TESTING);
                mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_SPI, null);
                break;

        }

        mAdapter.notifyDataSetChanged();
        mDetailBtn.setEnabled(false);

        // if (mAutoTest) {
        mHandler.removeCallbacks(mTimeoutRunnable);
        mHandler.postDelayed(mTimeoutRunnable, mAutoTestTimeout);
        // }
        return true;
    }

    private void autoNextTest() {
        if (mAutoTest) {
            boolean canceled = false;
            boolean timeout = false;
            boolean failed = false;
            if (mAutoTestPosition > 0) {
                int status = mTestStatus.get(TEST_ITEM[mAutoTestPosition - 1]);
                if (status == TEST_ITEM_STATUS_FAILED) {
                    failed = true;
                }
                if (status == TEST_ITEM_STATUS_TIMEOUT) {
                    timeout = true;
                }
                if (status == TEST_ITEM_STATUS_CANCELED) {
                    canceled = true;
                }
            }

            if (mAutoTestPosition < mAdapter.getCount() && !canceled/* && !timeout && !failed */) {
                mAutoTestPrevTestEndTime = System.currentTimeMillis();
                if (startTest(TEST_ITEM[mAutoTestPosition])) {
                    mAutoTestPosition++;
                }
                Log.d(TAG, "autoNextTest mAutoTestPosition = " + mAutoTestPosition);
            } else {
                checkResult();
                mDetailBtn.setEnabled(true);

                mAutoTest = false;
                Log.d(TAG, "autoNextTest mAutoTest = " + mAutoTest);
                mAutoTestPosition = 0;
                mAutoTestingView.setVisibility(View.INVISIBLE);
                // add fingerprint test for runtime 20170518 dingfan start
                this.finish();
                // add fingerprint test for runtime 20170518 dingfan end
            }
        } else {
            mDetailBtn.setEnabled(true);
        }
    }

    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return TEST_ITEM.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder = null;
            if (convertView == null) {
                convertView = LayoutInflater.from(HomeActivity.this).inflate(
                        R.layout.item_home, null);

                holder = new Holder();
                holder.titleView = (TextView) convertView.findViewById(R.id.test_title);
                holder.resultView = (TextView) convertView.findViewById(R.id.test_result);
                holder.testingView = convertView.findViewById(R.id.testing);
                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }

            if(mSensorValidityTestFlag == 0) {
                holder.titleView.setEnabled(false);
            } else {
                holder.titleView.setEnabled(true);
            }

            switch (TEST_ITEM[position]) {
                case TestResultChecker.TEST_SPI:
                    holder.titleView.setText(R.string.test_spi);
                    updateTestView(holder, mTestStatus.get(TEST_ITEM[position]));
                    break;

                case TestResultChecker.TEST_PIXEL:
                    if (mConfig != null
                    && (mConfig.mChipSeries == Constants.GF_MILAN_F_SERIES
                    || mConfig.mChipSeries == Constants.GF_MILAN_A_SERIES)) {
                        holder.titleView.setText(R.string.test_pixel_open);
                    } else {
                        holder.titleView.setText(R.string.test_sensor);
                    }
                    updateTestView(holder, mTestStatus.get(TEST_ITEM[position]));
                    break;

                case TestResultChecker.TEST_RESET_PIN:
                    holder.titleView.setText(R.string.test_reset_pin);
                    updateTestView(holder, mTestStatus.get(TEST_ITEM[position]));
                    break;

                case TestResultChecker.TEST_BAD_POINT:
                    holder.titleView.setText(R.string.test_bad_point);
                    updateTestView(holder, mTestStatus.get(TEST_ITEM[position]));
                    break;

                case TestResultChecker.TEST_PERFORMANCE:
                    holder.titleView.setText(R.string.test_performance);
                    updateTestView(holder, mTestStatus.get(TEST_ITEM[position]));
                    break;

                case TestResultChecker.TEST_CAPTURE:
                    holder.titleView.setText(R.string.test_capture);
                    updateTestView(holder, mTestStatus.get(TEST_ITEM[position]));
                    break;

                case TestResultChecker.TEST_ALGO:
                    holder.titleView.setText(R.string.test_algo);
                    updateTestView(holder, mTestStatus.get(TEST_ITEM[position]));
                    break;

                case TestResultChecker.TEST_BIO_CALIBRATION:
                    holder.titleView.setText(R.string.test_bio_assay);
                    updateTestView(holder, mTestStatus.get(TEST_ITEM[position]));
                    break;

                case TestResultChecker.TEST_HBD_CALIBRATION:
                    holder.titleView.setText(R.string.test_hbd_feature);
                    updateTestView(holder, mTestStatus.get(TEST_ITEM[position]));
                    break;

                case TestResultChecker.TEST_FW_VERSION:
                    holder.titleView.setText(R.string.test_fw_version);
                    updateTestView(holder, mTestStatus.get(TEST_ITEM[position]));
                    break;
            }

            return convertView;
        }

        private void updateTestView(Holder holder, int status) {

            if (status == TEST_ITEM_STATUS_IDLE) {
                holder.resultView.setVisibility(View.INVISIBLE);
                holder.testingView.setVisibility(View.INVISIBLE);
            } else if (status == TEST_ITEM_STATUS_TESTING) {
                holder.resultView.setVisibility(View.INVISIBLE);
                holder.testingView.setVisibility(View.VISIBLE);
            } else if (status == TEST_ITEM_STATUS_SUCCEED) {
                holder.resultView.setVisibility(View.VISIBLE);
                holder.testingView.setVisibility(View.INVISIBLE);
                holder.resultView.setText(R.string.test_succeed);
                holder.resultView.setTextColor(getResources().getColor(R.color.test_succeed_color));
            } else if (status == TEST_ITEM_STATUS_FAILED) {
                holder.resultView.setVisibility(View.VISIBLE);
                holder.testingView.setVisibility(View.INVISIBLE);
                holder.resultView.setText(R.string.test_failed);
                holder.resultView.setTextColor(getResources().getColor(R.color.test_failed_color));
            } else if (status == TEST_ITEM_STATUS_TIMEOUT) {
                holder.resultView.setVisibility(View.VISIBLE);
                holder.testingView.setVisibility(View.INVISIBLE);
                holder.resultView.setText(R.string.timeout);
                holder.resultView.setTextColor(getResources().getColor(R.color.test_failed_color));
            } else if (status == TEST_ITEM_STATUS_CANCELED) {
                holder.resultView.setVisibility(View.VISIBLE);
                holder.testingView.setVisibility(View.INVISIBLE);
                holder.resultView.setText(R.string.canceled);
                holder.resultView.setTextColor(getResources().getColor(R.color.test_failed_color));
            } else if (status == TEST_ITEM_STATUS_WAIT_FINGER_INPUT) {
                holder.resultView.setVisibility(View.VISIBLE);
                holder.resultView.setText(R.string.normal_touch_sensor);
                holder.resultView.setTextColor(getResources().getColor(R.color.fg_color));
                holder.testingView.setVisibility(View.INVISIBLE);
            } else if (status == TEST_ITEM_STATUS_WAIT_BAD_POINT_INPUT) {
                holder.resultView.setVisibility(View.VISIBLE);
                holder.resultView.setText(R.string.bad_point_touch_sensor);
                holder.resultView.setTextColor(getResources().getColor(R.color.fg_color));
                holder.testingView.setVisibility(View.INVISIBLE);
            } else if (status == TEST_ITEM_STATUS_WAIT_REAL_FINGER_INPUT) {
                holder.resultView.setVisibility(View.VISIBLE);
                holder.resultView.setText(R.string.real_finger_touch_sensor);
                holder.resultView.setTextColor(getResources().getColor(R.color.fg_color));
                holder.testingView.setVisibility(View.INVISIBLE);
            }
        }

        private class Holder {
            TextView titleView;
            TextView resultView;
            View testingView;
        }
    }

    public void getTimeout() {

        try {
            Class<?> systemPropertiesClazz = Class.forName("android.os.SystemProperties");
            Method method = systemPropertiesClazz.getMethod("getLong", new Class[]{
                    String.class, long.class
            });
            mAutoTestTimeout = (Long) method.invoke(null, new Object[]{
                    Constants.PROPERTY_TEST_ITME_TIMEOUT,
                    Constants.TEST_TIMEOUT_MS
            });
            Log.i(TAG, "getTimeout mAutoTestTimeout = " + mAutoTestTimeout);
        } catch (ClassNotFoundException e) {
            Log.e(TAG, "ClassNotFoundException");
        } catch (NoSuchMethodException e) {
            Log.e(TAG, "NoSuchMethodException");
        } catch (IllegalAccessException e) {
            Log.e(TAG, "IllegalAccessException");
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "IllegalArgumentException");
        } catch (InvocationTargetException e) {
            Log.e(TAG, "InvocationTargetException");
        }
    }

    private void onTestSpi(final HashMap<Integer, Object> result) {
        Log.d(TAG, "TEST_SPI end");

        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_PRIOR_CANCEL, null);

        if (mTestStatus.get(TestResultChecker.TEST_SPI) != TEST_ITEM_STATUS_TESTING) {
            return;
        }

        if (result == null) {
            Log.e(TAG, "TEST_SPI failed1");
            saveTestResult(TestResultChecker.TEST_SPI, TEST_ITEM_STATUS_FAILED);
            return;
        }

        saveTestDetail(TestResultChecker.TEST_SPI, result);

        boolean success = mTestResultChecker.checkSpiTestResult(result);

        if (success) {
            Log.d(TAG, "TEST_SPI succeed");
            saveTestResult(TestResultChecker.TEST_SPI, TEST_ITEM_STATUS_SUCCEED);
        } else {
            Log.e(TAG, "TEST_SPI failed2");
            saveTestResult(TestResultChecker.TEST_SPI, TEST_ITEM_STATUS_FAILED);
        }
    }

    private void onTestResetPin(final HashMap<Integer, Object> result) {
        Log.d(TAG, "TEST_RESET_PIN end");
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);

        if (mTestStatus.get(TestResultChecker.TEST_RESET_PIN) != TEST_ITEM_STATUS_TESTING) {
            return;
        }

        if (result == null) {
            Log.e(TAG, "TEST_RESET_PIN failed1");
            saveTestResult(TestResultChecker.TEST_RESET_PIN, TEST_ITEM_STATUS_FAILED);
            return;
        }

        saveTestDetail(TestResultChecker.TEST_RESET_PIN, result);

        boolean success = mTestResultChecker.checkResetPinTestReuslt(result);

        if (success) {
            Log.d(TAG, "TEST_RESET_PIN succeed");
            saveTestResult(TestResultChecker.TEST_RESET_PIN, TEST_ITEM_STATUS_SUCCEED);
        } else {
            Log.e(TAG, "TEST_RESET_PIN failed2");
            saveTestResult(TestResultChecker.TEST_RESET_PIN, TEST_ITEM_STATUS_FAILED);
        }
    }

    private void onTestSensor(final HashMap<Integer, Object> result) {
        Log.d(TAG, "TEST_PIXEL end");

        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);

        if (mTestStatus.get(TestResultChecker.TEST_PIXEL) != TEST_ITEM_STATUS_TESTING) {
            return;
        }

        if (result == null) {
            Log.e(TAG, "TEST_PIXEL failed1");
            saveTestResult(TestResultChecker.TEST_PIXEL, TEST_ITEM_STATUS_FAILED);
            return;
        }

        saveTestDetail(TestResultChecker.TEST_PIXEL, result);

        boolean success = mTestResultChecker.checkPixelTestResult(result);

        if (success) {
            Log.d(TAG, "TEST_PIXEL succeed");
            saveTestResult(TestResultChecker.TEST_PIXEL, TEST_ITEM_STATUS_SUCCEED);
        } else {
            Log.e(TAG, "TEST_PIXEL failed2");
            saveTestResult(TestResultChecker.TEST_PIXEL, TEST_ITEM_STATUS_FAILED);
        }
    }

    private void onTestFWVersion(final HashMap<Integer, Object> result) {
        Log.d(TAG, "TEST_FW_VERSION end");

        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);

        if (mTestStatus.get(TestResultChecker.TEST_FW_VERSION) != TEST_ITEM_STATUS_TESTING) {
            return;
        }

        if (result == null) {
            Log.e(TAG, "TEST_FW_VERSION failed1");
            saveTestResult(TestResultChecker.TEST_FW_VERSION, TEST_ITEM_STATUS_FAILED);
            return;
        }

        saveTestDetail(TestResultChecker.TEST_FW_VERSION, result);

        boolean success = mTestResultChecker.checkFwVersionTestResult(result);
        if (success) {
            Log.d(TAG, "TEST_FW_VERSION succeed");
            saveTestResult(TestResultChecker.TEST_FW_VERSION, TEST_ITEM_STATUS_SUCCEED);
        } else {
            Log.e(TAG, "TEST_FW_VERSION failed2");
            saveTestResult(TestResultChecker.TEST_FW_VERSION, TEST_ITEM_STATUS_FAILED);
        }
    }

    private void onTestBadPoint(final HashMap<Integer, Object> result) {
        Log.d(TAG, "TEST_BAD_POINT end");

        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);

        if (mTestStatus
                .get(TestResultChecker.TEST_BAD_POINT) != TEST_ITEM_STATUS_WAIT_BAD_POINT_INPUT) {
            return;
        }

        if (result == null) {
            Log.e(TAG, "TEST_BAD_POINT failed1");
            saveTestResult(TestResultChecker.TEST_BAD_POINT, TEST_ITEM_STATUS_FAILED);
            return;
        }

        saveTestDetail(TestResultChecker.TEST_BAD_POINT, result);

        boolean success = mTestResultChecker.checkBadPointTestResult(result);

        if (success) {
            Log.d(TAG, "TEST_BAD_POINT succeed");
            saveTestResult(TestResultChecker.TEST_BAD_POINT, TEST_ITEM_STATUS_SUCCEED);
        } else {
            Log.e(TAG, "TEST_BAD_POINT failed2");
            saveTestResult(TestResultChecker.TEST_BAD_POINT, TEST_ITEM_STATUS_FAILED);
        }

    }

    private void onTestPerformance(final HashMap<Integer, Object> result) {
        Log.d(TAG, "TEST_PERFORMANCE end");

        resetBioAssay();
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);

        if (mTestStatus
                .get(TestResultChecker.TEST_PERFORMANCE) != TEST_ITEM_STATUS_WAIT_FINGER_INPUT) {
            return;
        }

        if (result == null) {
            Log.e(TAG, "TEST_PERFORMANCE failed1");
            saveTestResult(TestResultChecker.TEST_PERFORMANCE, TEST_ITEM_STATUS_FAILED);
            return;
        }

        saveTestDetail(TestResultChecker.TEST_PERFORMANCE, result);

        boolean success = mTestResultChecker.checkPerformanceTestResult(result);

        if (success) {
            Log.d(TAG, "TEST_PERFORMANCE succeed");
            saveTestResult(TestResultChecker.TEST_PERFORMANCE, TEST_ITEM_STATUS_SUCCEED);
        } else {
            Log.e(TAG, "TEST_PERFORMANCE failed2");
            saveTestResult(TestResultChecker.TEST_PERFORMANCE, TEST_ITEM_STATUS_FAILED);
        }
        mAdapter.notifyDataSetChanged();
    }

    private void onTestCapture(final HashMap<Integer, Object> result) {
        Log.d(TAG, "TEST_CAPTURE end");

        resetBioAssay();
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);

        if (mTestStatus.get(TestResultChecker.TEST_CAPTURE) != TEST_ITEM_STATUS_WAIT_FINGER_INPUT) {
            return;
        }

        if (result == null) {
            Log.e(TAG, "TEST_CAPTURE failed1");
            saveTestResult(TestResultChecker.TEST_CAPTURE, TEST_ITEM_STATUS_FAILED);
            return;
        }

        saveTestDetail(TestResultChecker.TEST_CAPTURE, result);

        boolean success = mTestResultChecker.checkCaptureTestResult(result);

        if (success) {
            Log.d(TAG, "TEST_CAPTURE succeed");
            saveTestResult(TestResultChecker.TEST_CAPTURE, TEST_ITEM_STATUS_SUCCEED);
        } else {
            Log.e(TAG, "TEST_CAPTURE failed2");
            saveTestResult(TestResultChecker.TEST_CAPTURE, TEST_ITEM_STATUS_FAILED);
        }
        mAdapter.notifyDataSetChanged();
    }

    private void onTestAlgo(final HashMap<Integer, Object> result) {
        Log.d(TAG, "TEST_ALGO end");

        resetBioAssay();
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);

        if (mTestStatus.get(TestResultChecker.TEST_ALGO) != TEST_ITEM_STATUS_WAIT_FINGER_INPUT) {
            return;
        }

        if (result == null) {
            Log.e(TAG, "TEST_ALGO failed1");
            saveTestResult(TestResultChecker.TEST_ALGO, TEST_ITEM_STATUS_FAILED);
            return;
        }

        saveTestDetail(TestResultChecker.TEST_ALGO, result);

        boolean success = mTestResultChecker.checkAlgoTestResult(result);

        if (success) {
            Log.d(TAG, "TEST_ALGO succeed");
            saveTestResult(TestResultChecker.TEST_ALGO, TEST_ITEM_STATUS_SUCCEED);
        } else {
            Log.e(TAG, "TEST_ALGO failed2");
            saveTestResult(TestResultChecker.TEST_ALGO, TEST_ITEM_STATUS_FAILED);
        }

        if (mAutoTest) {
            TestHistoryUtils.addResult("fingerup");
        }

        mAdapter.notifyDataSetChanged();
    }

    private void onTestBioCalibrationWithFingerTouch(HashMap<Integer, Object> result) {

        resetBioAssay();
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);

        if (result == null) {
            Log.e(TAG, "TEST_BIO_CALIBRATION failed1");
            saveTestResult(TestResultChecker.TEST_BIO_CALIBRATION, TEST_ITEM_STATUS_FAILED);
            saveTestDetail(TestResultChecker.TEST_BIO_CALIBRATION, mPendingBioDetail);
            return;
        }

        saveTestDetail(TestResultChecker.TEST_BIO_CALIBRATION, mPendingBioDetail, result);
        mPendingBioDetail = null;

        boolean success = mTestResultChecker.checkBioTestResultWithTouched(result);
        if (!success) {
            Log.e(TAG, "TEST_BIO_CALIBRATION step 2 failed1");
            saveTestResult(TestResultChecker.TEST_BIO_CALIBRATION, TEST_ITEM_STATUS_FAILED);
        } else {
            saveTestResult(TestResultChecker.TEST_BIO_CALIBRATION, TEST_ITEM_STATUS_SUCCEED);
        }
    }

    private void onTestBioCalibrationWithFingerUntouch(HashMap<Integer, Object> result) {
        if (result == null) {
            Log.e(TAG, "TEST_BIO_CALIBRATION failed1");
            saveTestResult(TestResultChecker.TEST_BIO_CALIBRATION, TEST_ITEM_STATUS_FAILED);
            return;
        }

        boolean success = mTestResultChecker.checkBioTestResultWithoutTouched(result);

        if (!success) {
            Log.e(TAG, "TEST_BIO_CALIBRATION step 1 failed1");
            saveTestResult(TestResultChecker.TEST_BIO_CALIBRATION, TEST_ITEM_STATUS_FAILED);
            saveTestDetail(TestResultChecker.TEST_BIO_CALIBRATION, result, null);
        } else {
            mPendingBioDetail = result;
            mTestStatus.put(TestResultChecker.TEST_BIO_CALIBRATION,
                    TEST_ITEM_STATUS_WAIT_REAL_FINGER_INPUT);
            mAdapter.notifyDataSetChanged();
            startCheckFingerDownStatus();
        }
        if (mAutoTest) {
            TestHistoryUtils.addResult("finger down 2");
        }
    }

    private void onTestHbdCalibrationStep1(HashMap<Integer, Object> result) {
        if (result == null) {
            Log.e(TAG, "TEST_HBD_CALIBRATION failed1");
            saveTestResult(TestResultChecker.TEST_HBD_CALIBRATION, TEST_ITEM_STATUS_FAILED);
            return;
        }

        boolean success = mTestResultChecker.checkBioTestResultWithTouched(result);

        if (!success) {
            Log.e(TAG, "TEST_HBD_CALIBRATION step 1 failed1");
            saveTestResult(TestResultChecker.TEST_HBD_CALIBRATION, TEST_ITEM_STATUS_FAILED);
            saveTestDetail(TestResultChecker.TEST_HBD_CALIBRATION, result, null);
        } else {
            mPendingBioDetail = result;
            mAdapter.notifyDataSetChanged();
            startTestHbdCalibrationStep2();
        }
    }

    private void onTestHbdCalibrationStep2(HashMap<Integer, Object> result) {

        resetBioAssay();
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);

        if (result == null) {
            Log.e(TAG, "TEST_HBD_CALIBRATION failed1");
            saveTestResult(TestResultChecker.TEST_HBD_CALIBRATION, TEST_ITEM_STATUS_FAILED);
            saveTestDetail(TestResultChecker.TEST_HBD_CALIBRATION, mPendingBioDetail);
            return;
        }
        Log.d(TAG, "onTestHbdCalibrationStep2 -------------------------------");
        saveTestDetail(TestResultChecker.TEST_HBD_CALIBRATION, mPendingBioDetail, result);
        mPendingBioDetail = null;

        if (mAutoTest) {
            TestHistoryUtils.addResult("fingerup");
        }

        boolean success = mTestResultChecker.checkHBDTestResultWithTouched(result);
        if (!success) {
            Log.e(TAG, "TEST_HBD_CALIBRATION step 2 failed1");
            saveTestResult(TestResultChecker.TEST_HBD_CALIBRATION, TEST_ITEM_STATUS_FAILED);
        } else {
            saveTestResult(TestResultChecker.TEST_HBD_CALIBRATION, TEST_ITEM_STATUS_SUCCEED);
        }
    }

    private Runnable mCheckFingerupRunnable = new Runnable() {
        @Override
        public void run() {

            // get switch finger time as system property
            int switchTime = Constants.AUTO_TEST_BIO_PREPARE_TIME;
            try {
                switchTime = Integer
                        .parseInt(getSystemPropertyAsString(Constants.PROPERTY_SWITCH_FINGER_TIME));
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }

            Log.d(TAG, "switch finger time: " + switchTime);

            long millisUntilFinished = switchTime - mMillisStart;
            mCountDownDialog.setMessage(
                    getString(R.string.test_bio_waiting_start, millisUntilFinished / 1000));
            mMillisStart += 1000;

            String fingerStatus = getSystemPropertyAsString(Constants.PROPERTY_FINGER_STATUS);
            if ((fingerStatus != null && fingerStatus.equals("up"))
                    || mMillisStart >= switchTime) {
                if (fingerStatus != null && fingerStatus.equals("up")) {
                    mCountDownDialog.setMessage(getString(R.string.test_bio_recived_up_message));
                }

                Log.d(TAG, "TEST_BIO_ASSAY start step 1");
                mMillisStart = 0;

                stopCountDownForSwitchFinger();
                startTestBioCalibration();
            } else {
                mHandler.postDelayed(mCheckFingerupRunnable, 1000);
            }

        }
    };

    private Runnable mCheckFingerDownRunnable = new Runnable() {

        @Override
        public void run() {

            String status = getSystemPropertyAsString(Constants.PROPERTY_FINGER_STATUS);
            if (status != null && status.equals("down")) {
                if (mTestStatus
                        .get(TestResultChecker.TEST_BIO_CALIBRATION) == TEST_ITEM_STATUS_WAIT_REAL_FINGER_INPUT
                        || mTestStatus.get(
                                TestResultChecker.TEST_HBD_CALIBRATION) == TEST_ITEM_STATUS_WAIT_REAL_FINGER_INPUT) {
                    startTestBioCalibration();
                }
            } else {
                Log.d(TAG, "check system properties again");
                mHandler.postDelayed(mCheckFingerDownRunnable, 500);
            }

        }
    };

    private void startCountDownForSwitchFinger() {

        mCountDownDialog = new AlertDialog.Builder(HomeActivity.this)
                .setTitle(getString(R.string.sytem_info))
                .setMessage(getString(R.string.test_bio_waiting_start,
                        Constants.AUTO_TEST_BIO_PREPARE_TIME) + "\n"
                        + getString(R.string.test_bio_no_finger_tips))
                .create();
        mCountDownDialog.setCancelable(false);
        mCountDownDialog.show();
        mHandler.post(mCheckFingerupRunnable);
    }

    private void stopCountDownForSwitchFinger() {
        if (mCountDownDialog != null) {
            mCountDownDialog.dismiss();
        }
        mHandler.removeCallbacks(mCheckFingerupRunnable);
    }

    private void startCheckFingerDownStatus() {
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CHECK_FINGER_EVENT, null);
        mHandler.post(mCheckFingerDownRunnable);
    }

    private void startTestBioCalibration() {
        mHandler.removeCallbacks(mCheckFingerDownRunnable);
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_BIO_CALIBRATION, null);
    }

    private void startTestHbdCalibrationStep2() {
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_HBD_CALIBRATION, null);
    }

    private void checkResult() {
        if (!mAutoTest) {
            return;
        }

        boolean allSuccess = true;
        for (Integer test_item : TEST_ITEM) {
            if (mTestStatus.get(test_item) != TEST_ITEM_STATUS_SUCCEED) {
                allSuccess = false;
                break;
            }
        }
        // add fingerprint test for runtime 20170518 dingfan start
        if (allSuccess) {
            TestHistoryUtils.addResult("pass");
            Log.i(TAG,"all success");
            resultIntent = getIntent();
            resultIntent.putExtra("result", 1);
            setResult(RESULT_CODE_1, resultIntent);
        } else {
            TestHistoryUtils.addResult("fail");
            resultIntent = getIntent();
            resultIntent.putExtra("result", 0);
            setResult(RESULT_CODE_1, resultIntent);
        }
        // add fingerprint test for runtime 20170518 dingfan end
        TestHistoryUtils.addResult("total time:"
                + (System.currentTimeMillis() - mAutoTestStartTime)
                + "ms");
    }

    private Runnable mTimeoutRunnable = new Runnable() {

        @Override
        public void run() {
            stopTest(TEST_ITEM_STATUS_TIMEOUT);
        }

    };

    private TestCmdCallback mTestCmdCallback = new TestCmdCallback() {

        @Override
        public void onTestCmd(final int cmdId, final HashMap<Integer, Object> result) {
            Log.d(TAG, "onTestCmd");

            if (result == null || mTestResultChecker == null) {
                Log.e(TAG, "GFManager may be wrong");
                return;
            }

            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    switch (cmdId) {
                        case Constants.CMD_TEST_SPI:
                            if (mTestStatus
                                    .get(TestResultChecker.TEST_SPI) == TEST_ITEM_STATUS_TESTING) {
                                onTestSpi(result);
                            } else if (mTestStatus.get(
                                    TestResultChecker.TEST_FW_VERSION) == TEST_ITEM_STATUS_TESTING) {
                                onTestFWVersion(result);
                            }
                            break;

                        case Constants.CMD_TEST_PIXEL_OPEN:
                            if (mTestStatus.get(
                                    TestResultChecker.TEST_PIXEL) == TEST_ITEM_STATUS_TESTING) {
                                onTestSensor(result);
                            }
                            break;

                        case Constants.CMD_TEST_RESET_PIN:
                            if (mTestStatus.get(
                                    TestResultChecker.TEST_RESET_PIN) == TEST_ITEM_STATUS_TESTING) {
                                onTestResetPin(result);
                            }
                            break;

                        case Constants.CMD_TEST_BAD_POINT:
                            if (mTestStatus.get(
                                    TestResultChecker.TEST_BAD_POINT) == TEST_ITEM_STATUS_WAIT_BAD_POINT_INPUT) {
                                onTestBadPoint(result);
                            }
                            break;

                        case Constants.CMD_TEST_PERFORMANCE:
                            if (mTestStatus.get(
                                    TestResultChecker.TEST_ALGO) == TEST_ITEM_STATUS_WAIT_FINGER_INPUT) {
                                onTestAlgo(result);
                            } else if (mTestStatus.get(
                                    TestResultChecker.TEST_CAPTURE) == TEST_ITEM_STATUS_WAIT_FINGER_INPUT) {
                                onTestCapture(result);
                            } else if (mTestStatus.get(
                                    TestResultChecker.TEST_PERFORMANCE) == TEST_ITEM_STATUS_WAIT_FINGER_INPUT) {
                                onTestPerformance(result);
                            }
                            break;

                        case Constants.CMD_TEST_SENSOR_VALIDITY:
                            if (null != mDialog) {
                                mDialog.cancel();
                            }
                            if (result.containsKey(TestResultParser.TEST_TOKEN_SENSOR_VALIDITY)) {
                                mSensorValidityTestFlag = (Integer) result
                                        .get(TestResultParser.TEST_TOKEN_SENSOR_VALIDITY);
                            }

                            mIsSensorValidityTested = true;
                            if (0 == mSensorValidityTestFlag) {
                                Log.i(TAG, "Sensor validity test Fail");
                                new AlertDialog.Builder(HomeActivity.this)
                                .setTitle(HomeActivity.this.getString(R.string.sytem_info))
                                .setMessage(HomeActivity.this
                                        .getString(R.string.sensor_validity_test_fail))
                                .setPositiveButton(
                                        HomeActivity.this.getString(R.string.ok),
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog,
                                                    int which) {
                                                // finish();
                                            }
                                        })
                                .show();
                            } else {
                                mAutoTestingTitleView.setEnabled(true);
                                mAdapter.notifyDataSetChanged();
                                Log.i(TAG, "Sensor validity test Pass");
                            }
                            break;

                        case Constants.CMD_TEST_BIO_CALIBRATION:
                            if (mTestStatus.get(
                                    TestResultChecker.TEST_BIO_CALIBRATION) == TEST_ITEM_STATUS_TESTING) {
                                // step 1
                                onTestBioCalibrationWithFingerUntouch(result);
                            } else if (mTestStatus.get(
                                    TestResultChecker.TEST_BIO_CALIBRATION) == TEST_ITEM_STATUS_WAIT_REAL_FINGER_INPUT) {
                                // step 2
                                onTestBioCalibrationWithFingerTouch(result);
                            } else if (mTestStatus.get(
                                    TestResultChecker.TEST_HBD_CALIBRATION) == TEST_ITEM_STATUS_WAIT_REAL_FINGER_INPUT) {
                                onTestHbdCalibrationStep1(result);
                            }
                            break;
                        case Constants.CMD_TEST_HBD_CALIBRATION:
                            onTestHbdCalibrationStep2(result);
                            break;
                        case Constants.CMD_TEST_CHECK_FINGER_EVENT:
                            if (mTestStatus
                                    .get(TestResultChecker.TEST_BIO_CALIBRATION) == TEST_ITEM_STATUS_WAIT_REAL_FINGER_INPUT
                                    || mTestStatus.get(
                                            TestResultChecker.TEST_HBD_CALIBRATION) == TEST_ITEM_STATUS_WAIT_REAL_FINGER_INPUT) {
                                startTestBioCalibration();
                            }
                            break;
                    }
                }
            });

        }
    };

    private Runnable mAutoTestRunnable = new Runnable() {

        @Override
        public void run() {
            Log.d(TAG, "check system properties");

            String status = getSystemPropertyAsString(Constants.PROPERTY_TEST_ORDER);
            if (status != null && !status.equals("")) {
                int index = 0;
                try {
                    index = Integer.parseInt(status);
                    TEST_ITEM = mTestResultChecker.getTestItems(index);
                } catch (Exception e) {
                    TEST_ITEM = mTestResultChecker.getTestItems(0);
                }

            } else {
                TEST_ITEM = mTestResultChecker.getTestItems(0);
            }

            status = getSystemPropertyAsString(Constants.PROPERTY_AUTO_TEST);
            if (status != null && status.equals("start")) {
                mAutoTest = true;
                Log.d(TAG, "mAutoTestRunnable mAutoTest = " + mAutoTest);

                startAutoTest();
            } else {
                Log.d(TAG, "check system properties again");
                mHandler.postDelayed(mAutoTestRunnable, Constants.AUTO_TEST_TIME_INTERVAL);
            }

        }

    };

    private String getSystemPropertyAsString(String propertyName) {
        String value = null;

        try {
            Class<?> systemPropertiesClazz = Class.forName("android.os.SystemProperties");
            Method method = systemPropertiesClazz.getMethod("get", String.class);
            value = (String) method.invoke(null, propertyName);

        } catch (ClassNotFoundException e) {
        } catch (NoSuchMethodException e) {
        } catch (IllegalAccessException e) {
        } catch (IllegalArgumentException e) {
        } catch (InvocationTargetException e) {
        }

        return value;
    }

}
