
package com.goodix.gftest;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.GFConfig;
import com.goodix.fingerprint.service.GoodixFingerprintManager;
import com.goodix.fingerprint.service.GoodixFingerprintManager.TestCmdCallback;
import com.goodix.fingerprint.utils.TestResultParser;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";

    private ListView mListView = null;
    private MyAdapter mAdapter = null;
    private GoodixFingerprintManager mGoodixFingerprintManager = null;
    private GFConfig mConfig = null;

    private int[] SUB_ACTIVITY_NAME = {
            R.string.test_sensor,
            R.string.test_bad_point,
            R.string.test_performance,
            R.string.test_spi_performance,
            R.string.test_spi,
            R.string.test_reset_pin,
            R.string.dump_data,
            R.string.test_frr_database,
            R.string.test_about,
    };

    private static final String[] SUB_ACTIVITY = {
            "com.goodix.gftest.PixelTestActivity",
            "com.goodix.gftest.BadPointTestActivity",
            "com.goodix.gftest.PerformanceTestActivity",
            "com.goodix.gftest.SpiPerformanceTestActivity",
            "com.goodix.gftest.SpiTestActivity",
            "com.goodix.gftest.ResetPinTestActivity",
            "com.goodix.gftest.DumpActivity",
            "com.goodix.gftest.FRRDatabaseActivity",
            "com.goodix.gftest.AboutActivity",
    };

    private ArrayList<Integer> mSubActivityNameList = new ArrayList<Integer>();
    private ArrayList<String> mSubActivityList = new ArrayList<String>();

    private boolean mIsSensorValidityTested = false;
    private int mSensorValidityTestFlag = 1;
    private Handler mHandler = new Handler();
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mGoodixFingerprintManager = new GoodixFingerprintManager(MainActivity.this);
        mConfig = mGoodixFingerprintManager.getConfig();

        Intent intent = new Intent();
        int i = 0;
        for (String name : SUB_ACTIVITY) {
            if (mConfig != null && mConfig.mChipSeries == Constants.GF_MILAN_F_SERIES
                    && R.string.test_spi_performance == SUB_ACTIVITY_NAME[i]) {
                i++;
                continue;
            }

            intent.setClassName(MainActivity.this, name);
            if (getPackageManager().resolveActivity(intent, 0) != null) {
                mSubActivityNameList.add(SUB_ACTIVITY_NAME[i]);
                mSubActivityList.add(name);
            }
            i++;
        }

        initView();
        boolean isHardwareDetected = true;
        try {
            Object fingerprintManager = getSystemService("fingerprint");
            Method isHardwareDetectedMethod = fingerprintManager.getClass().getMethod(
                    "isHardwareDetected", new Class[] {});
            isHardwareDetectedMethod.setAccessible(true);
            isHardwareDetected = (Boolean) isHardwareDetectedMethod.invoke(fingerprintManager,
                    new Object[] {});
        } catch (IllegalAccessException e) {
        } catch (IllegalArgumentException e) {
        } catch (InvocationTargetException e) {
            isHardwareDetected = false;
        } catch (NoSuchMethodException e) {
        } catch (Exception e) {
        }

        Log.i(TAG, "isHardwareDetected = " + isHardwareDetected);

        if (!isHardwareDetected) {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle(MainActivity.this.getString(R.string.sytem_info))
                    .setMessage(MainActivity.this.getString(R.string.no_hardware))
                    .setPositiveButton(MainActivity.this.getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                    .show();
        }
        if (null != mConfig && Constants.GF_MILAN_F_SERIES == mConfig.mChipSeries) {
            mDialog = new ProgressDialog(this);
            mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mDialog.setCancelable(true);
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.setMessage(MainActivity.this.getString(R.string.sensor_checking));
            mDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {

                }
            });
            mDialog.show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mGoodixFingerprintManager.registerTestCmdCallback(mTestCmdCallback);

        if (null != mConfig && Constants.GF_MILAN_F_SERIES == mConfig.mChipSeries) {
            if (!mIsSensorValidityTested) {
                mSensorValidityTestFlag = 0;
                mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_SENSOR_VALIDITY, null);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        mGoodixFingerprintManager.unregisterTestCmdCallback(mTestCmdCallback);
    }

    public void initView() {
        mListView = (ListView) findViewById(R.id.listview);
        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (0 == mSensorValidityTestFlag) {
                    return;
                }

                if (position >= mSubActivityList.size()) {
                    return;
                }

                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setClassName(MainActivity.this, mSubActivityList.get(position));
                startActivity(intent);
            }

        });
        mAdapter = new MyAdapter();
        mListView.setAdapter(mAdapter);
    }

    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mSubActivityList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(MainActivity.this).inflate(
                        R.layout.item_main, null);
            }

            TextView text = (TextView) convertView;

            if(mSensorValidityTestFlag == 0) {
                text.setEnabled(false);
            } else {
                text.setEnabled(true);
            }

            if ((null != mConfig && R.string.test_sensor == mSubActivityNameList.get(position))) {
                if (Constants.GF_MILAN_F_SERIES == mConfig.mChipSeries
                        || Constants.GF_MILAN_A_SERIES == mConfig.mChipSeries) {
                    text.setText(R.string.test_pixel_open);
                } else {
                    text.setText(R.string.test_sensor);
                }
            } else {
                text.setText(mSubActivityNameList.get(position));
            }

            return convertView;
        }
    }

    private TestCmdCallback mTestCmdCallback = new TestCmdCallback() {

        @Override
        public void onTestCmd(final int cmdId, final HashMap<Integer, Object> result) {
            Log.d(TAG, "onTestCmd");

            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    switch (cmdId) {
                        case Constants.CMD_TEST_SENSOR_VALIDITY:
                            if (null != mDialog) {
                                mDialog.cancel();
                            }
                            if (result.containsKey(TestResultParser.TEST_TOKEN_SENSOR_VALIDITY)) {
                                mSensorValidityTestFlag = (Integer) result
                                        .get(TestResultParser.TEST_TOKEN_SENSOR_VALIDITY);
                            }

                            mIsSensorValidityTested = true;
                            if (0 == mSensorValidityTestFlag) {
                                Log.i(TAG, "Sensor validity test Fail");
                                new AlertDialog.Builder(MainActivity.this)
                                        .setTitle(MainActivity.this.getString(R.string.sytem_info))
                                        .setMessage(MainActivity.this
                                                .getString(R.string.sensor_validity_test_fail))
                                        .setPositiveButton(MainActivity.this.getString(R.string.ok),
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog,
                                                            int which) {
                                                        // finish();
                                                    }
                                                })
                                        .show();
                            } else {
                                //the trick to update view to gray
                                mAdapter.notifyDataSetChanged();
                                Log.i(TAG, "Sensor validity test Pass");
                            }
                            break;
                    }
                }
            });

        }
    };

}
