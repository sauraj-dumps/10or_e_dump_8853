
package com.goodix.gftest;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.GFConfig;
import com.goodix.fingerprint.service.GoodixFingerprintManager;
import com.goodix.fingerprint.utils.TestResultParser;
import com.goodix.gftest.utils.TestHistoryUtils;
import com.goodix.gftest.utils.TestResultChecker;
import com.goodix.gftest.utils.TestResultChecker.CheckPoint;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class DetailActivity extends Activity {
    private static final String TAG = "DetailActivity";
    private static final String KEY_TEST_ID = "test_id";
    private static final String KEY_TIME = "time";

    private ListView mListView = null;
    private MyAdapter mAdapter = null;
    private ArrayList<HashMap<String, Object>> mData = null;
    private DecimalFormat mDecimalFormat = new DecimalFormat("0.000");

    private GoodixFingerprintManager mGoodixFingerprintManager = null;
    private GFConfig mConfig = null;
    private TestResultChecker.Checker mTestResultChecker = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mGoodixFingerprintManager = new GoodixFingerprintManager(DetailActivity.this);
        mConfig = mGoodixFingerprintManager.getConfig();
        if (mConfig != null) {
            mTestResultChecker = TestResultChecker.TestResultCheckerFactory
                    .getInstance(mConfig.mChipType, mConfig.mChipSeries);
        }

        if (mTestResultChecker == null) {
            return;
        }

        loadData();
        initView();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initView() {
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        mListView = (ListView) findViewById(R.id.listview);
        mAdapter = new MyAdapter();
        mListView.setAdapter(mAdapter);
    }

    private void loadData() {
        mData = TestHistoryUtils.load();
    }

    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mData != null ? mData.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            HashMap<String, Object> item = mData.get(position);
            return Integer.parseInt((String) item.get(KEY_TEST_ID));
        }

        @Override
        public int getViewTypeCount() {
            return TestResultChecker.TEST_MAX;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            switch (getItemViewType(position)) {
                case TestResultChecker.TEST_SPI: {
                    convertView = getTestSpiView(position, convertView, parent);
                    break;
                }

                case TestResultChecker.TEST_PIXEL: {
                    convertView = getTestPixelView(position, convertView, parent);
                    break;
                }

                case TestResultChecker.TEST_RESET_PIN: {
                    convertView = getTestResetPinView(position, convertView, parent);
                    break;
                }

                case TestResultChecker.TEST_BAD_POINT: {
                    convertView = getTestBadPointView(position, convertView, parent);
                    break;
                }

                case TestResultChecker.TEST_PERFORMANCE: {
                    convertView = getTestPerformanceView(position, convertView, parent);
                    break;
                }

                case TestResultChecker.TEST_CAPTURE: {
                    convertView = getTestCaptureView(position, convertView, parent);
                    break;
                }

                case TestResultChecker.TEST_ALGO: {
                    convertView = getTestAlgoView(position, convertView, parent);
                    break;
                }


                case TestResultChecker.TEST_FW_VERSION: {
                    convertView = getTestFwVersion(position, convertView, parent);
                    break;
                }

                case TestResultChecker.TEST_BIO_CALIBRATION:
                    convertView = getTestBioView(position, convertView, parent);
                    break;

                case TestResultChecker.TEST_HBD_CALIBRATION:
                    convertView = getTestHbdView(position, convertView, parent);
                    break;

                default:
                    break;
            }

            return convertView;
        }

        private View getTestSpiView(int position, View convertView, ViewGroup parent) {
            HashMap<String, Object> item = mData.get(position);
            // for (String key : item.keySet()) {
            //     Log.d(TAG, key + " : " + (String) item.get(key));
            // }

            if (convertView == null) {
                convertView = LayoutInflater.from(DetailActivity.this).inflate(
                        R.layout.item_spi_test_detail, null);
            }

            TestResultChecker.Threshold threshold = mTestResultChecker.getThresHold();
            if (threshold == null) {
                Log.e(TAG, "failed to get threshold");
                return convertView;
            }
            int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
            String fwVersion = null;
            int chipID = 0;
            String result = null;
            int costTime = 0;
            int sensor_otp_type = 0;

            // error code view
            View errorCodeLayout = convertView.findViewById(R.id.error_code_layout);
            TextView errorCodeTitle = (TextView) errorCodeLayout.findViewById(R.id.title);
            TextView errorCodeView = (TextView) errorCodeLayout.findViewById(R.id.result);
            TextView errorCodeTipsView = (TextView) errorCodeLayout
                    .findViewById(R.id.success_tips);

            if (item.containsKey("error_code")) {
                errorCode = Integer.parseInt((String) item.get("error_code"));
            }

            errorCodeView.setText(String.valueOf(errorCode));
            if (errorCode == 0) {
                errorCodeLayout.setVisibility(View.GONE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                errorCodeLayout.setVisibility(View.VISIBLE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
                errorCodeTitle.setText(R.string.error_code);
                errorCodeTipsView
                .setText(getResources().getString(R.string.test_success_tips, " == ", 0));
            }

            // spi firmware version
            View fwVersionLayout = convertView.findViewById(R.id.fw_version_layout);

            TextView fwVersionTitle = (TextView) fwVersionLayout.findViewById(R.id.title);
            TextView fwVersionView = (TextView) fwVersionLayout.findViewById(R.id.result);
            TextView fwVersionTipsView = (TextView) fwVersionLayout
                    .findViewById(R.id.success_tips);

            if (item.containsKey("fw_version")
                    && (threshold.spiFwVersion != null && !threshold.spiFwVersion.equals(""))) {
                fwVersion = (String) item.get("fw_version");
                fwVersionLayout.setVisibility(View.VISIBLE);

                if (mConfig != null && mConfig.mChipSeries == Constants.GF_MILAN_A_SERIES) {
                    if (fwVersion == null || !fwVersion.startsWith(threshold.spiFwVersion)) {
                        fwVersionView.setTextColor(getResources()
                                .getColor(R.color.test_failed_color));
                    } else {
                        fwVersionView.setTextColor(getResources()
                                .getColor(R.color.test_succeed_color));

                    }
                    fwVersionTipsView
                    .setText(getResources().getString(R.string.test_success_tips_string,
                            " startsWith ", threshold.spiFwVersion));
                } else {
                    if (fwVersion == null || !fwVersion.startsWith(threshold.spiFwVersion)) {
                        fwVersionView.setTextColor(getResources()
                                .getColor(R.color.test_failed_color));
                    } else {
                        fwVersionView.setTextColor(getResources()
                                .getColor(R.color.test_succeed_color));
                    }
                    fwVersionTipsView
                    .setText(getResources().getString(R.string.test_success_tips_string,
                            " startsWith ", threshold.spiFwVersion));
                }
                fwVersionView.setText(fwVersion);
                fwVersionTitle.setText(R.string.test_spi_fw);

            } else {
                fwVersionLayout.setVisibility(View.GONE);
            }

            // chip id view
            View chipIDLayout = convertView.findViewById(R.id.chip_id_layout);

            TextView chipIDTitle = (TextView) chipIDLayout.findViewById(R.id.chip_id_title);
            TextView chipIDView = (TextView) chipIDLayout.findViewById(R.id.chip_id_result);
            TextView chipIDTipsView = (TextView) chipIDLayout
                    .findViewById(R.id.chip_id_success_tips);

            if (item.containsKey("chip_id") && threshold.chipId != 0) {
                chipID = Integer.parseInt((String) item.get("chip_id")) >> 8;

                chipIDLayout.setVisibility(View.VISIBLE);
                if (chipID == 0 || chipID != threshold.chipId) {
                    chipIDView.setTextColor(getResources()
                            .getColor(R.color.test_failed_color));
                } else {
                    chipIDView.setTextColor(getResources()
                            .getColor(R.color.test_succeed_color));
                }
                chipIDTitle.setText(R.string.test_spi_chip_id);
                chipIDView.setText("0x" + Integer.toHexString(chipID));
                chipIDTipsView
                .setText(getResources().getString(R.string.test_success_tips_float,
                        " == 0x", Integer.toHexString(threshold.chipId)));
            } else {
                chipIDLayout.setVisibility(View.GONE);
            }

            TextView resultView = (TextView) convertView.findViewById(R.id.test_result);
            if (item.containsKey("result")) {
                result = (String) item.get("result");
            }
            if(item.containsKey("sensor_otp_type")){
                sensor_otp_type = Integer.parseInt((String) item.get("sensor_otp_type"));
            }

            if (mTestResultChecker.checkSpiTestResult(errorCode, fwVersion, chipID, sensor_otp_type)) {
                resultView.setText(R.string.test_succeed);
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                if (result != null) {
                    if (result.equals("TIMEOUT")) {
                        resultView.setText(R.string.timeout);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else if (result.equals("CANCELED")) {
                        resultView.setText(R.string.canceled);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else {
                        resultView.setText(R.string.test_failed);
                    }
                } else {
                    resultView.setText(R.string.test_failed);
                }
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            // cost time view
            TextView costTimeView = (TextView) convertView
                    .findViewById(R.id.cost_time);
            if (costTimeView != null) {
                if (item.containsKey(KEY_TIME)) {
                    costTimeView.setVisibility(View.VISIBLE);
                    costTime = Integer.parseInt((String) item.get(KEY_TIME));
                    costTimeView.setText(String.valueOf(costTime));
                } else {
                    costTimeView.setVisibility(View.GONE);
                }
            }

            return convertView;
        }

        private View getTestPixelView(int position, View convertView, ViewGroup parent) {
            HashMap<String, Object> item = mData.get(position);
            // for (Object key : item.keySet()) {
            //     Log.d(TAG, key + " : " + (String) item.get(key));
            // }

            if (convertView == null) {
                convertView = LayoutInflater.from(DetailActivity.this).inflate(
                        R.layout.item_pixel_test_detail, null);
            }
            TestResultChecker.Threshold threshold = mTestResultChecker.getThresHold();
            if (threshold == null) {
                Log.e(TAG, "failed to get threshold");
                return convertView;
            }

            int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
            int badPixelNum = 999;
            String result = null;
            int costTime = 0;

            // errcode view
            View errorCodeLayout = convertView.findViewById(R.id.error_code_layout);

            TextView errorCodeTitle = (TextView) errorCodeLayout.findViewById(R.id.title);
            TextView errorCodeView = (TextView) errorCodeLayout.findViewById(R.id.result);
            TextView errorCodeTipsView = (TextView) errorCodeLayout
                    .findViewById(R.id.success_tips);

            errorCodeTitle.setText(R.string.error_code);
            errorCodeTipsView.setText(getResources().getString(R.string.test_success_tips,
                    " == ", 0));

            if (item.containsKey("error_code")) {
                errorCode = Integer.parseInt((String) item.get("error_code"));
            }

            errorCodeView.setText(String.valueOf(errorCode));
            if (errorCode == 0) {
                errorCodeLayout.setVisibility(View.GONE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                errorCodeLayout.setVisibility(View.VISIBLE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            // bad pixel layout
            View badPixelNumLayout = convertView.findViewById(R.id.bad_pixel_num_layout);
            TextView badPixelNumTitle = (TextView) badPixelNumLayout
                    .findViewById(R.id.title);
            TextView badPixelNumView = (TextView) badPixelNumLayout
                    .findViewById(R.id.result);
            TextView badPixelNumTipsView = (TextView) badPixelNumLayout
                    .findViewById(R.id.success_tips);

            if (item.containsKey("bad_pixel_num")) {
                badPixelNum = Integer.parseInt((String) item.get("bad_pixel_num"));
                badPixelNumLayout.setVisibility(View.VISIBLE);

                if (badPixelNum <= threshold.badBointNum) {
                    badPixelNumView.setTextColor(getResources()
                            .getColor(R.color.test_succeed_color));
                } else {
                    badPixelNumView.setTextColor(getResources()
                            .getColor(R.color.test_failed_color));
                }
                badPixelNumTitle.setText(R.string.test_sensor_bad_pixel_num);
                badPixelNumView.setText(String.valueOf(badPixelNum));
                badPixelNumTipsView.setText(getResources().getString(
                        R.string.test_success_tips,
                        " <= ", threshold.badBointNum));
            } else {
                badPixelNumLayout.setVisibility(View.GONE);
            }

            if (item.containsKey("result")) {
                result = (String) item.get("result");
            }

            TextView resultView = (TextView) convertView.findViewById(R.id.test_result);

            if (mTestResultChecker.checkPixelTestResult(errorCode, badPixelNum)) {
                resultView.setText(R.string.test_succeed);
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                if (result != null) {
                    if (result.equals("TIMEOUT")) {
                        resultView.setText(R.string.timeout);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else if (result.equals("CANCELED")) {
                        resultView.setText(R.string.canceled);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else {
                        resultView.setText(R.string.test_failed);
                    }
                } else {
                    resultView.setText(R.string.test_failed);
                }
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            // cost time view
            TextView costTimeView = (TextView) convertView
                    .findViewById(R.id.cost_time);
            if (costTimeView != null) {
                if (item.containsKey(KEY_TIME)) {
                    costTimeView.setVisibility(View.VISIBLE);
                    costTime = Integer.parseInt((String) item.get(KEY_TIME));
                    costTimeView.setText(String.valueOf(costTime));
                } else {
                    costTimeView.setVisibility(View.GONE);
                }
            }

            return convertView;
        }

        private View getTestResetPinView(int position, View convertView, ViewGroup parent) {
            HashMap<String, Object> item = mData.get(position);
            // for (Object key : item.keySet()) {
            //     Log.d(TAG, key + " : " + (String) item.get(key));
            // }

            if (convertView == null) {
                convertView = LayoutInflater.from(DetailActivity.this).inflate(
                        R.layout.item_reset_pin_test_detail, null);
            }
            int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
            String result = null;
            int resetFlag = 0;
            int costTime = 0;

            // ErrCode Layout
            View errorCodeLayout = convertView.findViewById(R.id.error_code_layout);
            TextView errorCodeTitle = (TextView) errorCodeLayout.findViewById(R.id.title);
            TextView errorCodeView = (TextView) errorCodeLayout.findViewById(R.id.result);
            TextView errorCodeTipsView = (TextView) errorCodeLayout
                    .findViewById(R.id.success_tips);

            errorCodeTitle.setText(R.string.error_code);
            errorCodeTipsView.setText(getResources().getString(R.string.test_success_tips,
                    " == ", 0));

            if (item.containsKey("error_code")) {
                errorCode = Integer.parseInt((String) item.get("error_code"));
            }

            errorCodeView.setText(String.valueOf(errorCode));
            if (errorCode == 0) {
                errorCodeLayout.setVisibility(View.GONE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                errorCodeLayout.setVisibility(View.VISIBLE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            // reset flag
            if (item.containsKey("reset_flag")) {
                resetFlag = Integer.parseInt((String) item.get("reset_flag"));
            }

            // result layout
            TextView resultView = (TextView) convertView.findViewById(R.id.test_result);
            if (item.containsKey("result")) {
                result = (String) item.get("result");
            }

            if (mTestResultChecker.checkResetPinTestReuslt(errorCode, resetFlag)) {
                resultView.setText(R.string.test_succeed);
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                if (result != null) {
                    if (result.equals("TIMEOUT")) {
                        resultView.setText(R.string.timeout);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else if (result.equals("CANCELED")) {
                        resultView.setText(R.string.canceled);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else {
                        resultView.setText(R.string.test_failed);
                    }
                } else {
                    resultView.setText(R.string.test_failed);
                }
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            // cost time view
            TextView costTimeView = (TextView) convertView
                    .findViewById(R.id.cost_time);
            if (costTimeView != null) {
                if (item.containsKey(KEY_TIME)) {
                    costTimeView.setVisibility(View.VISIBLE);
                    costTime = Integer.parseInt((String) item.get(KEY_TIME));
                    costTimeView.setText(String.valueOf(costTime));
                } else {
                    costTimeView.setVisibility(View.GONE);
                }
            }
            return convertView;
        }

        private View getTestBadPointView(int position, View convertView, ViewGroup parent) {
            HashMap<String, Object> item = mData.get(position);
            // for (Object key : item.keySet()) {
            //     Log.d(TAG, key + " : " + (String) item.get(key));
            // }

            if (convertView == null) {
                convertView = LayoutInflater.from(DetailActivity.this).inflate(
                        R.layout.item_bad_point_test_detail, null);
            }
            TestResultChecker.Threshold threshold = mTestResultChecker.getThresHold();
            if (threshold == null) {
                Log.e(TAG, "failed to get threshold");
                return convertView;
            }

            CheckPoint checkPoint = new CheckPoint();
            checkPoint.mErrorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
            checkPoint.mAvgDiffVal = 0;
            checkPoint.mBadPixelNum = 0;
            checkPoint.mLocalBadPixelNum = 0;
            checkPoint.mAllTiltAngle = 0;
            checkPoint.mBlockTiltAngleMax = 0;
            checkPoint.mLocalWorst = 0;
            checkPoint.mSingular = 0;
            checkPoint.mInCircle = 0;
            int costTime = 0;

            TextView resultView = (TextView) convertView.findViewById(R.id.test_result);

            // bad pixelNum layout
            View badPixelNumLayout = convertView.findViewById(R.id.bad_pixel_num_layout);
            TextView badPixelNumTitle = (TextView) badPixelNumLayout.findViewById(R.id.title);
            TextView badPixelNumView = (TextView) badPixelNumLayout.findViewById(R.id.result);
            TextView badPixelNumTipsView = (TextView) badPixelNumLayout
                    .findViewById(R.id.success_tips);

            if (item.containsKey("bad_pixel_num") && threshold.badPixelNum != 0) {
                checkPoint.mBadPixelNum = Integer.parseInt((String) item.get("bad_pixel_num"));
                badPixelNumLayout.setVisibility(View.VISIBLE);

                badPixelNumTitle.setText(R.string.bad_pixel_num);
                badPixelNumView.setText(String.valueOf(checkPoint.mBadPixelNum));
                badPixelNumTipsView.setText(getResources().getString(
                        R.string.test_success_tips, " < ", threshold.badPixelNum));

                if (checkPoint.mBadPixelNum < threshold.badPixelNum) {
                    badPixelNumView.setTextColor(getResources()
                            .getColor(R.color.test_succeed_color));
                } else {
                    badPixelNumView.setTextColor(getResources()
                            .getColor(R.color.test_failed_color));
                }
            } else {
                badPixelNumLayout.setVisibility(View.GONE);
            }

            // local badPixelNum layout
            View localBadPixelNumLayout = convertView.findViewById(R.id.local_bad_pixel_num_layout);
            TextView localBadPixelNumTitle = (TextView) localBadPixelNumLayout
                    .findViewById(R.id.title);
            TextView localBadPixelNumView = (TextView) localBadPixelNumLayout
                    .findViewById(R.id.result);
            TextView localBadPixelNumTipsView = (TextView) localBadPixelNumLayout
                    .findViewById(R.id.success_tips);

            if (item.containsKey("local_bad_pixel_num") && threshold.localBadPixelNum != 0) {
                checkPoint.mLocalBadPixelNum = Integer.parseInt((String) item
                        .get("local_bad_pixel_num"));
                localBadPixelNumLayout.setVisibility(View.VISIBLE);

                localBadPixelNumTitle.setText(R.string.local_bad_pixel);
                localBadPixelNumView.setText(String.valueOf(checkPoint.mLocalBadPixelNum));
                localBadPixelNumTipsView.setText(getResources().getString(
                        R.string.test_success_tips, " < ", threshold.localBadPixelNum));

                if (checkPoint.mLocalBadPixelNum < threshold.localBadPixelNum) {
                    localBadPixelNumView.setTextColor(getResources()
                            .getColor(R.color.test_succeed_color));
                } else {
                    localBadPixelNumView.setTextColor(getResources()
                            .getColor(R.color.test_failed_color));
                }
            } else {
                localBadPixelNumLayout.setVisibility(View.GONE);
            }

            // avg diffVal Layout
            View avgDiffValLayout = convertView.findViewById(R.id.avg_diff_val_layout);
            TextView avgDiffValTitle = (TextView) avgDiffValLayout
                    .findViewById(R.id.title);
            TextView avgDiffValView = (TextView) avgDiffValLayout
                    .findViewById(R.id.result);
            TextView avgDiffValTipsView = (TextView) avgDiffValLayout
                    .findViewById(R.id.success_tips);

            if (item.containsKey("avg_diff_val") && threshold.avgDiffVal != 0) {
                checkPoint.mAvgDiffVal = Short.parseShort((String) item.get("avg_diff_val"));
                avgDiffValLayout.setVisibility(View.VISIBLE);

                avgDiffValTitle.setText(R.string.avg_diff_val);
                avgDiffValTipsView.setText(getResources().getString(R.string.test_success_tips,
                        " > ", threshold.avgDiffVal));
                avgDiffValView.setText(String.valueOf(checkPoint.mAvgDiffVal));
                if (checkPoint.mAvgDiffVal > threshold.avgDiffVal) {
                    avgDiffValView.setTextColor(getResources()
                            .getColor(R.color.test_succeed_color));
                } else {
                    avgDiffValView.setTextColor(getResources()
                            .getColor(R.color.test_failed_color));
                }

            } else {
                avgDiffValLayout.setVisibility(View.GONE);
            }

            // allTiltAngle Layout
            View allTiltAngleLayout = convertView.findViewById(R.id.all_tilt_angle_layout);
            TextView allTiltAngleTitle = (TextView) allTiltAngleLayout
                    .findViewById(R.id.title);
            TextView allTiltAngleView = (TextView) allTiltAngleLayout
                    .findViewById(R.id.result);
            TextView allTiltAngleTipsView = (TextView) allTiltAngleLayout
                    .findViewById(R.id.success_tips);
            if (item.containsKey("all_tilt_angle") && threshold.allTiltAngle != 0) {
                checkPoint.mAllTiltAngle = Float.parseFloat((String) item.get("all_tilt_angle"));
                allTiltAngleLayout.setVisibility(View.VISIBLE);

                allTiltAngleTitle.setText(R.string.all_tilt_angle);
                allTiltAngleTipsView.setText(getResources().getString(
                        R.string.test_success_tips_float, " < ",
                        mDecimalFormat.format(threshold.allTiltAngle)));

                allTiltAngleView.setText(mDecimalFormat.format(checkPoint.mAllTiltAngle));
                if (checkPoint.mAllTiltAngle < threshold.allTiltAngle) {
                    allTiltAngleView.setTextColor(getResources()
                            .getColor(R.color.test_succeed_color));
                } else {
                    allTiltAngleView.setTextColor(getResources()
                            .getColor(R.color.test_failed_color));
                }
            } else {
                allTiltAngleLayout.setVisibility(View.GONE);
            }

            // blockTitlAngleMax Layout
            View blockTiltAngleMaxLayout = convertView
                    .findViewById(R.id.block_tilt_angle_max_layout);
            TextView blockTiltAngleMaxTitle = (TextView) blockTiltAngleMaxLayout
                    .findViewById(R.id.title);
            TextView blockTiltAngleMaxView = (TextView) blockTiltAngleMaxLayout
                    .findViewById(R.id.result);
            TextView blockTiltAngleMaxTipsView = (TextView) blockTiltAngleMaxLayout
                    .findViewById(R.id.success_tips);

            if (item.containsKey("block_tilt_angle_max") && threshold.blockTiltAngleMax != 0) {
                checkPoint.mBlockTiltAngleMax = Float.parseFloat((String) item
                        .get("block_tilt_angle_max"));
                blockTiltAngleMaxLayout.setVisibility(View.VISIBLE);

                blockTiltAngleMaxTitle.setText(R.string.block_tilt_angle_max);
                blockTiltAngleMaxTipsView.setText(getResources().getString(
                        R.string.test_success_tips_float, " < ",
                        mDecimalFormat.format(threshold.blockTiltAngleMax)));

                blockTiltAngleMaxView.setText(mDecimalFormat.format(checkPoint.mBlockTiltAngleMax));
                if (checkPoint.mBlockTiltAngleMax < Constants.Oswego.TEST_BAD_POINT_BLOCK_TILT_ANGLE_MAX) {
                    blockTiltAngleMaxView.setTextColor(getResources()
                            .getColor(R.color.test_succeed_color));
                } else {
                    blockTiltAngleMaxView.setTextColor(getResources()
                            .getColor(R.color.test_failed_color));
                }

            } else {
                blockTiltAngleMaxLayout.setVisibility(View.GONE);
            }

            // localWorstLayout
            View localWorstLayout = convertView.findViewById(R.id.local_worst_layout);
            TextView localWorstTitle = (TextView) localWorstLayout.findViewById(R.id.title);
            TextView localWorstView = (TextView) localWorstLayout.findViewById(R.id.result);
            TextView localWorstTipsView = (TextView) localWorstLayout
                    .findViewById(R.id.success_tips);

            if (item.containsKey("local_worst") && threshold.localWorst != 0) {
                checkPoint.mLocalWorst = Short.parseShort((String) item.get("local_worst"));
                localWorstLayout.setVisibility(View.VISIBLE);

                // localWorst
                localWorstTitle.setText(R.string.local_worst);
                localWorstView.setText(String.valueOf(checkPoint.mLocalWorst));
                localWorstTipsView.setText(getResources().getString(R.string.test_success_tips,
                        " < ", threshold.localWorst));
                if (checkPoint.mLocalWorst < threshold.localWorst) {
                    localWorstView.setTextColor(getResources().getColor(
                            R.color.test_succeed_color));
                } else {
                    localWorstView.setTextColor(getResources().getColor(
                            R.color.test_failed_color));
                }
            } else {
                localWorstLayout.setVisibility(View.GONE);
            }

            // singularLayout
            View singularLayout = convertView.findViewById(R.id.singular_layout);
            TextView singularTitle = (TextView) singularLayout.findViewById(R.id.title);
            TextView singularView = (TextView) singularLayout.findViewById(R.id.result);
            TextView singularTipsView = (TextView) singularLayout
                    .findViewById(R.id.success_tips);
            if (item.containsKey("singular") && threshold.singular != 0) {
                checkPoint.mSingular = Integer.parseInt((String) item.get("singular"));
                singularLayout.setVisibility(View.VISIBLE);

                singularTitle.setText(R.string.singular);
                singularView.setText(String.valueOf(checkPoint.mSingular));
                singularTipsView.setText(getResources().getString(R.string.test_success_tips, " < ",
                        threshold.singular));
                if (checkPoint.mSingular < threshold.singular) {
                    singularView.setTextColor(getResources().getColor(
                            R.color.test_succeed_color));
                } else {
                    singularView.setTextColor(getResources().getColor(
                            R.color.test_failed_color));
                }
            } else {
                singularLayout.setVisibility(View.GONE);
            }

            // inCircle Layout
            View inCircleLayout = convertView.findViewById(R.id.incircle_layout);
            TextView inCircleTitle = (TextView) inCircleLayout.findViewById(R.id.title);
            TextView inCircleView = (TextView) inCircleLayout.findViewById(R.id.result);
            TextView inCircleTipsView = (TextView) inCircleLayout
                    .findViewById(R.id.success_tips);
            inCircleLayout.setVisibility(View.GONE);
            if (item.containsKey("in_circle") && threshold.inCircle != 0) {
                checkPoint.mInCircle = Short.parseShort((String) item.get("in_circle"));
                inCircleLayout.setVisibility(View.VISIBLE);

                inCircleTitle.setText(R.string.incircle);
                inCircleView.setText(String.valueOf(checkPoint.mInCircle));
                inCircleTipsView.setText(getResources().getString(R.string.test_success_tips, " < ",
                        threshold.inCircle));
                if (checkPoint.mInCircle < threshold.inCircle) {
                    inCircleView.setTextColor(getResources().getColor(
                            R.color.test_succeed_color));
                } else {
                    inCircleView.setTextColor(getResources().getColor(
                            R.color.test_failed_color));
                }
            } else {
                inCircleLayout.setVisibility(View.GONE);
            }

            // small bad pixel Layout
            View smallBadPixelLayout = convertView.findViewById(R.id.local_small_bad_pixel_layout);
            TextView smallBadPixelTitle = (TextView) smallBadPixelLayout.findViewById(R.id.title);
            TextView smallBadPixelView = (TextView) smallBadPixelLayout.findViewById(R.id.result);
            TextView smallBadPixelTipsView = (TextView) smallBadPixelLayout
                    .findViewById(R.id.success_tips);
            smallBadPixelLayout.setVisibility(View.GONE);
            if (item.containsKey(TestResultParser.TEST_TOKEN_LOCAL_SMALL_BAD_PIXEL_NUM) && threshold.inCircle != 0) {
                checkPoint.mSmallBadPixel = (Integer)item.get(TestResultParser.TEST_TOKEN_LOCAL_SMALL_BAD_PIXEL_NUM);
                smallBadPixelLayout.setVisibility(View.VISIBLE);

                smallBadPixelTitle.setText(R.string.local_small_bad_pixel);
                smallBadPixelView.setText(String.valueOf(checkPoint.mInCircle));
                smallBadPixelTipsView.setText(getResources().getString(R.string.test_success_tips, " < ",
                        threshold.inCircle));
                if (checkPoint.mSmallBadPixel < threshold.localSmallBadPixel) {
                    smallBadPixelView.setTextColor(getResources().getColor(
                            R.color.test_succeed_color));
                } else {
                    smallBadPixelView.setTextColor(getResources().getColor(
                            R.color.test_failed_color));
                }
            } else {
                smallBadPixelLayout.setVisibility(View.GONE);
            }

            // big bad pixel Layout
            View bigBadPixelLayout = convertView.findViewById(R.id.local_big_bad_pixel_layout);
            TextView bigBadPixelTitle = (TextView) inCircleLayout.findViewById(R.id.title);
            TextView bigBadPixelView = (TextView) inCircleLayout.findViewById(R.id.result);
            TextView bigBadPixelTipsView = (TextView) inCircleLayout
                    .findViewById(R.id.success_tips);
            bigBadPixelLayout.setVisibility(View.GONE);
            if (item.containsKey(TestResultParser.TEST_TOKEN_LOCAL_BIG_BAD_PIXEL_NUM) && threshold.inCircle != 0) {
                checkPoint.mBigBadPixel = (Integer) item.get(TestResultParser.TEST_TOKEN_LOCAL_BIG_BAD_PIXEL_NUM);
                bigBadPixelLayout.setVisibility(View.VISIBLE);

                bigBadPixelTitle.setText(R.string.local_big_bad_pixel);
                bigBadPixelView.setText(String.valueOf(checkPoint.mBigBadPixel));
                bigBadPixelTipsView.setText(getResources().getString(R.string.test_success_tips, " < ",
                        threshold.localBigBadPixel));
                if (checkPoint.mBigBadPixel < threshold.localBigBadPixel) {
                    bigBadPixelView.setTextColor(getResources().getColor(
                            R.color.test_succeed_color));
                } else {
                    bigBadPixelView.setTextColor(getResources().getColor(
                            R.color.test_failed_color));
                }
            } else {
                bigBadPixelLayout.setVisibility(View.GONE);
            }

            // errcode layout
            View errorCodeLayout = convertView.findViewById(R.id.error_code_layout);
            TextView errorCodeTitle = (TextView) errorCodeLayout.findViewById(R.id.title);
            TextView errorCodeView = (TextView) errorCodeLayout.findViewById(R.id.result);
            TextView errorCodeTipsView = (TextView) errorCodeLayout
                    .findViewById(R.id.success_tips);

            errorCodeTitle.setText(R.string.error_code);
            errorCodeTipsView.setText(getResources().getString(R.string.test_success_tips,
                    " == ", 0));

            if (item.containsKey("error_code")) {
                checkPoint.mErrorCode = Integer.parseInt((String) item.get("error_code"));
            }

            errorCodeView.setText(String.valueOf(checkPoint.mErrorCode));
            if (checkPoint.mErrorCode == 0) {
                errorCodeLayout.setVisibility(View.GONE);
                errorCodeView.setTextColor(getResources().getColor(R.color.test_succeed_color));
            } else {
                errorCodeLayout.setVisibility(View.VISIBLE);
                errorCodeView.setTextColor(getResources().getColor(R.color.test_failed_color));
            }

            String result = null;
            if (item.containsKey("result")) {
                result = (String) item.get("result");
            }

            if (mTestResultChecker.checkBadPointTestResult(checkPoint)) {
                resultView.setText(R.string.test_succeed);
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                if (result != null) {
                    if (result.equals("TIMEOUT")) {
                        resultView.setText(R.string.timeout);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else if (result.equals("CANCELED")) {
                        resultView.setText(R.string.canceled);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else {
                        resultView.setText(R.string.test_failed);
                    }
                } else {
                    resultView.setText(R.string.test_failed);
                }
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            // cost time view
            TextView costTimeView = (TextView) convertView
                    .findViewById(R.id.cost_time);
            if (costTimeView != null) {
                if (item.containsKey(KEY_TIME)) {
                    costTimeView.setVisibility(View.VISIBLE);
                    costTime = Integer.parseInt((String) item.get(KEY_TIME));
                    costTimeView.setText(String.valueOf(costTime));
                } else {
                    costTimeView.setVisibility(View.GONE);
                }
            }

            return convertView;
        }

        private View getTestPerformanceView(int position, View convertView, ViewGroup parent) {
            HashMap<String, Object> item = mData.get(position);
            // for (String key : item.keySet()) {
            //      Log.d(TAG, key + " : " + (String) item.get(key));
            // }

            if (convertView == null) {
                convertView = LayoutInflater.from(DetailActivity.this).inflate(
                        R.layout.item_performance_test_detail, null);
            }

            TestResultChecker.Threshold threshold = mTestResultChecker.getThresHold();
            if (threshold == null) {
                Log.e(TAG, "failed to get threshold");
                return convertView;
            }

            int totalTime = 999;
            int costTime = 0;

            // total time layout
            View totalTimeLayout = convertView.findViewById(R.id.total_time_layout);
            TextView totalTimeTitle = (TextView) totalTimeLayout.findViewById(R.id.title);
            TextView totalTimeView = (TextView) totalTimeLayout.findViewById(R.id.result);
            TextView totalTimeTipsView = (TextView) totalTimeLayout.findViewById(R.id.success_tips);

            if (item.containsKey("total_time")) {
                totalTime = Integer.parseInt((String) item.get("total_time"));
                totalTimeLayout.setVisibility(View.VISIBLE);

                if (totalTime <= threshold.totalTime) {
                    totalTimeView.setTextColor(getResources()
                            .getColor(R.color.test_succeed_color));
                } else {
                    totalTimeView.setTextColor(getResources()
                            .getColor(R.color.test_failed_color));
                }

                totalTimeTitle.setText(R.string.total_time);
                totalTimeView.setText(String.valueOf(totalTime));
                totalTimeTipsView.setText(
                        getResources().getString(R.string.test_success_tips, " <= ",
                                threshold.totalTime));
            } else {
                totalTimeLayout.setVisibility(View.GONE);
            }

            // error code layout
            View errorCodeLayout = convertView.findViewById(R.id.error_code_layout);
            TextView errorCodeTitle = (TextView) errorCodeLayout.findViewById(R.id.title);
            TextView errorCodeView = (TextView) errorCodeLayout.findViewById(R.id.result);
            TextView errorCodeTipsView = (TextView) errorCodeLayout.findViewById(R.id.success_tips);
            errorCodeTitle.setText(R.string.error_code);
            errorCodeTipsView.setText(getResources().getString(R.string.test_success_tips,
                    " == ", 0));

            int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
            if (item.containsKey("error_code")) {
                errorCode = Integer.parseInt((String) item.get("error_code"));
            }

            errorCodeView.setText(String.valueOf(errorCode));
            if (errorCode == 0) {
                errorCodeLayout.setVisibility(View.GONE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                errorCodeLayout.setVisibility(View.VISIBLE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            // result layout
            TextView resultView = (TextView) convertView.findViewById(R.id.test_result);
            String result = null;
            if (item.containsKey("result")) {
                result = (String) item.get("result");
            }

            if (mTestResultChecker.checkPerformanceTestResult(errorCode, totalTime)) {
                resultView.setText(R.string.test_succeed);
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                if (result != null) {
                    if (result.equals("TIMEOUT")) {
                        resultView.setText(R.string.timeout);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else if (result.equals("CANCELED")) {
                        resultView.setText(R.string.canceled);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else {
                        resultView.setText(R.string.test_failed);
                    }
                } else {
                    resultView.setText(R.string.test_failed);
                }
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            // cost time view
            TextView costTimeView = (TextView) convertView
                    .findViewById(R.id.cost_time);
            if (costTimeView != null) {
                if (item.containsKey(KEY_TIME)) {
                    costTimeView.setVisibility(View.VISIBLE);
                    costTime = Integer.parseInt((String) item.get(KEY_TIME));
                    costTimeView.setText(String.valueOf(costTime));
                } else {
                    costTimeView.setVisibility(View.GONE);
                }
            }

            return convertView;
        }

        private View getTestCaptureView(int position, View convertView, ViewGroup parent) {
            HashMap<String, Object> item = mData.get(position);
            // for (String key : item.keySet()) {
            //     Log.d(TAG, key + " : " + (String) item.get(key));
            // }

            if (convertView == null) {
                convertView = LayoutInflater.from(DetailActivity.this).inflate(
                        R.layout.item_capture_test_detail, null);
            }

            TestResultChecker.Threshold threshold = mTestResultChecker.getThresHold();
            if (threshold == null) {
                Log.e(TAG, "failed to get threshold");
                return convertView;
            }

            int imageQuality = 0;
            int validArea = 0;
            int costTime = 0;

            // image quality layout
            View imageQualityLayout = convertView.findViewById(R.id.image_quality_layout);
            TextView imageQualityTitle = (TextView) imageQualityLayout.findViewById(R.id.title);
            TextView imageQualityView = (TextView) imageQualityLayout.findViewById(R.id.result);
            TextView imageQualityTipsView = (TextView) imageQualityLayout
                    .findViewById(R.id.success_tips);

            if (item.containsKey("image_quality")) {
                imageQuality = Integer.parseInt((String) item.get("image_quality"));
                imageQualityLayout.setVisibility(View.VISIBLE);

                if (imageQuality >= threshold.imageQuality) {
                    imageQualityView.setTextColor(getResources()
                            .getColor(R.color.test_succeed_color));
                } else {
                    imageQualityView.setTextColor(getResources()
                            .getColor(R.color.test_failed_color));
                }
                imageQualityTitle.setText(R.string.image_quality);
                imageQualityView.setText(String.valueOf(imageQuality));
                imageQualityTipsView.setText(getResources().getString(
                        R.string.test_success_tips, " >= ", threshold.imageQuality));
            } else {
                imageQualityLayout.setVisibility(View.GONE);
            }

            // valid area layout
            View validAreaLayout = convertView.findViewById(R.id.valid_area_layout);
            TextView validAreaTitle = (TextView) validAreaLayout.findViewById(R.id.title);
            TextView validAreaView = (TextView) validAreaLayout.findViewById(R.id.result);
            TextView validAreaTipsView = (TextView) validAreaLayout.findViewById(R.id.success_tips);
            if (item.containsKey("valid_area")) {
                validArea = Integer.parseInt((String) item.get("valid_area"));
                validAreaLayout.setVisibility(View.VISIBLE);

                if (validArea >= threshold.validArea) {
                    validAreaView.setTextColor(getResources()
                            .getColor(R.color.test_succeed_color));
                } else {
                    validAreaView.setTextColor(getResources()
                            .getColor(R.color.test_failed_color));
                }

                validAreaView.setText(String.valueOf(validArea));
                validAreaTitle.setText(R.string.valid_area);
                validAreaTipsView.setText(getResources().getString(R.string.test_success_tips,
                        " >= ", threshold.validArea));
            } else {
                validAreaLayout.setVisibility(View.GONE);
            }

            // error code layout
            View errorCodeLayout = convertView.findViewById(R.id.error_code_layout);
            TextView errorCodeTitle = (TextView) errorCodeLayout.findViewById(R.id.title);
            TextView errorCodeView = (TextView) errorCodeLayout.findViewById(R.id.result);
            TextView errorCodeTipsView = (TextView) errorCodeLayout
                    .findViewById(R.id.success_tips);

            errorCodeTitle.setText(R.string.error_code);
            errorCodeTipsView.setText(getResources().getString(R.string.test_success_tips,
                    " == ", 0));

            int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
            if (item.containsKey("error_code")) {
                errorCode = Integer.parseInt((String) item.get("error_code"));
            }

            errorCodeView.setText(String.valueOf(errorCode));
            if (errorCode == 0) {
                errorCodeLayout.setVisibility(View.GONE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                errorCodeLayout.setVisibility(View.VISIBLE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            // result layout
            TextView resultView = (TextView) convertView.findViewById(R.id.test_result);
            String result = null;
            if (item.containsKey("result")) {
                result = (String) item.get("result");
            }

            if (mTestResultChecker.checkCaptureTestResult(errorCode, imageQuality, validArea)) {
                resultView.setText(R.string.test_succeed);
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                if (result != null) {
                    if (result.equals("TIMEOUT")) {
                        resultView.setText(R.string.timeout);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else if (result.equals("CANCELED")) {
                        resultView.setText(R.string.canceled);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else {
                        resultView.setText(R.string.test_failed);
                    }
                } else {
                    resultView.setText(R.string.test_failed);
                }
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            // cost time view
            TextView costTimeView = (TextView) convertView
                    .findViewById(R.id.cost_time);
            if (costTimeView != null) {
                if (item.containsKey(KEY_TIME)) {
                    costTimeView.setVisibility(View.VISIBLE);
                    costTime = Integer.parseInt((String) item.get(KEY_TIME));
                    costTimeView.setText(String.valueOf(costTime));
                } else {
                    costTimeView.setVisibility(View.GONE);
                }
            }

            return convertView;
        }

        private View getTestAlgoView(int position, View convertView, ViewGroup parent) {
            HashMap<String, Object> item = mData.get(position);
            // for (String key : item.keySet()) {
            //     Log.d(TAG, key + " : " + (String) item.get(key));
            // }

            if (convertView == null) {
                convertView = LayoutInflater.from(DetailActivity.this).inflate(
                        R.layout.item_algo_test_detail, null);
            }

            TestResultChecker.Threshold threshold = mTestResultChecker.getThresHold();
            if (threshold == null) {
                Log.e(TAG, "failed to get threshold");
                return convertView;
            }

            int costTime = 0;

            // error code layout
            View errorCodeLayout = convertView.findViewById(R.id.error_code_layout);
            TextView errorCodeTitle = (TextView) errorCodeLayout.findViewById(R.id.title);
            TextView errorCodeView = (TextView) errorCodeLayout.findViewById(R.id.result);
            TextView errorCodeTipsView = (TextView) errorCodeLayout
                    .findViewById(R.id.success_tips);

            errorCodeTitle.setText(R.string.error_code);
            errorCodeTipsView.setText(getResources().getString(R.string.test_success_tips,
                    " == ", 0));

            int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
            if (item.containsKey("error_code")) {
                errorCode = Integer.parseInt((String) item.get("error_code"));
            }

            errorCodeView.setText(String.valueOf(errorCode));
            if (errorCode == 0) {
                errorCodeLayout.setVisibility(View.GONE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                errorCodeLayout.setVisibility(View.VISIBLE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            String result = null;
            if (item.containsKey("result")) {
                result = (String) item.get("result");
            }

            // result layout
            TextView resultView = (TextView) convertView.findViewById(R.id.test_result);
            if (mTestResultChecker.checkAlgoTestResult(errorCode)) {
                resultView.setText(R.string.test_succeed);
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                if (result != null) {
                    if (result.equals("TIMEOUT")) {
                        resultView.setText(R.string.timeout);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else if (result.equals("CANCELED")) {
                        resultView.setText(R.string.canceled);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else {
                        resultView.setText(R.string.test_failed);
                    }
                } else {
                    resultView.setText(R.string.test_failed);
                }
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            // cost time view
            TextView costTimeView = (TextView) convertView
                    .findViewById(R.id.cost_time);
            if (costTimeView != null) {
                if (item.containsKey(KEY_TIME)) {
                    costTimeView.setVisibility(View.VISIBLE);
                    costTime = Integer.parseInt((String) item.get(KEY_TIME));
                    costTimeView.setText(String.valueOf(costTime));
                } else {
                    costTimeView.setVisibility(View.GONE);
                }
            }

            return convertView;
        }

        private View getTestFwVersion(int position, View convertView, ViewGroup parent) {
            HashMap<String, Object> item = mData.get(position);
            for (String key : item.keySet()) {
                Log.d(TAG, key + " : " + (String) item.get(key));
            }

            if (convertView == null) {
                convertView = LayoutInflater.from(DetailActivity.this).inflate(
                        R.layout.item_fw_version_test_detail, null);
            }

            TestResultChecker.Threshold threshold = mTestResultChecker.getThresHold();
            if (threshold == null) {
                Log.e(TAG, "failed to get threshold");
                return convertView;
            }

            int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
            String fwVersion = null;
            String codeFwVersion = null;
            int costTime = 0;
            int sensorOtpType = 0;

            if (item.containsKey("error_code")) {
                errorCode = Integer.parseInt((String) item.get("error_code"));
            }
            if (item.containsKey("code_fw_version")) {
                codeFwVersion = (String) item.get("code_fw_version");
            }
            if (item.containsKey("fw_version")) {
                fwVersion = (String) item.get("fw_version");
            }
            if(item.containsKey("sensor_otp_type")){
                sensorOtpType = Integer.parseInt((String) item.get("sensor_otp_type"));
            }

            // fwVersion layout
            View fwVersionLayout = convertView.findViewById(R.id.fw_version_layout);
            TextView fwVersionTitle = (TextView) fwVersionLayout.findViewById(R.id.title);
            TextView fwVersionView = (TextView) fwVersionLayout.findViewById(R.id.result);
            TextView fwVersionTipsView = (TextView) fwVersionLayout.findViewById(R.id.success_tips);

            if (mConfig != null && mConfig.mChipSeries == Constants.GF_MILAN_A_SERIES) {
                fwVersionLayout.setVisibility(View.VISIBLE);
                if (mTestResultChecker.checkFwVersionTestResult(errorCode, fwVersion, codeFwVersion, sensorOtpType)) {
                    fwVersionView.setTextColor(getResources()
                            .getColor(R.color.test_succeed_color));
                } else {
                    fwVersionView.setTextColor(getResources()
                            .getColor(R.color.test_failed_color));
                }

                fwVersionTipsView
                .setText(getResources().getString(R.string.test_success_tips_string,
                        " equals ", codeFwVersion.trim()));
                fwVersionView.setText(fwVersion);
                fwVersionTitle.setText(R.string.test_spi_fw);
            } else if (mConfig != null && mConfig.mChipSeries == Constants.GF_OSWEGO_M) {
                fwVersionLayout.setVisibility(View.VISIBLE);
                if (mTestResultChecker.checkFwVersionTestResult(errorCode, fwVersion, codeFwVersion)) {
                    fwVersionView.setTextColor(getResources()
                            .getColor(R.color.test_succeed_color));
                } else {
                    fwVersionView.setTextColor(getResources()
                            .getColor(R.color.test_failed_color));
                }

                fwVersionTipsView
                .setText(getResources().getString(R.string.test_success_tips_string,
                        " equals ", codeFwVersion));
                fwVersionView.setText(fwVersion);
                fwVersionTitle.setText(R.string.test_spi_fw);
            } else {
                fwVersionLayout.setVisibility(View.GONE);
            }

            // errcode layout
            View errorCodeLayout = convertView.findViewById(R.id.error_code_layout);
            TextView errorCodeTitle = (TextView) errorCodeLayout.findViewById(R.id.title);
            TextView errorCodeView = (TextView) errorCodeLayout.findViewById(R.id.result);
            TextView errorCodeTipsView = (TextView) errorCodeLayout.findViewById(R.id.success_tips);

            if (errorCode == 0) {
                errorCodeLayout.setVisibility(View.GONE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                errorCodeLayout.setVisibility(View.VISIBLE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            errorCodeTitle.setText(R.string.error_code);
            errorCodeView.setText(String.valueOf(errorCode));
            errorCodeTipsView
            .setText(getResources().getString(R.string.test_success_tips, " == ", 0));

            // result view
            TextView resultTitle = (TextView) convertView.findViewById(R.id.result_title);
            TextView resultView = (TextView) convertView.findViewById(R.id.test_result);

            String result = null;
            if (item.containsKey("result")) {
                result = (String) item.get("result");
            }

            resultTitle.setText(R.string.test_fw_version);
            if (mTestResultChecker.checkFwVersionTestResult(errorCode, fwVersion, codeFwVersion)) {
                resultView.setText(R.string.test_succeed);
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                if (result != null) {
                    if (result.equals("TIMEOUT")) {
                        resultView.setText(R.string.timeout);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else if (result.equals("CANCELED")) {
                        resultView.setText(R.string.canceled);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else {
                        resultView.setText(R.string.test_failed);
                    }
                } else {
                    resultView.setText(R.string.test_failed);
                }
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            // cost time view
            TextView costTimeView = (TextView) convertView
                    .findViewById(R.id.cost_time);
            if (costTimeView != null) {
                if (item.containsKey(KEY_TIME)) {
                    costTimeView.setVisibility(View.VISIBLE);
                    costTime = Integer.parseInt((String) item.get(KEY_TIME));
                    costTimeView.setText(String.valueOf(costTime));
                } else {
                    costTimeView.setVisibility(View.GONE);
                }
            }

            return convertView;
        }

        private View getTestBioView(int position, View convertView, ViewGroup parent) {
            HashMap<String, Object> item = mData.get(position);
            for (String key : item.keySet()) {
                Log.d(TAG, key + " : " + (String) item.get(key));
            }

            if (convertView == null) {
                convertView = LayoutInflater.from(DetailActivity.this).inflate(
                        R.layout.item_bio_assay_test_detail, null);
            }

            TestResultChecker.Threshold threshold = mTestResultChecker.getThresHold();
            if (threshold == null) {
                Log.e(TAG, "failed to get threshold");
                return convertView;
            }

            short baseUntouched = 0;
            short avgUntouched = 0;
            short baseTouched = 0;
            short avgTouched = 0;
            int costTime = 0;

            // untouched layout
            View untouchedLayout = convertView.findViewById(R.id.bio_assay_step1_layout);
            TextView untouchedTitle = (TextView) untouchedLayout.findViewById(R.id.title);
            TextView untouchedDelta = (TextView) untouchedLayout.findViewById(R.id.result);
            TextView untouchedCondition = (TextView) untouchedLayout
                    .findViewById(R.id.success_tips);

            if (item.containsKey("base0") && item.containsKey("r0") && threshold.osdUntoucded != 0) {
                baseUntouched = Short.parseShort((String) item.get("base0"));
                avgUntouched = Short.parseShort((String) item.get("r0"));
                untouchedLayout.setVisibility(View.VISIBLE);

                int delta = Math.abs(baseUntouched - avgUntouched);
                if (delta <= threshold.osdUntoucded) {
                    untouchedDelta
                    .setTextColor(getResources().getColor(R.color.test_succeed_color));
                } else {
                    untouchedDelta.setTextColor(getResources().getColor(R.color.test_failed_color));
                }
                untouchedDelta.setText(String.valueOf(delta));

                untouchedTitle.setText(R.string.test_bio_assay_no_finger_touch);
                untouchedCondition.setText(
                        getString(R.string.test_success_tips, " <= ", threshold.osdUntoucded));
            } else {
                untouchedLayout.setVisibility(View.GONE);
            }

            // touched layout
            View touchedLayout = convertView.findViewById(R.id.bio_assay_step2_layout);

            TextView touchedTitle = (TextView) touchedLayout.findViewById(R.id.title);
            TextView touchedDelta = (TextView) touchedLayout.findViewById(R.id.result);
            TextView touchedCondition = (TextView) touchedLayout.findViewById(R.id.success_tips);

            if (item.containsKey("base1") && item.containsKey("r1")) {
                baseTouched = Short.parseShort((String) item.get("base1"));
                avgTouched = Short.parseShort((String) item.get("r1"));
                touchedLayout.setVisibility(View.VISIBLE);

                int delta = Math.abs(baseTouched - avgTouched);
                if (delta >= threshold.osdTouched) {
                    touchedDelta.setTextColor(getResources().getColor(R.color.test_succeed_color));
                } else {
                    touchedDelta.setTextColor(getResources().getColor(R.color.test_failed_color));
                }
                touchedDelta.setText(String.valueOf(delta));

                touchedTitle.setText(R.string.test_bio_assay_finger_touch);
                touchedCondition.setText(
                        getString(R.string.test_success_tips, " >= ", threshold.osdTouched));
            } else {
                touchedLayout.setVisibility(View.GONE);
            }

            // errcode layout
            View errorCodeLayout = convertView.findViewById(R.id.error_code_layout);
            TextView errorCodeTitle = (TextView) errorCodeLayout.findViewById(R.id.title);
            TextView errorCodeView = (TextView) errorCodeLayout.findViewById(R.id.result);
            TextView errorCodeTipsView = (TextView) errorCodeLayout.findViewById(R.id.success_tips);

            int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
            if (item.containsKey("error_code")) {
                errorCode = Integer.parseInt((String) item.get("error_code"));
            }

            if (errorCode == 0) {
                errorCodeLayout.setVisibility(View.GONE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                errorCodeLayout.setVisibility(View.VISIBLE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            errorCodeTitle.setText(R.string.error_code);
            errorCodeView.setText(String.valueOf(errorCode));
            errorCodeTipsView
            .setText(getResources().getString(R.string.test_success_tips, " == ", 0));

            // result view
            TextView resultView = (TextView) convertView.findViewById(R.id.test_result);

            String result = null;
            if (item.containsKey("result")) {
                result = (String) item.get("result");
            }

            if (mTestResultChecker.checkBioTestResultWithoutTouched(errorCode, baseUntouched,
                    avgUntouched)
                    && mTestResultChecker.checkBioTestResultWithTouched(errorCode, baseTouched,
                            avgTouched)) {
                resultView.setText(R.string.test_succeed);
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                if (result != null) {
                    if (result.equals("TIMEOUT")) {
                        resultView.setText(R.string.timeout);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else if (result.equals("CANCELED")) {
                        resultView.setText(R.string.canceled);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else {
                        resultView.setText(R.string.test_failed);
                    }
                } else {
                    resultView.setText(R.string.test_failed);
                }
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            // cost time view
            TextView costTimeView = (TextView) convertView
                    .findViewById(R.id.cost_time);
            if (costTimeView != null) {
                if (item.containsKey(KEY_TIME)) {
                    costTimeView.setVisibility(View.VISIBLE);
                    costTime = Integer.parseInt((String) item.get(KEY_TIME));
                    costTimeView.setText(String.valueOf(costTime));
                } else {
                    costTimeView.setVisibility(View.GONE);
                }
            }

            return convertView;

        }

        private View getTestHbdView(int position, View convertView, ViewGroup parent) {
            HashMap<String, Object> item = mData.get(position);
            // for (String key : item.keySet()) {
            //     Log.d(TAG, key + " : " + (String) item.get(key));
            // }

            if (convertView == null) {
                convertView = LayoutInflater.from(DetailActivity.this).inflate(
                        R.layout.item_hbd_calibration_test_detail, null);
            }

            TestResultChecker.Threshold threshold = mTestResultChecker.getThresHold();
            if (threshold == null) {
                Log.e(TAG, "failed to get threshold");
                return convertView;
            }

            short baseTouched = 0;
            short avgTouched = 0;
            int electricity = 0;
            int avgAfterAdjust = 0;
            int costTime = 0;

            // touched layout
            View touchedLayout = convertView.findViewById(R.id.hbd_calibration_step1_layout);

            TextView touchedTitle = (TextView) touchedLayout.findViewById(R.id.title);
            TextView touchedDelta = (TextView) touchedLayout.findViewById(R.id.result);
            TextView touchedCondition = (TextView) touchedLayout.findViewById(R.id.success_tips);

            if (item.containsKey("base2") && item.containsKey("r2")) {
                baseTouched = Short.parseShort((String) item.get("base2"));
                avgTouched = Short.parseShort((String) item.get("r2"));
                touchedLayout.setVisibility(View.VISIBLE);
                touchedTitle.setText(R.string.test_bio_assay_finger_touch);

                int delta = Math.abs(baseTouched - avgTouched);
                if (delta >= threshold.osdTouched) {
                    touchedDelta.setTextColor(getResources().getColor(R.color.test_succeed_color));
                } else {
                    touchedDelta.setTextColor(getResources().getColor(R.color.test_failed_color));
                }
                touchedDelta.setText(String.valueOf(delta));
                touchedCondition.setText(
                        getString(R.string.test_success_tips, " >= ", threshold.osdTouched));

            } else {
                touchedLayout.setVisibility(View.GONE);
            }

            // hbd data avg layout
            View avgLayout = convertView.findViewById(R.id.hbd_calibration_raw_data_layout);
            TextView avgTitleView = (TextView) avgLayout.findViewById(R.id.title);
            TextView avgView = (TextView) avgLayout.findViewById(R.id.result);
            TextView avgTipsView = (TextView) avgLayout.findViewById(R.id.success_tips);

            if (item.containsKey("r3") && threshold.hbdAvgMin != 0
                    && threshold.hbdAvgMax != 0) {
                avgAfterAdjust = Short.parseShort((String) item.get("r3"));
                avgLayout.setVisibility(View.VISIBLE);

                if (avgAfterAdjust <= threshold.hbdAvgMax
                        && avgAfterAdjust >= threshold.hbdAvgMin) {
                    avgView.setTextColor(getResources().getColor(R.color.test_succeed_color));
                } else {
                    avgView.setTextColor(getResources().getColor(R.color.test_failed_color));
                }
                avgView.setText(String.valueOf(avgAfterAdjust));

                avgTitleView.setText(R.string.test_hbd_calibraion_avg);
                avgTipsView.setText(
                        getString(R.string.test_success_tips_multi_case_and,
                                threshold.hbdAvgMin + " <= ", "R",
                                " <= " + threshold.hbdAvgMax));
            } else {
                avgLayout.setVisibility(View.GONE);
            }

            // electricity layout
            View electricitydLayout = convertView
                    .findViewById(R.id.hbd_calibration_electricity_layout);

            TextView electricityTitleView = (TextView) electricitydLayout
                    .findViewById(R.id.title);
            TextView electricityView = (TextView) electricitydLayout.findViewById(R.id.result);
            TextView electricityTipsView = (TextView) electricitydLayout
                    .findViewById(R.id.success_tips);

            if (item.containsKey("electricity") && threshold.hbdElectricity != 0) {
                electricity = Integer.parseInt((String) item.get("electricity"));
                electricitydLayout.setVisibility(View.VISIBLE);

                if (electricity <= threshold.hbdElectricity) {
                    electricityView
                    .setTextColor(getResources().getColor(R.color.test_succeed_color));
                } else {
                    electricityView
                    .setTextColor(getResources().getColor(R.color.test_failed_color));
                }
                electricityView.setText(String.valueOf(electricity));

                electricityTitleView.setText(R.string.test_hbd_calibraion_electricity);
                electricityTipsView.setText(
                        getString(R.string.test_success_tips, " <= ",
                                threshold.hbdElectricity));
            } else {
                electricitydLayout.setVisibility(View.GONE);
            }

            // errcode layout
            View errorCodeLayout = convertView.findViewById(R.id.error_code_layout);
            TextView errorCodeTitle = (TextView) errorCodeLayout.findViewById(R.id.title);
            TextView errorCodeView = (TextView) errorCodeLayout.findViewById(R.id.result);
            TextView errorCodeTipsView = (TextView) errorCodeLayout.findViewById(R.id.success_tips);

            int errorCode = 0;
            if (item.containsKey("error_code")) {
                errorCode = Integer.parseInt((String) item.get("error_code"));
            }

            if (errorCode == 0) {
                errorCodeLayout.setVisibility(View.GONE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                errorCodeLayout.setVisibility(View.VISIBLE);
                errorCodeView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            errorCodeTitle.setText(R.string.error_code);
            errorCodeView.setText(String.valueOf(errorCode));
            errorCodeTipsView.setText(

                    getResources().getString(R.string.test_success_tips, " == ", 0));

            // result view
            TextView resultView = (TextView) convertView.findViewById(R.id.test_result);

            String result = null;
            if (item.containsKey("result")) {
                result = (String) item.get("result");
            }

            if (mTestResultChecker.checkBioTestResultWithTouched(errorCode, baseTouched, avgTouched)
                    && mTestResultChecker.checkHBDTestResultWithTouched(errorCode, avgAfterAdjust,
                            electricity)) {
                resultView.setText(R.string.test_succeed);
                resultView.setTextColor(getResources().getColor(R.color.test_succeed_color));
            } else {
                if (result != null) {
                    if (result.equals("TIMEOUT")) {
                        resultView.setText(R.string.timeout);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else if (result.equals("CANCELED")) {
                        resultView.setText(R.string.canceled);
                        errorCodeLayout.setVisibility(View.GONE);
                    } else {
                        resultView.setText(R.string.test_failed);
                    }
                } else {
                    resultView.setText(R.string.test_failed);
                }
                resultView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            // cost time view
            TextView costTimeView = (TextView) convertView
                    .findViewById(R.id.cost_time);
            if (costTimeView != null) {
                if (item.containsKey(KEY_TIME)) {
                    costTimeView.setVisibility(View.VISIBLE);
                    costTime = Integer.parseInt((String) item.get(KEY_TIME));
                    costTimeView.setText(String.valueOf(costTime));
                } else {
                    costTimeView.setVisibility(View.GONE);
                }
            }

            return convertView;

        }
    }
}
