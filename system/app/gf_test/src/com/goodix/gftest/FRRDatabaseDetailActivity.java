
package com.goodix.gftest;

import java.util.ArrayList;

import com.goodix.gftest.utils.Metadata;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class FRRDatabaseDetailActivity extends Activity {
    private static final String TAG = "FRRDatabaseDetailActivity";
    private ArrayList<Metadata> mMetadataList = null;
    private ListView mListView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frr_database_detail);
        initView();
        Intent intent = getIntent();
        mMetadataList = intent.getParcelableArrayListExtra("metadatalist");
        if ((mMetadataList == null) || (mMetadataList.size() == 0)) {
            Log.e(TAG, "Invalid Metadata input. To do nothing ");
        } else {
            mListView.setAdapter(new MyAdapter(FRRDatabaseDetailActivity.this, mMetadataList));
        }
    }

    private void initView() {
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        mListView = (ListView) findViewById(R.id.frr_database_listview);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater layoutInflater;
        private ArrayList<Metadata> list;

        public MyAdapter(Context context, ArrayList<Metadata> list) {
            this.layoutInflater = LayoutInflater.from(context);
            this.list = list;
        }

        public class OneLineInfo {
            public TextView mBioView;
            public TextView mImageQualityView;
            public TextView mValidAreaView;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            OneLineInfo oneLine = null;
            if (convertView == null) {
                oneLine = new OneLineInfo();
                convertView = layoutInflater.inflate(R.layout.item_frr_database_detail, null);
                oneLine.mBioView = (TextView) convertView.findViewById(R.id.item_bio_flag);
                oneLine.mImageQualityView = (TextView) convertView
                        .findViewById(R.id.item_image_quality);
                oneLine.mValidAreaView = (TextView) convertView.findViewById(R.id.item_valid_area);
                convertView.setTag(oneLine);
            } else {
                oneLine = (OneLineInfo) convertView.getTag();
            }
            int bio = list.get(position).getBioFlag();
            String BioStr;
            String imgQ = String.valueOf(list.get(position).getImageQuality());
            String imgA = String.valueOf(list.get(position).getValidArea());

            if (bio == Metadata.CHIP_UNSUPPORT_BIO) {
                BioStr = FRRDatabaseDetailActivity.this.getString(R.string.frr_chip_not_support);
            } else if (bio == Metadata.CHIP_SUPPORT_BIO_DISABLE) {
                BioStr = FRRDatabaseDetailActivity.this.getString(R.string.frr_disable);
            } else if (bio == Metadata.CHIP_SUPPORT_BIO_ENABLE_BIO_FAILED) {
                BioStr = FRRDatabaseDetailActivity.this.getString(R.string.frr_not_living);
            } else if (bio == Metadata.CHIP_SUPPORT_BIO_ENABLE_MATCH_FAILED) {
                BioStr = FRRDatabaseDetailActivity.this.getString(R.string.frr_not_matched);
            } else {
                BioStr = FRRDatabaseDetailActivity.this.getString(R.string.none);
            }
            oneLine.mBioView.setText(BioStr);
            oneLine.mImageQualityView.setText(imgQ);
            oneLine.mValidAreaView.setText(imgA);
            return convertView;
        }
    }
}
