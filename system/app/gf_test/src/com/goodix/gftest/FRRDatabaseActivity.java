
package com.goodix.gftest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.GFConfig;
import com.goodix.fingerprint.service.GoodixFingerprintManager;
import com.goodix.fingerprint.service.GoodixFingerprintManager.TestCmdCallback;
import com.goodix.fingerprint.utils.TestParamEncoder;
import com.goodix.fingerprint.utils.TestResultParser;
import com.goodix.gftest.utils.Metadata;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

public class FRRDatabaseActivity extends Activity {
    private static final String TAG = "FRRDatabaseActivity";
    private GoodixFingerprintManager mGoodixFingerprintManager = null;

    private Switch mFRRAnalysisView = null;
    private View mFRRAnalysisDividerView = null;
    private View mFRRAnalysisLayout = null;
    private TextView mSoftwareVersionView = null;
    private TextView mProtocolVersionView = null;
    private TextView mScreenOnAuthenticateFailRetryCountView = null;
    private TextView mScreenOffAuthenticateFailRetryCountView = null;
    private TextView mAuthenticatedSuccessCountView = null;
    private TextView mAuthenticatedFailedCountView = null;
    private TextView mFRRRateView = null;
    private Spinner mImageQualitySpinner = null;
    private Spinner mValidAreaSpinner = null;
    private Button mDetailBtn = null;

    private ArrayAdapter<Integer> mAdapter;
    private ArrayList<Metadata> mMetadataList = null;
    private int mPackageVersion = 0;
    private int mProtocolVersion = 0;
    private int mChipType = 0;
    private int mScreenOnAuthenticateFailRetryCount = 0;
    private int mScreenOffAuthenticateFailRetryCount = 0;
    private int mChipSupportBio = 0;
    private int mIsBioEnable = 0;
    private int mAuthenticatedWithBioSuccessCount = 0;
    private int mAuthenticatedWithBioFailedCount = 0;
    private int mAuthenticatedSuccessCount = 0;
    private int mAuthenticatedFailedCount = 0;
    private String mMetadataStr = null;
    protected String mName;
    private int mImageQualityThreshold = 0;
    private int mValidAreaThreshold = 0;
    private GFConfig mConfig = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frr_database);

        mGoodixFingerprintManager = new GoodixFingerprintManager(this);
        mConfig = mGoodixFingerprintManager.getConfig();

        initView();
    }

    private void accessFrrDatabase() {
        mMetadataList = new ArrayList<Metadata>();
        mGoodixFingerprintManager.registerTestCmdCallback(mTestCmdCallback);

        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_FRR_DATABASE_ACCESS, null);
    }

    private void initView() {
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        mFRRAnalysisView = (Switch) findViewById(R.id.frr_analysis);
        mFRRAnalysisLayout = findViewById(R.id.frr_analysis_layout);
        mFRRAnalysisDividerView = findViewById(R.id.frr_analysis_divider);

        mSoftwareVersionView = (TextView) findViewById(R.id.software_version_id);
        mProtocolVersionView = (TextView) findViewById(R.id.protocol_version_id);
        mScreenOnAuthenticateFailRetryCountView = (TextView) findViewById(
                R.id.screen_on_fail_retry_id);
        mScreenOffAuthenticateFailRetryCountView = (TextView) findViewById(
                R.id.screen_off_fail_retry_id);
        mAuthenticatedSuccessCountView = (TextView) findViewById(R.id.success_id);
        mAuthenticatedFailedCountView = (TextView) findViewById(R.id.failed_id);
        mFRRRateView = (TextView) findViewById(R.id.frr_rate_id);
        mImageQualitySpinner = (Spinner) findViewById(R.id.image_quality_spinner);
        mValidAreaSpinner = (Spinner) findViewById(R.id.valid_area_spinner);
        mDetailBtn = (Button) findViewById(R.id.frr_more_detail_id);

        List<Integer> data_list = new ArrayList<Integer>();
        for (int i = 0; i <= 100; i++) {
            data_list.add(i);
        }
        mAdapter = new ArrayAdapter<Integer>(FRRDatabaseActivity.this,
                android.R.layout.simple_spinner_item, data_list);
        // set style
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mImageQualitySpinner.setAdapter(mAdapter);
        mValidAreaSpinner.setAdapter(mAdapter);

        mImageQualitySpinner.setOnItemSelectedListener(mSpinnerLister);
        mValidAreaSpinner.setOnItemSelectedListener(mSpinnerLister);

        mDetailBtn.setOnClickListener(listener);

        mFRRAnalysisView.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mFRRAnalysisLayout.setVisibility(View.VISIBLE);
                    mDetailBtn.setVisibility(View.VISIBLE);
                    mFRRAnalysisDividerView.setVisibility(View.VISIBLE);
                } else {
                    mFRRAnalysisLayout.setVisibility(View.GONE);
                    mDetailBtn.setVisibility(View.GONE);
                    mFRRAnalysisDividerView.setVisibility(View.GONE);
                }

                byte[] byteArray = new byte[TestParamEncoder.TEST_ENCODE_SIZEOF_INT32];
                int offset = 0;

                offset = TestParamEncoder.encodeInt32(byteArray, offset,
                        TestResultParser.TEST_TOKEN_SUPPORT_FRR_ANALYSIS, isChecked ? 1 : 0);
                mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_SET_CONFIG, byteArray);
            }
        });

        if (mConfig.mSupportFrrAnalysis > 0) {
            mFRRAnalysisView.setChecked(true);
            mFRRAnalysisLayout.setVisibility(View.VISIBLE);
            mDetailBtn.setVisibility(View.VISIBLE);
            mFRRAnalysisDividerView.setVisibility(View.VISIBLE);
        } else {
            mFRRAnalysisView.setChecked(false);
            mFRRAnalysisLayout.setVisibility(View.GONE);
            mDetailBtn.setVisibility(View.GONE);
            mFRRAnalysisDividerView.setVisibility(View.GONE);
        }
    }

    public void handleThreshold() {
        int invalidMetadataCount = 0;
        if ((mMetadataList == null) || (mMetadataList.size() == 0)) {
            return;
        }

        for (int i = 0; i < mMetadataList.size(); i++) {
            if ((mMetadataList.get(i).getValidArea() < mValidAreaThreshold)
                    || mMetadataList.get(i).getImageQuality() < mImageQualityThreshold) {
                invalidMetadataCount++;
            }
        }
        Log.i(TAG, "After fliter, InvalidMetadataCount =" + invalidMetadataCount);

        float frrRate = ((float) (mAuthenticatedFailedCount + mAuthenticatedWithBioFailedCount
                - invalidMetadataCount)
                / (float) (mAuthenticatedFailedCount + mAuthenticatedWithBioFailedCount
                        + mAuthenticatedSuccessCount
                        + mAuthenticatedWithBioSuccessCount
                        - invalidMetadataCount))
                * 100;
        StringBuffer sb = new StringBuffer(String.valueOf(frrRate));
        sb.append("%");
        mFRRRateView.setText(sb.toString());
        mAuthenticatedFailedCountView.setText(String
                .valueOf(mAuthenticatedFailedCount + mAuthenticatedWithBioFailedCount
                        - invalidMetadataCount));
    }

    Spinner.OnItemSelectedListener mSpinnerLister = new Spinner.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if ((mAuthenticatedFailedCount != 0) || (mAuthenticatedSuccessCount != 0)
                    || (mAuthenticatedWithBioSuccessCount != 0)
                    || (mAuthenticatedWithBioFailedCount != 0)) {
                mImageQualityThreshold = (Integer) mImageQualitySpinner.getSelectedItem();
                mValidAreaThreshold = (Integer) mValidAreaSpinner.getSelectedItem();
                Log.e(TAG,
                        "imageQualityThreshold = " + mValidAreaThreshold
                        + ", validAreaThreshold = "
                        + mValidAreaThreshold);
                handleThreshold();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    Button.OnClickListener listener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setClass(FRRDatabaseActivity.this, FRRDatabaseDetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("metadatalist", mMetadataList);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    };

    private void fillMetaData(String[] split) {
        int len = split.length;
        int BioFlag;
        int imgQ;
        int imgA;

        Log.i(TAG, "Metadata Count =" + len);

        for (int i = 0; i < len; i++) {
            String bioflag = split[i].substring(0, 1);
            // get bio
            if ("-".equals(bioflag)) {
                BioFlag = Metadata.CHIP_UNSUPPORT_BIO;
            } else if ("x".equals(bioflag)) {
                BioFlag = Metadata.CHIP_SUPPORT_BIO_DISABLE;
            } else if ("0".equals(bioflag)) {
                BioFlag = Metadata.CHIP_SUPPORT_BIO_ENABLE_BIO_FAILED;
            } else {
                BioFlag = Metadata.CHIP_SUPPORT_BIO_ENABLE_MATCH_FAILED;
            }

            int Index = split[i].indexOf("Q");
            String Str = split[i].substring(Index + 1, Index + 4);
            imgQ = Integer.parseInt(Str);

            Index = split[i].indexOf("A");
            Str = split[i].substring(Index + 1, Index + 4);
            imgA = Integer.parseInt(Str);
            Metadata metadata = new Metadata(BioFlag, imgQ, imgA);
            mMetadataList.add(metadata);
        }
    }

    private void updateView() {
        handleThreshold();

        mSoftwareVersionView.setText(String.valueOf(mPackageVersion));
        mProtocolVersionView.setText(String.valueOf(mProtocolVersion));
        mScreenOnAuthenticateFailRetryCountView
        .setText(String.valueOf(mScreenOnAuthenticateFailRetryCount));
        mScreenOffAuthenticateFailRetryCountView
        .setText(String.valueOf(mScreenOffAuthenticateFailRetryCount));
        mAuthenticatedSuccessCountView
        .setText(String
                .valueOf(mAuthenticatedSuccessCount + mAuthenticatedWithBioSuccessCount));
    }

    private TestCmdCallback mTestCmdCallback = new TestCmdCallback() {

        @Override
        public void onTestCmd(int cmdId, HashMap<Integer, Object> result) {
            if ((cmdId == Constants.CMD_TEST_FRR_DATABASE_ACCESS)
                    && (result.containsKey(TestResultParser.TEST_TOKEN_PACKAGE_VERSION))) {
                mPackageVersion = (Integer) result.get(TestResultParser.TEST_TOKEN_PACKAGE_VERSION);
                mProtocolVersion = (Integer) result
                        .get(TestResultParser.TEST_TOKEN_PROTOCOL_VERSION);
                mChipType = (Integer) result.get(TestResultParser.TEST_TOKEN_CHIP_TYPE);
                mScreenOnAuthenticateFailRetryCount = (Integer) result
                        .get(TestResultParser.TEST_TOKEN_SCREEN_ON_AUTHENTICATE_FAIL_RETRY_COUNT);
                mScreenOffAuthenticateFailRetryCount = (Integer) result
                        .get(TestResultParser.TEST_TOKEN_SCREEN_OFF_AUTHENTICATE_FAIL_RETRY_COUNT);
                mChipSupportBio = (Integer) result
                        .get(TestResultParser.TEST_TOKEN_CHIP_SUPPORT_BIO);
                mIsBioEnable = (Integer) result.get(TestResultParser.TEST_TOKEN_IS_BIO_ENABLE);
                mAuthenticatedWithBioSuccessCount = (Integer) result
                        .get(TestResultParser.TEST_TOKEN_AUTHENTICATED_WITH_BIO_SUCCESS_COUNT);
                mAuthenticatedWithBioFailedCount = (Integer) result
                        .get(TestResultParser.TEST_TOKEN_AUTHENTICATED_WITH_BIO_FAILED_COUNT);
                mAuthenticatedSuccessCount = (Integer) result
                        .get(TestResultParser.TEST_TOKEN_AUTHENTICATED_SUCCESS_COUNT);
                mAuthenticatedFailedCount = (Integer) result
                        .get(TestResultParser.TEST_TOKEN_AUTHENTICATED_FAILED_COUNT);
                mMetadataStr = (String) result.get(TestResultParser.TEST_TOKEN_METADATA);

                if ((mMetadataStr != null) && (mMetadataStr.length() != 0)) {
                    String MetadataSplit[] = mMetadataStr.split("\n");
                    fillMetaData(MetadataSplit);
                }
                updateView();
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        accessFrrDatabase();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGoodixFingerprintManager.unregisterTestCmdCallback(mTestCmdCallback);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
