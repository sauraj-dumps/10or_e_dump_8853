
package com.goodix.gftest.service;

import java.util.HashMap;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.service.GoodixFingerprintManager;
import com.goodix.fingerprint.service.GoodixFingerprintManager.DumpCallback;
import com.goodix.fingerprint.utils.TestParamEncoder;
import com.goodix.fingerprint.utils.TestResultParser;
import com.goodix.gftest.DumpActivity;
import com.goodix.gftest.R;
import com.goodix.gftest.utils.DumpUtils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.CancellationSignal;
import android.os.IBinder;

public class DumpService extends Service {

    private static final String TAG = "DumpService";

    public static final String ACTION_DUMP_DATA = "dump_data";
    public static final String ACTION_DUMP_TEMPLATES = "dump_templates";
    public static final String ACTION_DUMP_PATH = "dump_path";
    public static final String ACTION_DUMP_BASE_FRAME = "dump_base_frame";

    private final static int DUMP_SERVICE_NOTIFY_ID = 100;
    private NotificationManager mNotificationManager;

    private GoodixFingerprintManager mGoodixFingerprintManager = null;
    private CancellationSignal mDumpCancel = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mGoodixFingerprintManager = new GoodixFingerprintManager(DumpService.this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();
            if (ACTION_DUMP_DATA.equals(action)) {
                mDumpCancel = new CancellationSignal();
                mGoodixFingerprintManager.dump(mDumpCancel, mDumpCallback);
                showNotification();

                int path = intent.getIntExtra(DumpActivity.KEY_DUMP_PATH,
                        Constants.DUMP_PATH_SDCARD);
                dumpPathChanged(path);
            } else if (ACTION_DUMP_TEMPLATES.equals(action)) {
                mGoodixFingerprintManager.dumpCmd(Constants.CMD_DUMP_TEMPLATES, null);
            } else if (ACTION_DUMP_PATH.equals(action)) {
                int path = intent.getIntExtra(DumpActivity.KEY_DUMP_PATH,
                        Constants.DUMP_PATH_SDCARD);
                dumpPathChanged(path);
            } else if (ACTION_DUMP_BASE_FRAME.equals(action)) {
                mGoodixFingerprintManager.dumpCmd(Constants.CMD_DUMP_NAV_BASE, null);
                mGoodixFingerprintManager.dumpCmd(Constants.CMD_DUMP_FINGER_BASE, null);
            }
        }

        return START_NOT_STICKY;
    }

    private void dumpPathChanged(int path) {
        byte[] byteArray = new byte[TestParamEncoder.TEST_ENCODE_SIZEOF_INT32];
        int offset = 0;

        offset = TestParamEncoder.encodeInt32(byteArray, offset,
                TestResultParser.TEST_PARAM_TOKEN_DUMP_PATH, path);
        mGoodixFingerprintManager.dumpCmd(Constants.CMD_DUMP_PATH, byteArray);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mDumpCancel.cancel();
        mNotificationManager.cancel(DUMP_SERVICE_NOTIFY_ID);
    }

    private void showNotification() {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentTitle(getResources().getString(R.string.app_name_goodix))
        .setContentText(getResources().getString(R.string.dump_data))
        .setContentIntent(getDefalutIntent(Notification.FLAG_AUTO_CANCEL))
        .setPriority(Notification.PRIORITY_DEFAULT)
        .setOngoing(true)
        .setSmallIcon(R.drawable.ic_launcher);
        mNotificationManager.notify(DUMP_SERVICE_NOTIFY_ID, builder.build());
    }

    private PendingIntent getDefalutIntent(int flags) {
        Intent intent = new Intent(this, DumpActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, flags);
        return pendingIntent;
    }

    private DumpCallback mDumpCallback = new DumpCallback() {

        @Override
        public void onDump(int cmdId, HashMap<Integer, Object> data) {
            if (null == data) {
                return;
            }

            int isEncrypted = 0;
            if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_IS_ENCRYPTED)) {
                isEncrypted = (Integer) data.get(TestResultParser.TEST_TOKEN_DUMP_IS_ENCRYPTED);
            }

            if (isEncrypted > 0) {
                byte encryptedData[] = null;
                long timestamp = 0;

                if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_TIMESTAMP)) {
                    timestamp = (Long) data.get(TestResultParser.TEST_TOKEN_DUMP_TIMESTAMP);
                }
                if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_ENCRYPTED_DATA)) {
                    encryptedData = (byte[]) data
                            .get(TestResultParser.TEST_TOKEN_DUMP_ENCRYPTED_DATA);

                    switch (cmdId) {
                        case Constants.CMD_DUMP_DATA:
                            DumpUtils.dumpEncryptedData(encryptedData, timestamp);
                            break;
                        case Constants.CMD_DUMP_TEMPLATES:
                            DumpUtils.dumpEncryptedTemplate(encryptedData, timestamp);
                            break;
                        case Constants.CMD_DUMP_NAV_BASE:
                            DumpUtils.dumpEncryptedData(encryptedData, timestamp);
                            break;
                        case Constants.CMD_DUMP_FINGER_BASE:
                            DumpUtils.dumpEncryptedData(encryptedData, timestamp);
                            break;
                    }
                }
                return;
            } else {
                switch (cmdId) {
                    case Constants.CMD_DUMP_DATA:
                        DumpUtils.dumpData(data);
                        break;

                    case Constants.CMD_DUMP_TEMPLATES: {
                        int fingerId = 0;
                        byte templateData[] = null;
                        long timestamp = 0;

                        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_TIMESTAMP)) {
                            timestamp = (Long) data.get(TestResultParser.TEST_TOKEN_DUMP_TIMESTAMP);
                        }

                        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_FINGER_ID)) {
                            fingerId = (Integer) data
                                    .get(TestResultParser.TEST_TOKEN_DUMP_FINGER_ID);
                        }
                        if (data.containsKey(TestResultParser.TEST_TOKEN_DUMP_TEMPLATE)) {
                            templateData = (byte[]) data
                                    .get(TestResultParser.TEST_TOKEN_DUMP_TEMPLATE);
                        }
                        DumpUtils.dumpTemplate(fingerId, templateData, timestamp);
                        break;
                    }

                    case Constants.CMD_DUMP_NAV_BASE:
                        DumpUtils.dumpData(data);
                        break;

                    case Constants.CMD_DUMP_FINGER_BASE:
                        DumpUtils.dumpData(data);
                        break;
                }
            }

        }

    };

}
