
package com.goodix.gftest.utils;

import java.util.HashMap;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.utils.TestResultParser;

import android.util.Log;

public class TestResultChecker {

    private static final String TAG = "TestResultChecker";

    public static final int TEST_NONE = 0;
    public static final int TEST_SPI = 1;
    public static final int TEST_PIXEL = 2;
    public static final int TEST_RESET_PIN = 3;
    public static final int TEST_BAD_POINT = 4;
    public static final int TEST_PERFORMANCE = 5;
    public static final int TEST_CAPTURE = 6;
    public static final int TEST_ALGO = 7;
    public static final int TEST_FW_VERSION = 8;
    public static final int TEST_SENSOR_CHECK = 9;
    public static final int TEST_BIO_CALIBRATION = 10;
    public static final int TEST_HBD_CALIBRATION = 11;
    public static final int TEST_CODE_FW_VERSION = 12;
    public static final int TEST_MAX = 13;

    public static class Threshold {
        public String spiFwVersion;
        public int chipId;
        public int badBointNum = 0;
        public long totalTime = 0;
        public int imageQuality = 0;
        public int validArea = 0;
        public short avgDiffVal;
        public int badPixelNum = 0;
        public int localBadPixelNum = 0;
        public float allTiltAngle = 0;
        public float blockTiltAngleMax = 0;
        public short localWorst = 0;
        public int singular = 0;
        public int inCircle = 0;
        public int osdUntoucded = 0;
        public int osdTouched = 0;
        public int hbdAvgMax = 0;
        public int hbdAvgMin = 0;
        public int hbdElectricity = 0;
        public int localSmallBadPixel = 0;
        public int localBigBadPixel = 0;

        public Threshold() {
            super();
            imageQuality = Constants.TEST_CAPTURE_VALID_IMAGE_QUALITY_THRESHOLD;
            validArea = Constants.TEST_CAPTURE_VALID_IMAGE_AREA_THRESHOLD;
            totalTime = Constants.TEST_PERFORMANCE_TOTAL_TIME;
        }

        public Threshold(int chipType) {
            this();
            switch (chipType) {
                case Constants.GF_CHIP_318M:
                case Constants.GF_CHIP_3118M:
                case Constants.GF_CHIP_518M:
                case Constants.GF_CHIP_5118M:
                    spiFwVersion = Constants.Oswego.TEST_SPI_GFX18;
                    badBointNum = Constants.Oswego.TEST_SENSOR_BAD_POINT_COUNT;

                    badPixelNum = Constants.Oswego.TEST_BAD_POINT_BAD_PIXEL_NUM;
                    // localBadPixelNum = Constants.Oswego.TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM;
                    avgDiffVal = Constants.Oswego.TEST_BAD_POINT_AVG_DIFF_VAL;
                    // allTiltAngle = Constants.Oswego.TEST_BAD_POINT_ALL_TILT_ANGLE;
                    // blockTiltAngleMax = Constants.Oswego.TEST_BAD_POINT_BLOCK_TILT_ANGLE_MAX;
                    localSmallBadPixel = Constants.Oswego.TEST_BAD_POINT_LOCAL_SMALL_BAD_POINT;
                    localBigBadPixel = Constants.Oswego.TEST_BAD_POINT_LOCAL_BIG_BAD_POINT;
                    break;

                case Constants.GF_CHIP_316M:
                case Constants.GF_CHIP_516M:
                case Constants.GF_CHIP_816M:
                    spiFwVersion = Constants.Oswego.TEST_SPI_GFX16;
                    badBointNum = Constants.Oswego.TEST_SENSOR_BAD_POINT_COUNT;

                    badPixelNum = Constants.Oswego.TEST_BAD_POINT_BAD_PIXEL_NUM;
                    // localBadPixelNum = Constants.Oswego.TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM;
                    avgDiffVal = Constants.Oswego.TEST_BAD_POINT_AVG_DIFF_VAL;
                    // allTiltAngle = Constants.Oswego.TEST_BAD_POINT_ALL_TILT_ANGLE;
                    // blockTiltAngleMax = Constants.Oswego.TEST_BAD_POINT_BLOCK_TILT_ANGLE_MAX;
                    localSmallBadPixel = Constants.Oswego.TEST_BAD_POINT_LOCAL_SMALL_BAD_POINT;
                    localBigBadPixel = Constants.Oswego.TEST_BAD_POINT_LOCAL_BIG_BAD_POINT;
                    break;

                case Constants.GF_CHIP_3208:
                    chipId = Constants.MilanF.TEST_SPI_CHIP_ID;
                    badBointNum = Constants.MilanF.TEST_SENSOR_BAD_POINT_COUNT;

                    badPixelNum = Constants.MilanF.TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM;
                    localBadPixelNum = Constants.MilanF.TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM;
                    localWorst = Constants.MilanF.TEST_BAD_POINT_LOCAL_WORST;

                    break;
                case Constants.GF_CHIP_3268:
                    chipId = Constants.MilanFN.TEST_SPI_CHIP_ID;
                    badBointNum = Constants.MilanFN.TEST_SENSOR_BAD_POINT_COUNT;

                    badPixelNum = Constants.MilanFN.TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM;
                    localBadPixelNum = Constants.MilanFN.TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM;
                    localWorst = Constants.MilanFN.TEST_BAD_POINT_LOCAL_WORST;

                    break;
                case Constants.GF_CHIP_3206:
                    chipId = Constants.MilanG.TEST_SPI_CHIP_ID;
                    badBointNum = Constants.MilanG.TEST_SENSOR_BAD_POINT_COUNT;

                    badPixelNum = Constants.MilanG.TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM;
                    localBadPixelNum = Constants.MilanG.TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM;
                    localWorst = Constants.MilanG.TEST_BAD_POINT_LOCAL_WORST;
                    break;

                case Constants.GF_CHIP_3266:
                    chipId = Constants.MilanE.TEST_SPI_CHIP_ID;
                    badBointNum = Constants.MilanE.TEST_SENSOR_BAD_POINT_COUNT;

                    badPixelNum = Constants.MilanE.TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM;
                    localBadPixelNum = Constants.MilanE.TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM;
                    localWorst = Constants.MilanE.TEST_BAD_POINT_LOCAL_WORST;
                    break;

                case Constants.GF_CHIP_3288:
                    chipId = Constants.MilanL.TEST_SPI_CHIP_ID;
                    badBointNum = Constants.MilanL.TEST_SENSOR_BAD_POINT_COUNT;

                    badPixelNum = Constants.MilanL.TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM;
                    localBadPixelNum = Constants.MilanL.TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM;
                    localWorst = Constants.MilanL.TEST_BAD_POINT_LOCAL_WORST;
                    break;
                case Constants.GF_CHIP_3228:
                    chipId = Constants.MilanK.TEST_SPI_CHIP_ID;
                    badBointNum = Constants.MilanK.TEST_SENSOR_BAD_POINT_COUNT;

                    badPixelNum = Constants.MilanK.TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM;
                    localBadPixelNum = Constants.MilanK.TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM;
                    localWorst = Constants.MilanK.TEST_BAD_POINT_LOCAL_WORST;
                    break;
                case Constants.GF_CHIP_3226:
                    chipId = Constants.MilanJ.TEST_SPI_CHIP_ID;
                    badBointNum = Constants.MilanJ.TEST_SENSOR_BAD_POINT_COUNT;

                    badPixelNum = Constants.MilanJ.TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM;
                    localBadPixelNum = Constants.MilanJ.TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM;
                    localWorst = Constants.MilanJ.TEST_BAD_POINT_LOCAL_WORST;
                    break;
                case Constants.GF_CHIP_3258:
                    chipId = Constants.MilanH.TEST_SPI_CHIP_ID;
                    badBointNum = Constants.MilanH.TEST_SENSOR_BAD_POINT_COUNT;

                    badPixelNum = Constants.MilanH.TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM;
                    localBadPixelNum = Constants.MilanH.TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM;
                    localWorst = Constants.MilanH.TEST_BAD_POINT_LOCAL_WORST;
                    break;

                case Constants.GF_CHIP_5206:
                case Constants.GF_CHIP_5216:
                    spiFwVersion = Constants.MilanA.TEST_SPI_FW_VERSION;
                    badBointNum = Constants.MilanA.TEST_SENSOR_BAD_POINT_COUNT;
                    totalTime = Constants.MilanA.TEST_PERFORMANCE_TOTAL_TIME;

                    badPixelNum = Constants.MilanA.TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM;
                    localBadPixelNum = Constants.MilanA.TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM;
                    localWorst = Constants.MilanA.TEST_BAD_POINT_LOCAL_WORST;
                    inCircle = Constants.MilanA.TEST_BAD_POINT_INCIRCLE;

                    if (chipType == Constants.GF_CHIP_5206) {
                        osdUntoucded = Constants.MilanA.TEST_BIO_THRESHOLD_UNTOUCHED;
                        osdTouched = Constants.MilanA.TEST_BIO_THRESHOLD_TOUCHED;

                        hbdAvgMin = Constants.MilanA.TEST_HBD_THRESHOLD_AVG_MIN;
                        hbdAvgMax = Constants.MilanA.TEST_HBD_THRESHOLD_AVG_MAX;
                        hbdElectricity = Constants.MilanA.TEST_HBD_THRESHOLD_ELECTRICITY;
                    }
                    break;

                case Constants.GF_CHIP_5208:
                case Constants.GF_CHIP_5218:

                    spiFwVersion = Constants.MilanC.TEST_SPI_FW_VERSION;
                    badBointNum = Constants.MilanC.TEST_SENSOR_BAD_POINT_COUNT;
                    totalTime = Constants.MilanC.TEST_PERFORMANCE_TOTAL_TIME;

                    badPixelNum = Constants.MilanC.TEST_BAD_POINT_TOTAL_BAD_PIXEL_NUM;
                    localBadPixelNum = Constants.MilanC.TEST_BAD_POINT_LOCAL_BAD_PIXEL_NUM;
                    localWorst = Constants.MilanC.TEST_BAD_POINT_LOCAL_WORST;
                    inCircle = Constants.MilanC.TEST_BAD_POINT_INCIRCLE;

                    if (chipType == Constants.GF_CHIP_5208) {
                        osdUntoucded = Constants.MilanC.TEST_BIO_THRESHOLD_UNTOUCHED;
                        osdTouched = Constants.MilanC.TEST_BIO_THRESHOLD_TOUCHED;

                        hbdAvgMin = Constants.MilanC.TEST_HBD_THRESHOLD_AVG_MIN;
                        hbdAvgMax = Constants.MilanC.TEST_HBD_THRESHOLD_AVG_MAX;
                        hbdElectricity = Constants.MilanC.TEST_HBD_THRESHOLD_ELECTRICITY;
                    }

                    break;

                default:
                    break;
            }

        }
    }

    public static abstract class Checker {

        protected Threshold mThresHold;
        protected int mChipType;

        public Threshold getThresHold() {
            return mThresHold;
        }

        public int[] getTestItems(int index) {
            return null;
        }

        private boolean checkErrcode(final HashMap<Integer, Object> result) {
            int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
            if (result.containsKey(TestResultParser.TEST_TOKEN_ERROR_CODE)) {
                errorCode = (Integer) result
                        .get(TestResultParser.TEST_TOKEN_ERROR_CODE);
            }

            return (errorCode == 0);
        }

        public boolean checkSpiTestResult(final HashMap<Integer, Object> result) {
            return checkErrcode(result);
        }

        public boolean checkSpiTestResult(int errCode, String fwVersion, int ChipId, int sensorOtpType) {
            return (errCode == 0);
        }

        public boolean checkResetPinTestReuslt(final HashMap<Integer, Object> result) {
            if (checkErrcode(result)) {
                int resetFlag = 0;
                if (result.containsKey(TestResultParser.TEST_TOKEN_RESET_FLAG)) {
                    resetFlag = (Integer) result
                            .get(TestResultParser.TEST_TOKEN_RESET_FLAG);
                } else if (result.containsKey("reset_flag")) {
                    resetFlag = (Integer) result.get("reset_flag");
                }

                if (checkResetPinTestReuslt(0, resetFlag)) {
                    return true;
                }
            }

            return false;
        }

        public boolean checkResetPinTestReuslt(int errCode, int resetFlag) {
            return (errCode == 0) & (resetFlag > 0);
        }

        public boolean checkPixelTestResult(final HashMap<Integer, Object> result) {
            if (checkErrcode(result)) {
                int badPixelNum = 999;
                if (result.containsKey(TestResultParser.TEST_TOKEN_BAD_PIXEL_NUM)) {
                    badPixelNum = (Integer) result
                            .get(TestResultParser.TEST_TOKEN_BAD_PIXEL_NUM);
                }

                if (checkPixelTestResult(0, badPixelNum)) {
                    return true;
                }
            }
            return false;
        }

        public boolean checkPixelTestResult(int errCode, int badPixelNum) {
            return (errCode == 0) & (badPixelNum <= mThresHold.badBointNum);
        }

        public boolean checkFwVersionTestResult(final HashMap<Integer, Object> result) {
            return checkErrcode(result);
        }

        public boolean checkFwVersionTestResult(int errCode, String fwVersion, String codeFwVersion) {
            return (errCode == 0);
        }

        public boolean checkFwVersionTestResult(int errCode, String fwVersion, int sensorOtpType) {
            return (errCode == 0);
        }
        public boolean checkFwVersionTestResult(int errCode, String fwVersion, String codeFwVersion, int sensorOtpType) {
            return (errCode == 0);
        }

        public boolean checkPerformanceTestResult(final HashMap<Integer, Object> result) {
            if (checkErrcode(result)) {
                int totalTime = 0;
                if (result.containsKey(TestResultParser.TEST_TOKEN_TOTAL_TIME)) {
                    totalTime = (Integer) result
                            .get(TestResultParser.TEST_TOKEN_TOTAL_TIME);
                }

                if (checkPerformanceTestResult(0, totalTime)) {
                    return true;
                }
            }

            return false;
        }

        public boolean checkPerformanceTestResult(int errCode, int totalTime) {
            return (errCode == 0) && (totalTime < mThresHold.totalTime);
        }

        public boolean checkCaptureTestResult(final HashMap<Integer, Object> result) {
            if (checkErrcode(result)) {
                int imageQuality = 0;
                int validArea = 0;

                if (result.containsKey(TestResultParser.TEST_TOKEN_IMAGE_QUALITY)) {
                    imageQuality = (Integer) result
                            .get(TestResultParser.TEST_TOKEN_IMAGE_QUALITY);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_VALID_AREA)) {
                    validArea = (Integer) result
                            .get(TestResultParser.TEST_TOKEN_VALID_AREA);
                }

                Log.d(TAG, "image quality: " + imageQuality + ", " + "validArea: " + validArea);

                if (checkCaptureTestResult(0, imageQuality, validArea)) {
                    return true;
                }
            }

            return false;
        }

        public boolean checkCaptureTestResult(int errCode, int imageQuality, int validArea) {
            return (errCode == 0) && (imageQuality >= mThresHold.imageQuality)
                    && (validArea >= mThresHold.validArea);
        }

        public boolean checkAlgoTestResult(final HashMap<Integer, Object> result) {
            return checkErrcode(result);
        }

        public boolean checkAlgoTestResult(int errCode) {
            return (errCode == 0);
        }

        public boolean checkBadPointTestResult(final HashMap<Integer, Object> result) {
            return checkErrcode(result);
        }

        public boolean checkBadPointTestResult(CheckPoint checkPoint) {
            return (checkPoint.mErrorCode == 0);
        }

        public boolean checkBioTestResultWithoutTouched(final HashMap<Integer, Object> result) {
            return checkErrcode(result);
        }

        public boolean checkBioTestResultWithoutTouched(int errCode, int base, int avg) {
            return (errCode == 0);
        }

        public boolean checkBioTestResultWithTouched(final HashMap<Integer, Object> result) {
            return checkErrcode(result);
        }

        public boolean checkBioTestResultWithTouched(int errCode, int base, int avg) {
            return (errCode == 0);
        }

        public boolean checkHBDTestResultWithTouched(final HashMap<Integer, Object> result) {
            return checkErrcode(result);
        }

        public boolean checkHBDTestResultWithTouched(int errCode, int avg, int electricity) {
            return (errCode == 0);
        }
    }

    public static class TestResultCheckerFactory {

        private static Checker mChecker = null;

        public synchronized static Checker getInstance(int chipType, int chipSeries) {

            if (mChecker == null || mChecker.mChipType != chipType) {
                switch (chipSeries) {
                    case Constants.GF_OSWEGO_M: {
                        mChecker = new OswegoMChecker(chipType);
                        break;
                    }

                    case Constants.GF_MILAN_F_SERIES: {
                        mChecker = new MilanFSeriesChecker(chipType);
                        break;
                    }

                    case Constants.GF_MILAN_A_SERIES: {
                        mChecker = new MilanASeriesChecker(chipType);
                        break;
                    }

                    default:
                        break;
                }
            }

            return mChecker;
        }
    }

    public static class OswegoMChecker extends Checker {

        private static final int[] TEST_ITEM_OSWEGO = { //
                TEST_SPI, /**/
                TEST_PIXEL, /**/
                TEST_RESET_PIN, /**/
                TEST_BAD_POINT, /**/
                TEST_PERFORMANCE, /**/
                TEST_CAPTURE, /**/
                TEST_ALGO, /**/
                TEST_FW_VERSION
        };

        @Override
        public int[] getTestItems(int index) {
            return TEST_ITEM_OSWEGO;
        }

        public OswegoMChecker(int chipType) {
            mChipType = chipType;
            mThresHold = new Threshold(mChipType);
        }

        @Override
        public boolean checkSpiTestResult(HashMap<Integer, Object> result) {
            if (super.checkSpiTestResult(result)) {
                String fwVersion = null;
                if (result.containsKey(TestResultParser.TEST_TOKEN_FW_VERSION)) {
                    fwVersion = (String) result.get(TestResultParser.TEST_TOKEN_FW_VERSION);
                }

                if (checkSpiTestResult(0, fwVersion, 0, 0)) {
                    return true;
                }
            }

            return false;
        }

        @Override
        public boolean checkSpiTestResult(int errCode, String fwVersion, int ChipId, int sensorOtpType) {

            return (errCode == 0)
                    && (fwVersion != null && fwVersion.startsWith(mThresHold.spiFwVersion));
        }

        @Override
        public boolean checkFwVersionTestResult(HashMap<Integer, Object> result) {

            if (super.checkFwVersionTestResult(result)) {
                String fwVersion = null;
                String codeFwVersion = null;
                if (result.containsKey(TestResultParser.TEST_TOKEN_FW_VERSION)) {
                    fwVersion = (String) result.get(TestResultParser.TEST_TOKEN_FW_VERSION);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_CODE_FW_VERSION)) {
                    codeFwVersion = (String) result
                            .get(TestResultParser.TEST_TOKEN_CODE_FW_VERSION);
                }

                Log.d(TAG, "codeFwVersion: " + codeFwVersion + ", ");

                if (checkFwVersionTestResult(0, fwVersion, codeFwVersion)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public boolean checkFwVersionTestResult(int errCode, String fwVersion, String codeFwVersion) {
            return (errCode == 0) && (fwVersion != null)
                    && (fwVersion.startsWith(codeFwVersion));
        }

        @Override
        public boolean checkBadPointTestResult(HashMap<Integer, Object> result) {
            if (super.checkBadPointTestResult(result)) {
                CheckPoint checkPoint = new CheckPoint();
                checkPoint.mAvgDiffVal = 0;
                checkPoint.mBadPixelNum = 0;
                checkPoint.mLocalBadPixelNum = 0;
                checkPoint.mAllTiltAngle = 0;
                checkPoint.mBlockTiltAngleMax = 0;
                checkPoint.mBigBadPixel = 0;
                checkPoint.mSmallBadPixel = 0;

                if (result.containsKey(TestResultParser.TEST_TOKEN_AVG_DIFF_VAL)) {
                    checkPoint.mAvgDiffVal = (Short) result
                            .get(TestResultParser.TEST_TOKEN_AVG_DIFF_VAL);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_BAD_PIXEL_NUM)) {
                    checkPoint.mBadPixelNum = (Integer) result
                            .get(TestResultParser.TEST_TOKEN_BAD_PIXEL_NUM);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_LOCAL_BAD_PIXEL_NUM)) {
                    checkPoint.mLocalBadPixelNum = (Integer) result
                            .get(TestResultParser.TEST_TOKEN_LOCAL_BAD_PIXEL_NUM);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_ALL_TILT_ANGLE)) {
                    checkPoint.mAllTiltAngle = (Float) result
                            .get(TestResultParser.TEST_TOKEN_ALL_TILT_ANGLE);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_BLOCK_TILT_ANGLE_MAX)) {
                    checkPoint.mBlockTiltAngleMax = (Float) result
                            .get(TestResultParser.TEST_TOKEN_BLOCK_TILT_ANGLE_MAX);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_LOCAL_SMALL_BAD_PIXEL_NUM)) {
                    checkPoint.mSmallBadPixel = (Integer) result
                            .get(TestResultParser.TEST_TOKEN_LOCAL_SMALL_BAD_PIXEL_NUM);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_LOCAL_BIG_BAD_PIXEL_NUM)) {
                    checkPoint.mBigBadPixel = (Integer) result
                            .get(TestResultParser.TEST_TOKEN_LOCAL_BIG_BAD_PIXEL_NUM);
                }

                if (checkBadPointTestResult(checkPoint)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public boolean checkBadPointTestResult(CheckPoint checkPoint) {
            Log.i(TAG, "checkPoint:" + checkPoint.toString());
            return (null != checkPoint) && (checkPoint.mErrorCode == 0) && (checkPoint.mBadPixelNum < mThresHold.badPixelNum
                    && checkPoint.mSmallBadPixel < mThresHold.localSmallBadPixel
                    && checkPoint.mBigBadPixel < mThresHold.localBigBadPixel
                    && checkPoint.mAvgDiffVal > mThresHold.avgDiffVal);
        }

    }

    public static class MilanASeriesChecker extends Checker {

        public static final int[] TEST_ITEM_MILANA = { //
                TEST_SPI, /**/
                TEST_PIXEL, /**/
                TEST_RESET_PIN, /**/
                TEST_FW_VERSION, /**/
                TEST_BIO_CALIBRATION, /**/
                TEST_HBD_CALIBRATION, /**/
                // TEST_BAD_POINT, /**/
                TEST_PERFORMANCE, /**/
                TEST_CAPTURE, /**/
                TEST_ALGO, /**/
        };

        public static final int[] TEST_ITEM_MILANA_1 = {
                TEST_SPI, /**/
                TEST_PIXEL, /**/
                TEST_RESET_PIN, /**/
                TEST_FW_VERSION, /**/
                // TEST_BAD_POINT, /**/
                TEST_PERFORMANCE, /**/
                TEST_CAPTURE, /**/
                TEST_ALGO, /**/
                TEST_BIO_CALIBRATION, /**/
                TEST_HBD_CALIBRATION, /**/
        };

        @Override
        public int[] getTestItems(int index) {
            switch (index) {
                case 0:
                    return TEST_ITEM_MILANA;
                case 1:
                    return TEST_ITEM_MILANA_1;
                default:
                    return TEST_ITEM_MILANA;
            }
        }

        public MilanASeriesChecker(int chipType) {
            mChipType = chipType;
            mThresHold = new Threshold(mChipType);
        }

        @Override
        public boolean checkSpiTestResult(HashMap<Integer, Object> result) {
            if (super.checkSpiTestResult(result)) {
                String fwVersion = null;
                int sensorOtpType = 0;
                if (result.containsKey(TestResultParser.TEST_TOKEN_FW_VERSION)) {
                    fwVersion = (String) result.get(TestResultParser.TEST_TOKEN_FW_VERSION);
                }
                if (result.containsKey(TestResultParser.TEST_TOKEN_SENSOR_OTP_TYPE)) {
                    sensorOtpType = Integer.valueOf(result.get(TestResultParser.TEST_TOKEN_SENSOR_OTP_TYPE).toString());
                }
                if (checkSpiTestResult(0, fwVersion, 0, sensorOtpType)) {
                    return true;
                }
            }

            return false;
        }

        @Override
        public boolean checkSpiTestResult(int errCode, String fwVersion, int ChipId, int sensorOtpType) {

            return (errCode == 0) && (fwVersion != null)
                    && fwVersion.startsWith(mThresHold.spiFwVersion);
        }

        @Override
        public boolean checkFwVersionTestResult(HashMap<Integer, Object> result) {

            if (super.checkFwVersionTestResult(result)) {
                String fwVersion = null;
                String codeFwVersion = null;
                int sensorOtpType = 0;
                if (result.containsKey(TestResultParser.TEST_TOKEN_FW_VERSION)) {
                    fwVersion = (String) result.get(TestResultParser.TEST_TOKEN_FW_VERSION);
                }
                if (result.containsKey(TestResultParser.TEST_TOKEN_SENSOR_OTP_TYPE)) {
                    sensorOtpType = Integer.valueOf(result.get(TestResultParser.TEST_TOKEN_SENSOR_OTP_TYPE).toString());
                }
                if (result.containsKey(TestResultParser.TEST_TOKEN_CODE_FW_VERSION)) {
                    codeFwVersion = (String) result
                            .get(TestResultParser.TEST_TOKEN_CODE_FW_VERSION);
                }

                if (checkFwVersionTestResult(0, fwVersion, codeFwVersion)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public boolean checkFwVersionTestResult(int errCode, String fwVersion, int sensorOtpType) {

            return (errCode == 0) && (fwVersion != null);
        }
        @Override
        public boolean checkFwVersionTestResult(int errCode, String fwVersion, String codeFwVersion) {
            Log.d(TAG, "fwVersion.trim()="+fwVersion.trim()+", codeFwVersion="+codeFwVersion);
            return (errCode == 0) && (fwVersion != null)
                    && fwVersion.trim().equals(codeFwVersion.trim());
        }

        @Override
        public boolean checkBadPointTestResult(HashMap<Integer, Object> result) {
            if (super.checkBadPointTestResult(result)) {

                CheckPoint checkPoint = new CheckPoint();
                checkPoint.mBadPixelNum = 0;
                checkPoint.mLocalBadPixelNum = 0;
                checkPoint.mLocalWorst = 0;
                checkPoint.mInCircle = 0;

                if (result.containsKey(TestResultParser.TEST_TOKEN_BAD_PIXEL_NUM)) {
                    checkPoint.mBadPixelNum = (Integer) result
                            .get(TestResultParser.TEST_TOKEN_BAD_PIXEL_NUM);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_LOCAL_BAD_PIXEL_NUM)) {
                    checkPoint.mLocalBadPixelNum = (Integer) result
                            .get(TestResultParser.TEST_TOKEN_LOCAL_BAD_PIXEL_NUM);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_LOCAL_WORST)) {
                    checkPoint.mLocalWorst = (Short) result
                            .get(TestResultParser.TEST_TOKEN_LOCAL_WORST);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_IN_CIRCLE)) {
                    checkPoint.mInCircle = (Short) result.get(TestResultParser.TEST_TOKEN_IN_CIRCLE);
                }

                if (checkBadPointTestResult(checkPoint)) {
                    return true;
                }

            }
            return false;
        }

        @Override
        public boolean checkBadPointTestResult(CheckPoint checkPoint) {
            return (null != checkPoint) && (checkPoint.mErrorCode == 0) && (checkPoint.mBadPixelNum < mThresHold.badPixelNum
                    && checkPoint.mLocalBadPixelNum < mThresHold.localBadPixelNum
                    && checkPoint.mLocalWorst < mThresHold.localWorst && checkPoint.mInCircle < mThresHold.inCircle);
        }

        @Override
        public boolean checkBioTestResultWithTouched(HashMap<Integer, Object> result) {

            if (super.checkBioTestResultWithTouched(result)) {
                int baseValue = 0;
                int avgValue = 0;

                if (result.containsKey(TestResultParser.TEST_TOKEN_HBD_BASE)) {
                    baseValue = (Short) result.get(TestResultParser.TEST_TOKEN_HBD_BASE);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_HBD_AVG)) {
                    avgValue = (Short) result.get(TestResultParser.TEST_TOKEN_HBD_AVG);
                }

                if (checkBioTestResultWithTouched(0, baseValue, avgValue)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public boolean checkBioTestResultWithTouched(int errCode, int base, int avg) {
            return (errCode == 0) && (Math.abs(base - avg) > mThresHold.osdTouched);
        }

        @Override
        public boolean checkBioTestResultWithoutTouched(HashMap<Integer, Object> result) {

            if (super.checkBioTestResultWithTouched(result)) {
                int baseValue = 0;
                int avgValue = 0;

                if (result.containsKey(TestResultParser.TEST_TOKEN_HBD_BASE)) {
                    baseValue = (Short) result.get(TestResultParser.TEST_TOKEN_HBD_BASE);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_HBD_AVG)) {
                    avgValue = (Short) result.get(TestResultParser.TEST_TOKEN_HBD_AVG);
                }

                if (checkBioTestResultWithoutTouched(0, baseValue, avgValue)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public boolean checkBioTestResultWithoutTouched(int errCode, int base, int avg) {
            return (errCode == 0) && (Math.abs(base - avg) < mThresHold.osdUntoucded);
        }

        @Override
        public boolean checkHBDTestResultWithTouched(HashMap<Integer, Object> result) {
            if (super.checkHBDTestResultWithTouched(result)) {
                int avgValue = 0;
                int electricity = 0;

                if (result.containsKey(TestResultParser.TEST_TOKEN_HBD_AVG)) {
                    avgValue = (Short) result.get(TestResultParser.TEST_TOKEN_HBD_AVG);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_ELECTRICITY_VALUE)) {
                    electricity = (Integer) result
                            .get(TestResultParser.TEST_TOKEN_ELECTRICITY_VALUE);
                }

                if (checkHBDTestResultWithTouched(0, avgValue, electricity)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public boolean checkHBDTestResultWithTouched(int errCode, int avg, int electricity) {
            return (errCode == 0) && (avg <= mThresHold.hbdAvgMax && avg >= mThresHold.hbdAvgMin)
                    && (electricity <= mThresHold.hbdElectricity);
        }

    }

    public static class MilanFSeriesChecker extends Checker {
        // add fingerprint test for runtime 20170518 dingfan start
        private static final int[] TEST_ITEM_MILAN_F_SERIES = { //
                TEST_SPI, /**/
                TEST_PIXEL, /**/
                TEST_RESET_PIN, /**/
                //TEST_BAD_POINT, /**/
                //TEST_PERFORMANCE, /**/
                //TEST_CAPTURE, /**/
                //TEST_ALGO, /**/
                TEST_FW_VERSION
        };
        // add fingerprint test for runtime 20170518 dingfan end
        @Override
        public int[] getTestItems(int index) {
            return TEST_ITEM_MILAN_F_SERIES;
        }

        public MilanFSeriesChecker(int chipType) {
            mChipType = chipType;
            mThresHold = new Threshold(mChipType);
        }

        @Override
        public boolean checkSpiTestResult(HashMap<Integer, Object> result) {
            if (super.checkSpiTestResult(result)) {
                int chipID = 0;
                if (result.containsKey(TestResultParser.TEST_TOKEN_CHIP_ID)) {
                    byte[] chip = (byte[]) result.get(TestResultParser.TEST_TOKEN_CHIP_ID);
                    if (chip != null && chip.length >= 4) {
                        chipID = TestResultParser.decodeInt32(chip, 0) >> 8;
                    }
                }
                if (checkSpiTestResult(0, null, chipID, 0)) {
                    return true;
                }
            }

            return false;
        }

        @Override
        public boolean checkSpiTestResult(int errCode, String fwVersion, int chipId, int sensorOtpType) {
            return (errCode == 0) && (chipId == mThresHold.chipId);
        }

        @Override
        public boolean checkBadPointTestResult(HashMap<Integer, Object> result) {
            if (super.checkBadPointTestResult(result)) {

                CheckPoint checkPoint = new CheckPoint();
                checkPoint.mBadPixelNum = 0;
                checkPoint.mLocalBadPixelNum = 0;
                checkPoint.mLocalWorst = 0;
                checkPoint.mSingular = 0;

                if (result.containsKey(TestResultParser.TEST_TOKEN_BAD_PIXEL_NUM)) {
                    checkPoint.mBadPixelNum = (Integer) result
                            .get(TestResultParser.TEST_TOKEN_BAD_PIXEL_NUM);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_LOCAL_BAD_PIXEL_NUM)) {
                    checkPoint.mLocalBadPixelNum = (Integer) result
                            .get(TestResultParser.TEST_TOKEN_LOCAL_BAD_PIXEL_NUM);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_LOCAL_WORST)) {
                    checkPoint.mLocalWorst = (Short) result
                            .get(TestResultParser.TEST_TOKEN_LOCAL_WORST);
                }

                if (result.containsKey(TestResultParser.TEST_TOKEN_SINGULAR)) {
                    checkPoint.mSingular = (Integer) result.get(TestResultParser.TEST_TOKEN_SINGULAR);
                }

                if (checkBadPointTestResult(checkPoint)) {
                    return true;
                }

            }
            return false;
        }

        @Override
        public boolean checkBadPointTestResult(CheckPoint checkPoint) {
            return (null != checkPoint) && (checkPoint.mErrorCode == 0) && (checkPoint.mBadPixelNum < mThresHold.badPixelNum
                    && checkPoint.mLocalBadPixelNum < mThresHold.localBadPixelNum
                    && checkPoint.mLocalWorst < mThresHold.localWorst);
        }

    }

    public static class CheckPoint {
        public int mErrorCode = 0;
        public short mAvgDiffVal = 0;
        public int mBadPixelNum = 0;
        public int mLocalBadPixelNum = 0;
        public float mAllTiltAngle = 0;
        public float mBlockTiltAngleMax = 0;
        public short mLocalWorst = 0;
        public int mSingular = 0;
        public short mInCircle = 0;
        public int mSmallBadPixel = 0;
        public int mBigBadPixel = 0;

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("error code:").append(mErrorCode).append("\n")
            .append("avgDiffVal:").append(mAvgDiffVal).append("\n")
            .append("badPixelNum:").append(mBadPixelNum).append("\n")
            .append("localBadPixelNum:").append(mLocalBadPixelNum).append("\n")
            .append("allTiltAngle:").append(mAllTiltAngle).append("\n")
            .append("blockTiltAngleMax:").append(mBlockTiltAngleMax).append("\n")
            .append("localWorst:").append(mLocalWorst).append("\n")
            .append("singular:").append(mSingular).append("\n")
            .append("inCircle:").append(mInCircle).append("\n")
            .append("smallBadPixel:").append(mSmallBadPixel).append("\n")
            .append("bigBadPixel:").append(mBigBadPixel).append("\n");

            return builder.toString();
        }
    }
}
