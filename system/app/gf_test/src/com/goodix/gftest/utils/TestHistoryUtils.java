
package com.goodix.gftest.utils;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.utils.TestResultParser;

import android.os.Environment;
import android.util.Log;

public class TestHistoryUtils {
    private static final String TAG = "TestHistoryUtils";
    private static final String TEST_HISTORY_FILE_NAME_FORMAT = "yyyyMMddHHmmss";
    private static String TEST_HISTORY_DIR_PATH = null;
    private static String TEST_HISTORY_DETAIL_FILE_PATH = null;
    private static String TEST_HISTORY_RESULT_FILE_PATH = null;

    public static void init(String rootPath, String resultFileName, String detailFileName) {
        if (rootPath == null) {
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                rootPath = Environment.getExternalStorageDirectory().getPath();
            }
        }

        StringBuilder dirPath = new StringBuilder();
        dirPath.append(rootPath);
        dirPath.append(File.separatorChar);
        dirPath.append(Constants.TEST_HISTORY_PATH);
        TEST_HISTORY_DIR_PATH = dirPath.toString();

        File dirFile = new File(TEST_HISTORY_DIR_PATH);
        if (!dirFile.exists() || !dirFile.isDirectory()) {
            dirFile.mkdirs();
        }

        if (detailFileName == null) {
            SimpleDateFormat df = new SimpleDateFormat(TEST_HISTORY_FILE_NAME_FORMAT);
            detailFileName = "detail" + df.format(new Date()) + ".dat";
        }
        if (resultFileName == null) {
            SimpleDateFormat df = new SimpleDateFormat(TEST_HISTORY_FILE_NAME_FORMAT);
            resultFileName = "result" + df.format(new Date()) + ".dat";
        }

        StringBuilder detailFilePath = new StringBuilder();
        detailFilePath.append(TEST_HISTORY_DIR_PATH).append(File.separatorChar)
        .append(detailFileName);
        TEST_HISTORY_DETAIL_FILE_PATH = detailFilePath.toString();
        StringBuilder resultFilePath = new StringBuilder();
        resultFilePath.append(TEST_HISTORY_DIR_PATH).append(File.separatorChar)
        .append(resultFileName);
        TEST_HISTORY_RESULT_FILE_PATH = resultFilePath.toString();
    }

    public static void clearHistory() {
        File file = new File(TEST_HISTORY_DETAIL_FILE_PATH);
        if (file != null && file.exists()) {
            file.delete();
        }

        file = new File(TEST_HISTORY_RESULT_FILE_PATH);
        if (file != null && file.exists()) {
            file.delete();
        }
    }

    public static void addDetail(int testId, byte[] result) {
        if (TEST_HISTORY_DETAIL_FILE_PATH == null) {
            init(null, null, null);
        }

        RandomAccessFile file = null;
        try {
            file = new RandomAccessFile(TEST_HISTORY_DETAIL_FILE_PATH, "rw");
            file.seek(file.length());

            if (testId > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("test_id=");
                sb.append(testId);
                sb.append("\n");
                file.writeBytes(sb.toString());
            }

            file.write(result);
            file.writeBytes("\n");
        } catch (IOException e) {
        }

        if (file != null) {
            try {
                file.close();
            } catch (IOException e) {
            }
        }
    }

    // dedicated patch function for TEST_BIO_ASSAY
    public static void addDetail(int testId, HashMap<Integer, Object> result1,
            HashMap<Integer, Object> result2) {
        StringBuilder sb = new StringBuilder();

        if (testId == TestResultChecker.TEST_BIO_CALIBRATION) {
            if (result1 != null) {
                sb.append("base0=").append(result1.get(TestResultParser.TEST_TOKEN_HBD_BASE))
                .append(",");
                sb.append("r0=").append(result1.get(TestResultParser.TEST_TOKEN_HBD_AVG))
                .append(",");
                sb.append("error_code=")
                .append(result1.get(TestResultParser.TEST_TOKEN_ERROR_CODE));
            }
            if (result2 != null) {
                sb.append("\n");
                sb.append("base1=").append(result2.get(TestResultParser.TEST_TOKEN_HBD_BASE))
                .append(",");
                sb.append("r1=").append(result2.get(TestResultParser.TEST_TOKEN_HBD_AVG))
                .append(",");
                sb.append("error_code=")
                .append(result2.get(TestResultParser.TEST_TOKEN_ERROR_CODE));
            }
        } else if (testId == TestResultChecker.TEST_HBD_CALIBRATION) {
            if (result1 != null) {
                sb.append("base2=").append(result1.get(TestResultParser.TEST_TOKEN_HBD_BASE))
                .append(",");
                sb.append("r2=").append(result1.get(TestResultParser.TEST_TOKEN_HBD_AVG))
                .append(",");
                sb.append("error_code=")
                .append(result1.get(TestResultParser.TEST_TOKEN_ERROR_CODE));
            }
            if (result2 != null) {
                sb.append("\n");
                sb.append("r3=").append(result2.get(TestResultParser.TEST_TOKEN_HBD_AVG))
                .append(",");
                sb.append("electricity=")
                .append(result2.get(TestResultParser.TEST_TOKEN_ELECTRICITY_VALUE))
                .append(",");
                sb.append("error_code=")
                .append(result2.get(TestResultParser.TEST_TOKEN_ERROR_CODE));
            }
        }
        addDetail(testId, sb.toString());
    }

    public static void addDetail(int testId, HashMap<Integer, Object> result) {
        StringBuilder sb = new StringBuilder();
        if (result != null) {
            for (Integer key : result.keySet()) {
                switch (key) {
                    case TestResultParser.TEST_TOKEN_ERROR_CODE:
                        sb.append("error_code=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_ALGO_VERSION:
                        sb.append("algo_version=");
                        sb.append(((String) result.get(key)).trim());
                        break;

                    case TestResultParser.TEST_TOKEN_PREPROCESS_VERSION:
                        sb.append("preprocess_version=");
                        sb.append(((String) result.get(key)).trim());
                        break;

                    case TestResultParser.TEST_TOKEN_FW_VERSION:
                        sb.append("fw_version=");
                        sb.append(((String) result.get(key)).trim());
                        break;
                    case TestResultParser.TEST_TOKEN_SENSOR_OTP_TYPE:
                        sb.append("sensor_otp_type=");
                        sb.append(result.get(key).toString().trim());
                        break;

                    case TestResultParser.TEST_TOKEN_CODE_FW_VERSION:
                        sb.append("code_fw_version=");
                        sb.append(((String) result.get(key)).trim());
                        break;

                    case TestResultParser.TEST_TOKEN_TEE_VERSION:
                        sb.append("tee_version=");
                        sb.append(((String) result.get(key)).trim());
                        break;

                    case TestResultParser.TEST_TOKEN_TA_VERSION:
                        sb.append("ta_version=");
                        sb.append(((String) result.get(key)).trim());
                        break;

                    case TestResultParser.TEST_TOKEN_CHIP_ID:
                        sb.append("chip_id=");
                        byte[] chip_id = (byte[]) result.get(key);
                        sb.append(TestResultParser.decodeInt32(chip_id, 0));
                        break;

                    case TestResultParser.TEST_TOKEN_VENDOR_ID:
                        sb.append("vendor_id=");
                        byte[] vendor_id = (byte[]) result.get(key);
                        sb.append(Integer.toHexString(vendor_id[0]));
                        break;

                    case TestResultParser.TEST_TOKEN_SENSOR_ID:
                        sb.append("sensor_id=");
                        byte[] sensor_id = (byte[]) result.get(key);
                        sb.append(sensor_id);
                        break;

                    case TestResultParser.TEST_TOKEN_PRODUCTION_DATE:
                        sb.append("production_date=");
                        byte[] production_date = (byte[]) result.get(key);
                        sb.append(('0' + production_date[0]));
                        sb.append(('0' + production_date[1]));
                        sb.append(('0' + production_date[2]));
                        sb.append(('0' + production_date[3]));
                        sb.append(('0' + production_date[4]));
                        sb.append(('0' + production_date[5]));
                        break;

                    case TestResultParser.TEST_TOKEN_CHIP_TYPE:
                        sb.append("chip_type=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_CHIP_SERIES:
                        sb.append("chip_series=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_AVG_DIFF_VAL:
                        sb.append("avg_diff_val=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_NOISE:
                        sb.append("noise=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_BAD_PIXEL_NUM:
                        sb.append("bad_pixel_num=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_LOCAL_BAD_PIXEL_NUM:
                        sb.append("local_bad_pixel_num=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_ALL_TILT_ANGLE:
                        sb.append("all_tilt_angle=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_BLOCK_TILT_ANGLE_MAX:
                        sb.append("block_tilt_angle_max=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_LOCAL_WORST:
                        sb.append("local_worst=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_SINGULAR:
                        sb.append("singular=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_IN_CIRCLE:
                        sb.append("in_circle=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_BIG_BUBBLE:
                        sb.append("big_bubble=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_LINE:
                        sb.append("line=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_GET_DR_TIMESTAMP_TIME:
                        sb.append("get_timestamp_time=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_GET_MODE_TIME:
                        sb.append("get_mode_time=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_GET_FW_VERSION_TIME:
                        sb.append("get_fw_version_time=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_GET_IMAGE_TIME:
                        sb.append("get_image_time=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_RAW_DATA_LEN:
                        sb.append("raw_data_len=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_IMAGE_QUALITY:
                        sb.append("image_quality=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_VALID_AREA:
                        sb.append("valid_area=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_KEY_POINT_NUM:
                        sb.append("key_point_num=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_INCREATE_RATE:
                        sb.append("increate_rate=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_OVERLAY:
                        sb.append("overlay=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_GET_RAW_DATA_TIME:
                        sb.append("get_raw_data_time=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_PREPROCESS_TIME:
                        sb.append("preprocess_time=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_GET_FEATURE_TIME:
                        sb.append("get_feature_time=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_ENROLL_TIME:
                        sb.append("enroll_time=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_AUTHENTICATE_TIME:
                        sb.append("authenticate_time=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_AUTHENTICATE_UPDATE_FLAG:
                        sb.append("authenticate_update_flag=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_AUTHENTICATE_FINGER_COUNT:
                        sb.append("authenticate_finger_count=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_AUTHENTICATE_FINGER_ITME:
                        sb.append("authenticate_finger_time=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_TOTAL_TIME:
                        sb.append("total_time=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_RESET_FLAG:
                        sb.append("reset_flag=");
                        sb.append(result.get(key));
                        break;

                    case TestResultParser.TEST_TOKEN_RAW_DATA:
                        sb.append("raw_data=data");
                        break;

                    default:
                        break;

                }
                sb.append(",");
            }
        }
        addDetail(testId, sb.toString());
    }

    public static void addDetail(int testId, String result) {
        if (result == null) {
            return;
        }
        addDetail(testId, result.getBytes());
    }

    public static void addDetail(String result) {
        if (result == null) {
            return;
        }
        addDetail(0, result.getBytes());
    }

    public static void addResult(String result) {
        if (result == null) {
            return;
        }

        addResult(0, result);

    }

    public static void addResult(int testId, String result) {
        if (result == null) {
            return;
        }

        addResult(testId, result.getBytes());
    }

    public static void addResult(int testId, byte[] result) {
        if (TEST_HISTORY_RESULT_FILE_PATH == null) {
            init(null, null, null);
        }

        RandomAccessFile file = null;
        try {
            file = new RandomAccessFile(TEST_HISTORY_RESULT_FILE_PATH, "rw");
            file.seek(file.length());

            if (testId > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("test_id=");
                sb.append(testId);
                sb.append("\n");
                file.writeBytes(sb.toString());
            }

            file.write(result);
            file.writeBytes("\n");
        } catch (IOException e) {
        }

        if (file != null) {
            try {
                file.close();
            } catch (IOException e) {
            }
        }
    }

    private static void parseKVString(HashMap<String, Object> item, String kvStr) {
        String[] pair = kvStr.split("=");
        if (pair != null && pair.length > 1) {
            try {
                item.put(pair[0], pair[1]);
            } catch (NumberFormatException e) {
            }
        }
    }

    public static ArrayList<HashMap<String, Object>> load() {
        if (TEST_HISTORY_DETAIL_FILE_PATH == null) {
            return null;
        }

        ArrayList<HashMap<String, Object>> testResult = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> item = null;
        RandomAccessFile detailFile = null;
        RandomAccessFile resultFile = null;
        String line = null;
        try {
            resultFile = new RandomAccessFile(TEST_HISTORY_RESULT_FILE_PATH, "r");
            detailFile = new RandomAccessFile(TEST_HISTORY_DETAIL_FILE_PATH, "r");

            while (true) {
                // read the result from result file
                line = resultFile.readLine();
                if (line == null) {
                    break;
                }
                Log.d(TAG, line);
                if (line.startsWith("test_id")) {
                    item = new HashMap<String, Object>();

                    parseKVString(item, line);
                    continue; // read the next 'result' line
                } else if (line.startsWith("result")) {
                    parseKVString(item, line);

                    // now turn to read the detail file
                    while (true) {
                        line = detailFile.readLine();
                        if (line == null) {
                            break;
                        }

                        Log.d(TAG, line);

                        if (line.contains("error_code")) {
                            Log.d(TAG, "error_code");
                            String[] array = line.split(",");
                            for (String result : array) {
                                parseKVString(item, result);
                            }
                        } else if (line.startsWith("time:")) {
                            Log.d(TAG, "time");
                            parseKVString(item, line);

                            break; // return to read the result file...pfff
                        }
                    }
                    testResult.add(item);
                }
            }

        } catch (IOException e) {
        }

        if (detailFile != null) {
            try {
                detailFile.close();
            } catch (IOException e) {
            }
        }

        if (resultFile != null) {
            try {
                resultFile.close();
            } catch (IOException e) {
            }
        }

        return testResult;
    }
}
