
package com.goodix.gftest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.GFConfig;
import com.goodix.fingerprint.service.GoodixFingerprintManager;
import com.goodix.fingerprint.utils.TestParamEncoder;
import com.goodix.fingerprint.utils.TestResultParser;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

public class DownloadFwCfgTestActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "DownloadFwCfgTestActivity";

    private static final String PATH_FW = "fw";
    private static final String PATH_CFG = "cfg";
    private static final int GF_OSWEGO_M_FW_LENGTH = (4 + 8 + 2 + (32 + 4 + 2 + 4) * 1024);
    private static final int GF_OSWEGO_M_CFG_LENGTH = 249;

    private static final int GF_MILAN_A_SERIES_FW_LENGTH = 5120;
    private static final int GF_MILAN_A_SERIES_CFG_LENGTH = 256;

    private static final int INVALID_FW_FILE_LEN = 0;
    private static final int INVALID_FW_FILE_DATA = 1;
    private static final int INVALID_CFG_FILE_LEN = 2;

    private GoodixFingerprintManager mGoodixFingerprintManager = null;

    private Button mStartFwTestBtn = null;
    private Button mStartCfgTestBtn = null;
    private Button mResetFwCfgBtn =null;
    private Spinner mFwSpinner = null;
    private Spinner mCfgSpinner = null;
    private List<String> mFwItems = null; // Cache the file names to be shown in spinner
    private List<String> mCfgItems = null;
    private List<String> mFwPaths = null; // Cache the file path to be read
    private List<String> mCfgPaths = null;
    private Handler mHandler = new Handler();
    private ProgressBar mFwProgressBar = null;
    private ProgressBar mCfgProgressBar = null;
    private TextView mFwResultView = null;
    private TextView mCfgResultView = null;

    private GFConfig mConfig = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_fw_cfg);

        mGoodixFingerprintManager = new GoodixFingerprintManager(DownloadFwCfgTestActivity.this);
        mConfig = mGoodixFingerprintManager.getConfig();

        mFwItems = new ArrayList<String>();
        mCfgItems = new ArrayList<String>();
        mFwPaths = new ArrayList<String>();
        mCfgPaths = new ArrayList<String>();

        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getFileName(PATH_FW);
        getFileName(PATH_CFG);

        if (mFwItems.isEmpty()) {
            mStartFwTestBtn.setEnabled(false);
        }

        if (mCfgItems.isEmpty()) {
            mStartCfgTestBtn.setEnabled(false);
        }

        ArrayAdapter<String> adapterFw = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, mFwItems);
        mFwSpinner.setAdapter(adapterFw);
        ArrayAdapter<String> adapterCfg = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, mCfgItems);
        mCfgSpinner.setAdapter(adapterCfg);
        mGoodixFingerprintManager.registerTestCmdCallback(mTestCmdCallback);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);
        mGoodixFingerprintManager.unregisterTestCmdCallback(mTestCmdCallback);

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_download_fw_start: {
                int pos = mFwSpinner.getSelectedItemPosition();
                String fpath = mFwPaths.get(pos);

                byte[] fwData = readFwCfgFile(fpath);
                if (fwData == null || (fwData.length != GF_OSWEGO_M_FW_LENGTH
                        && fwData.length != GF_MILAN_A_SERIES_FW_LENGTH)) {
                    Log.e(TAG, "invalid fw file, length err, len " + fwData.length);
                    showInvalidFileDialog(INVALID_FW_FILE_LEN);
                    return;
                } else if (isOswegoX18M()) {
                    if (fwData[4] != 0x78 || fwData[5] != 0x31 || fwData[6] != 0x38) {
                        Log.e(TAG, "invalid x18 fw file");
                        showInvalidFileDialog(INVALID_FW_FILE_DATA);
                        return;
                    }
                } else if (isOswegoX16M()) {
                    if (fwData[4] != 0x78 || fwData[5] != 0x31 || fwData[6] != 0x36) {
                        Log.e(TAG, "invalid x16 fw file");
                        showInvalidFileDialog(INVALID_FW_FILE_DATA);
                        return;
                    }
                } else if (mConfig != null && mConfig.mChipSeries == Constants.GF_MILAN_A_SERIES) {
                    Log.i(TAG, "Down Milan a series fw file");
                    // return;
                } else {
                    return;
                }
                mStartFwTestBtn.setEnabled(false);
                mStartCfgTestBtn.setEnabled(false);
                mResetFwCfgBtn.setEnabled(false);

                mFwProgressBar.setVisibility(View.VISIBLE);
                mFwResultView.setVisibility(View.INVISIBLE);

                byte[] byteArray = new byte[TestParamEncoder.testEncodeSizeOfArray(fwData.length)];
                int offset = 0;
                offset = TestParamEncoder.encodeArray(byteArray, offset, TestResultParser.TEST_PARAM_TOKEN_FW_DATA,
                        fwData, fwData.length);
                mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_DOWNLOAD_FW, byteArray);
                break;
            }

            case R.id.btn_download_cfg_start: {
                int pos = mCfgSpinner.getSelectedItemPosition();
                String fpath = mCfgPaths.get(pos);

                byte[] cfgData = readFwCfgFile(fpath);
                if (cfgData == null || (cfgData.length != GF_OSWEGO_M_CFG_LENGTH
                        && cfgData.length != GF_MILAN_A_SERIES_CFG_LENGTH)) {
                    Log.e(TAG, "invalid cfg file, length err, len " + cfgData.length);
                    showInvalidFileDialog(INVALID_CFG_FILE_LEN);
                    return;
                }

                mStartFwTestBtn.setEnabled(false);
                mStartCfgTestBtn.setEnabled(false);
                mResetFwCfgBtn.setEnabled(false);

                mCfgProgressBar.setVisibility(View.VISIBLE);
                mCfgResultView.setVisibility(View.INVISIBLE);

                byte[] byteArray = new byte[TestParamEncoder.testEncodeSizeOfArray(cfgData.length)];
                int offset = 0;
                offset = TestParamEncoder.encodeArray(byteArray, offset, TestResultParser.TEST_PARAM_TOKEN_CFG_DATA,
                        cfgData, cfgData.length);
                mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_DOWNLOAD_CFG, byteArray);
                break;
            }

            case R.id.btn_fw_cfg_reset: {
                mFwResultView.setVisibility(View.INVISIBLE);
                mCfgResultView.setVisibility(View.INVISIBLE);
                mFwProgressBar.setVisibility(View.VISIBLE);
                mCfgProgressBar.setVisibility(View.VISIBLE);
                mStartFwTestBtn.setEnabled(false);
                mStartCfgTestBtn.setEnabled(false);
                mResetFwCfgBtn.setEnabled(false);

                mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_RESET_FWCFG, null);
                break;
            }
        }

    }

    public void initView() {
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        if (mConfig.mChipSeries != Constants.GF_MILAN_A_SERIES) {
            mStartFwTestBtn = (Button) findViewById(R.id.btn_download_fw_start);
            mStartFwTestBtn.setOnClickListener(this);
            mStartCfgTestBtn = (Button) findViewById(R.id.btn_download_cfg_start);
            mStartCfgTestBtn.setOnClickListener(this);
        } else {
            mStartFwTestBtn = (Button) findViewById(R.id.btn_download_fw_start);
            mStartFwTestBtn.setVisibility(View.GONE);
            mStartCfgTestBtn = (Button) findViewById(R.id.btn_download_cfg_start);
            mStartCfgTestBtn.setOnClickListener(new OnClickListener(){

                @Override
                public void onClick(View v) {
                    int fwpos = mFwSpinner.getSelectedItemPosition();
                    String fwpath = mFwPaths.get(fwpos);
                    int cfgpos = mCfgSpinner.getSelectedItemPosition();
                    String cfgpath = mCfgPaths.get(cfgpos);

                    byte[] fwData = readFwCfgFile(fwpath);
                    byte[] cfgData = readFwCfgFile(cfgpath);

                    mFwProgressBar.setVisibility(View.VISIBLE);
                    mFwResultView.setVisibility(View.INVISIBLE);

                    if (fwData.length != GF_MILAN_A_SERIES_FW_LENGTH) {
                        Log.e(TAG, "invalid fw file, length err, len " + fwData.length);
                        showInvalidFileDialog(INVALID_FW_FILE_LEN);
                        return;
                    }
                    if (cfgData.length != GF_MILAN_A_SERIES_CFG_LENGTH) {
                        Log.e(TAG, "invalid cfg file, length err, len " + cfgData.length);
                        showInvalidFileDialog(INVALID_CFG_FILE_LEN);
                        return;
                    }

                    byte[] byteArray = new byte[TestParamEncoder.testEncodeSizeOfArray(fwData.length)
                                                + TestParamEncoder.testEncodeSizeOfArray(cfgData.length)];
                    int offset = 0;
                    offset = TestParamEncoder.encodeArray(byteArray, offset, TestResultParser.TEST_PARAM_TOKEN_FW_DATA,
                            fwData, fwData.length);
                    offset = TestParamEncoder.encodeArray(byteArray, offset, TestResultParser.TEST_PARAM_TOKEN_CFG_DATA,
                            cfgData, cfgData.length);
                    mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_DOWNLOAD_FWCFG, byteArray);
                }
            });
        }

        mResetFwCfgBtn = (Button) findViewById(R.id.btn_fw_cfg_reset);
        mResetFwCfgBtn.setOnClickListener(this);
        mFwSpinner = (Spinner) findViewById(R.id.fw_spinner);
        mCfgSpinner = (Spinner) findViewById(R.id.cfg_spinner);
        mFwResultView = (TextView) findViewById(R.id.fw_test_result);
        mCfgResultView = (TextView) findViewById(R.id.cfg_test_result);
        mFwProgressBar = (ProgressBar) findViewById(R.id.fw_testing);
        mCfgProgressBar = (ProgressBar) findViewById(R.id.cfg_testing);
        mFwProgressBar.setVisibility(View.INVISIBLE);
        mCfgProgressBar.setVisibility(View.INVISIBLE);
        mFwResultView.setVisibility(View.INVISIBLE);
        mCfgResultView.setVisibility(View.INVISIBLE);
    }

    public void getFileName(String folder) {
        File fileFolder = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                + File.separator + folder);
        File[] files = fileFolder.listFiles();
        if (files == null) {
            Log.e(TAG, "Get a empty folder with no cfg or fw!");
            return;
        }

        for (File file : files) {
            String name = file.getName();
            if (folder.equals("fw")) {
                if (isOswegoX18M()) {
                    if (name != null && !name.startsWith("GFx18M")) {
                        continue;
                    }
                } else if (isOswegoX16M()) {
                    if (name != null && !name.startsWith("GFx16M")) {
                        continue;
                    }
                } else if (mConfig != null && mConfig.mChipSeries == Constants.GF_MILAN_A_SERIES) {
                    if (name != null && (!name.startsWith("GF52x6") && !name.startsWith("GF52x8"))) {
                        continue;
                    }
                } else {
                    break;
                }

                mFwItems.add(name);
                mFwPaths.add(file.getPath());
            } else {
                mCfgItems.add(name);
                mCfgPaths.add(file.getPath());
            }
        }
    }

    public byte[] readFwCfgFile(String filePath) {
        byte[] buffer = null;
        try {
            FileInputStream fin = new FileInputStream(filePath);
            int length = fin.available();

            buffer = new byte[length];
            fin.read(buffer);
            fin.close();
        } catch (IOException e) {
            Log.e(TAG, "Failed to open " + filePath);
        }
        return buffer;
    }

    private void showInvalidFileDialog(int reason) {
        int message = R.string.invalid_fw_file;

        switch (reason) {
            case INVALID_FW_FILE_LEN:
                message = R.string.invalid_fw_file;
                break;

            case INVALID_FW_FILE_DATA:
                message = R.string.invalid_fw_file;
                break;

            case INVALID_CFG_FILE_LEN:
                message = R.string.invalid_cfg_file;
                break;
        }

        new AlertDialog.Builder(DownloadFwCfgTestActivity.this)
        .setTitle(DownloadFwCfgTestActivity.this.getString(R.string.sytem_info))
        .setMessage(DownloadFwCfgTestActivity.this.getString(message))
        .setPositiveButton(DownloadFwCfgTestActivity.this.getString(R.string.ok),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        })
        .show();
    }

    private boolean isOswegoX18M() {
        if (mConfig == null) {
            return false;
        }

        if (mConfig.mChipType == Constants.GF_CHIP_318M
                || mConfig.mChipType == Constants.GF_CHIP_3118M
                || mConfig.mChipType == Constants.GF_CHIP_518M
                || mConfig.mChipType == Constants.GF_CHIP_5118M) {
            return true;
        }

        return false;
    }

    private boolean isOswegoX16M() {
        if (mConfig == null) {
            return false;
        }

        if (mConfig.mChipType == Constants.GF_CHIP_316M
                || mConfig.mChipType == Constants.GF_CHIP_516M
                || mConfig.mChipType == Constants.GF_CHIP_816M) {
            return true;
        }

        return false;
    }

    private GoodixFingerprintManager.TestCmdCallback mTestCmdCallback = new GoodixFingerprintManager.TestCmdCallback() {

        @Override
        public void onTestCmd(final int cmdId, final HashMap<Integer, Object> result) {
            Log.d(TAG, "onTestCmd cmdId = " + cmdId);

            if (result == null
                    || (cmdId != Constants.CMD_TEST_DOWNLOAD_FW
                    && cmdId != Constants.CMD_TEST_DOWNLOAD_CFG
                    && cmdId != Constants.CMD_TEST_DOWNLOAD_FWCFG
                    && cmdId != Constants.CMD_TEST_RESET_FWCFG)) {
                return;
            }

            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    if (cmdId != Constants.CMD_TEST_DOWNLOAD_FWCFG) {
                        mFwResultView.setVisibility(View.VISIBLE);
                        mCfgResultView.setVisibility(View.VISIBLE);
                        mFwProgressBar.setVisibility(View.INVISIBLE);
                        mCfgProgressBar.setVisibility(View.INVISIBLE);
                        if (mFwItems.isEmpty()) {
                            mStartFwTestBtn.setEnabled(false);
                        } else {
                            mStartFwTestBtn.setEnabled(true);
                        }
                        if (mCfgItems.isEmpty()) {
                            mStartCfgTestBtn.setEnabled(false);
                        } else {
                            mStartCfgTestBtn.setEnabled(true);
                        }
                        mResetFwCfgBtn.setEnabled(true);

                        int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
                        if (result.containsKey(TestResultParser.TEST_TOKEN_ERROR_CODE)) {
                            errorCode = (Integer) result
                                    .get(TestResultParser.TEST_TOKEN_ERROR_CODE);
                        }
                        Log.d(TAG, " " + errorCode);
                        if (errorCode == 0) {
                            if (cmdId == Constants.CMD_TEST_DOWNLOAD_FW) {
                                mFwResultView.setText(R.string.test_succeed);
                                mFwResultView.setTextColor(getResources()
                                        .getColor(R.color.test_succeed_color));
                            }
                            if (cmdId == Constants.CMD_TEST_DOWNLOAD_CFG) {
                                mCfgResultView.setText(R.string.test_succeed);
                                mCfgResultView.setTextColor(getResources()
                                        .getColor(R.color.test_succeed_color));
                            }
                            if (cmdId == Constants.CMD_TEST_RESET_FWCFG) {
                                mFwResultView.setText(R.string.test_succeed);
                                mFwResultView.setTextColor(getResources()
                                        .getColor(R.color.test_succeed_color));
                                mCfgResultView.setText(R.string.test_succeed);
                                mCfgResultView.setTextColor(getResources()
                                        .getColor(R.color.test_succeed_color));
                            }
                        } else {
                            if (cmdId == Constants.CMD_TEST_DOWNLOAD_FW) {
                                mFwResultView.setText(R.string.test_failed);
                                mFwResultView.setTextColor(getResources()
                                        .getColor(R.color.test_failed_color));
                            }
                            if (cmdId == Constants.CMD_TEST_DOWNLOAD_CFG) {
                                mCfgResultView.setText(R.string.test_failed);
                                mCfgResultView.setTextColor(getResources()
                                        .getColor(R.color.test_failed_color));
                            }
                            if (cmdId == Constants.CMD_TEST_RESET_FWCFG) {
                                mFwResultView.setText(R.string.test_failed);
                                mFwResultView.setTextColor(getResources()
                                        .getColor(R.color.test_failed_color));
                                mCfgResultView.setText(R.string.test_failed);
                                mCfgResultView.setTextColor(getResources()
                                        .getColor(R.color.test_failed_color));
                            }
                        }
                    } else {
                        mFwResultView.setVisibility(View.VISIBLE);
                        mCfgResultView.setVisibility(View.INVISIBLE);
                        mFwProgressBar.setVisibility(View.INVISIBLE);
                        mCfgProgressBar.setVisibility(View.INVISIBLE);
                        mStartFwTestBtn.setEnabled(false);
                        if (mCfgItems.isEmpty() || mFwItems.isEmpty()) {
                            mStartCfgTestBtn.setEnabled(false);
                        } else {
                            mStartCfgTestBtn.setEnabled(true);
                        }
                        mResetFwCfgBtn.setEnabled(true);

                        int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
                        if (result.containsKey(TestResultParser.TEST_TOKEN_ERROR_CODE)) {
                            errorCode = (Integer) result
                                    .get(TestResultParser.TEST_TOKEN_ERROR_CODE);
                        }
                        Log.d(TAG, String.valueOf(errorCode));
                        if (errorCode == 0) {
                            if (cmdId == Constants.CMD_TEST_DOWNLOAD_FWCFG) {
                                mFwResultView.setText(R.string.test_succeed);
                                mFwResultView.setTextColor(getResources()
                                        .getColor(R.color.test_succeed_color));
                            }
                        } else {
                            if (cmdId == Constants.CMD_TEST_DOWNLOAD_FWCFG) {
                                mFwResultView.setText(R.string.test_failed);
                                mFwResultView.setTextColor(getResources()
                                        .getColor(R.color.test_failed_color));
                            }
                        }
                    }
                }
            });
        }
    };
}
