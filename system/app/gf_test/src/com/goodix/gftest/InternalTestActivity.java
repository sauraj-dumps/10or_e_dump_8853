
package com.goodix.gftest;

import java.util.HashMap;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.GFConfig;
import com.goodix.fingerprint.GFDevice;
import com.goodix.fingerprint.service.GoodixFingerprintManager;
import com.goodix.fingerprint.service.GoodixFingerprintManager.TestCmdCallback;
import com.goodix.fingerprint.utils.TestParamEncoder;
import com.goodix.fingerprint.utils.TestResultParser;
import com.goodix.gftest.utils.TestResultChecker;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.util.Log;
import android.view.MenuItem;

public class InternalTestActivity extends PreferenceActivity
implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG = "InternalTestActivity";

    private static final String KEY_FW_CFG_DOWNLOAD = "key_fw_cfg_download";
    private static final String KEY_MAX_FINGERS = "key_max_fingers";
    private static final String KEY_MAX_FINGERS_PER_USER = "key_max_fingers_per_user";
    private static final String KEY_SUPPORT_KEY_MODE = "key_support_key_mode";
    private static final String KEY_SUPPORT_FF_MODE = "key_support_ff_mode";
    private static final String KEY_SUPPORT_POWER_KEY_FREATURE = "key_support_power_key_feature";
    private static final String KEY_FORBIDDEN_UNTRUSTED_ENROLL = "key_forbidden_untrusted_enroll";
    private static final String KEY_FORBIDDEN_ENROLL_DUPLICATE_FINGERS = "key_forbidden_enroll_duplicate_fingers";
    private static final String KEY_SUPPORT_BIO_ASSAY = "key_support_bio_assay";
    private static final String KEY_SUPPORT_PERFORMANCE_DUMP = "key_support_performance_dump";

    private static final String KEY_SUPPORT_NAV_MODE = "key_support_nav_mode";
    private static final String KEY_NAV_DOUBLE_CLICK_TIME = "key_nav_double_click_time";
    private static final String KEY_NAV_LONG_PRESS_TIME = "key_nav_long_press_time";

    private static final String KEY_ENROLLING_MIN_TEMPLATES = "key_enrolling_min_templates";

    public static final String KEY_AUTHENTICATE = "authenticate";
    public static final String KEY_BAD_POINT_BAD_PIXEL_NUM_THRESHOLD = "key_bad_point_bad_pixel_threshold";
    public static final String KEY_BAD_POINT_AVG_DIFF_VAL = "key_bad_point_avg_diff_val_threshold";
    public static final String KEY_LOCAL_SMALL_BAD_PIXEL = "key_bad_point_local_small_bad_pixel_threshold";
    public static final String KEY_LOCAL_BIG_BAD_PIXEL = "key_bad_point_local_big_bad_pixel_threshold";
    public static final String KEY_BAD_POINT_TOTAL_BAD_PIXEL_NUM = "key_bad_point_total_bad_pixel_num_threshold";
    public static final String KEY_BAD_POINT_LOCAL_BAD_PIXEL_NUM = "key_bad_point_local_bad_pixel_num_threshold";
    public static final String KEY_BAD_POINT_LOCAL_WORST_NUM = "key_bad_point_local_worst_num_threshold";

    private static final String KEY_VALID_IMAGE_QUALITY_THRESHOLD = "key_valid_image_quality_threshold";
    private static final String KEY_VALID_IMAGE_AREA_THRESHOLD = "key_valid_image_area_threshold";
    private static final String KEY_DUPLICATE_FINGER_OVERLAY_SCORE = "key_duplicate_finger_overlay_score";
    private static final String KEY_INCREATE_RATE_BETWEEN_STITCH_INFO = "key_increase_rate_between_stitch_info";

    private static final String KEY_SAFE_CLASS = "key_safe_class";
    private static final String KEY_SCREEN_ON_AUTHENTICATE_FAIL_RETRY_COUNT = "key_screen_on_authenticate_fail_retry_count";
    private static final String KEY_SCREEN_OFF_AUTHENTICATE_FAIL_RETRY_COUNT = "key_screen_off_authenticate_fail_retry_count";
    private static final String KEY_AUTHENTICATE_ORDER = "key_authenticate_order";

    private static final String KEY_REISSUE_KEY_DOWN_WHEN_ENTRY_FF_MODE = "key_reissue_key_down_when_entry_ff_mode";
    private static final String KEY_REISSUE_KEY_DOWN_WHEN_ENTRY_IMAGE_MODE = "key_reissue_key_down_when_entry_image_mode";

    private static final String KEY_SUPPORT_SENSOR_BROKEN_CHECK = "key_support_sensor_broken_check";
    private static final String KEY_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_SENSOR = "key_broken_pixel_threshold_for_disable_sensor";
    private static final String KEY_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_STUDY = "key_broken_pixel_threshold_for_disable_study";

    private static final String KEY_BAD_POINT_TEST_MAX_FRAME_NUMBER = "key_bad_point_test_max_frame_number";

    private static final String KEY_REPORT_KEY_EVENT_ONLY_ENROLL_AUTHENTICATE = "key_report_key_event_only_enroll_authenticate";

    private static final String KEY_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_IMAGE_MODE = "key_require_down_and_up_in_pairs_for_image_mode";
    private static final String KEY_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_FF_MODE = "key_require_down_and_up_in_pairs_for_ff_mode";
    private static final String KEY_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_KEY_MODE = "key_require_down_and_up_in_pairs_for_key_mode";
    private static final String KEY_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_NAV_MODE = "key_require_down_and_up_in_pairs_for_nav_mode";

    private static final String KEY_SUPPORT_SET_SPI_SPEED_IN_TEE = "key_support_set_spi_speed_in_tee";

    private SharedPreferences mSharedPreference = null;
    private GoodixFingerprintManager mGoodixFingerprintManager = null;
    private Handler mHandler = new Handler();
    private GFConfig mGFConfig = null;
    private GFDevice mGFDevice = null;
    private TestResultChecker.Checker mTestResultChecker;
    private PreferenceScreen mRootPref;
    private PreferenceCategory mAuthenCategory;
    private PreferenceCategory mBadPointCategory;

    @Override
    protected void onCreate(Bundle onSavedInstanceState) {
        super.onCreate(onSavedInstanceState);
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        addPreferencesFromResource(R.xml.internal_test);

        mGoodixFingerprintManager = new GoodixFingerprintManager(InternalTestActivity.this);
        mGFConfig = mGoodixFingerprintManager.getConfig();
        mGFDevice = mGoodixFingerprintManager.getDevice();

        mTestResultChecker = TestResultChecker.TestResultCheckerFactory.getInstance(mGFConfig.mChipType, mGFConfig.mChipSeries);

        mSharedPreference = getPreferenceScreen().getSharedPreferences();
        mSharedPreference.registerOnSharedPreferenceChangeListener(this);
        mRootPref = getPreferenceScreen();
        mAuthenCategory = (PreferenceCategory) mRootPref.findPreference(KEY_AUTHENTICATE);
        mBadPointCategory = (PreferenceCategory) mRootPref.findPreference("badpoint");

        updateView();
        updatePreference(mGFConfig.mChipSeries);

        PreferenceCategory featureCategory = (PreferenceCategory) findPreference("feature");
        featureCategory.removePreference(findPreference(KEY_MAX_FINGERS));
        featureCategory.removePreference(findPreference(KEY_MAX_FINGERS_PER_USER));

        if (null != mGFConfig && mGFConfig.mChipType != Constants.GF_CHIP_5206
                && mGFConfig.mChipType != Constants.GF_CHIP_5208) {
            featureCategory.removePreference(findPreference(KEY_SUPPORT_BIO_ASSAY));
        }

        ListPreference mNavListPreference = (ListPreference) findPreference(KEY_SUPPORT_NAV_MODE);
        if (Constants.GF_OSWEGO_M == mGFConfig.mChipSeries
                || Constants.GF_MILAN_A_SERIES == mGFConfig.mChipSeries) {

        } else if (Constants.GF_MILAN_F_SERIES == mGFConfig.mChipSeries) {
            mNavListPreference.setEntries(R.array.support_nav_mode_entries_milan_f_series);
            mNavListPreference.setEntryValues(R.array.support_nav_mode_entry_values_milan_f_series);
        }

        PreferenceCategory otherCategory = (PreferenceCategory) findPreference("other");
        if (null != mGFConfig) {
            if (Constants.GF_MILAN_A_SERIES == mGFConfig.mChipSeries) {
                otherCategory.removePreference(findPreference(KEY_SUPPORT_SENSOR_BROKEN_CHECK));
                otherCategory.removePreference(findPreference(KEY_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_SENSOR));
                otherCategory.removePreference(findPreference(KEY_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_STUDY));
            } else if (Constants.GF_MILAN_F_SERIES == mGFConfig.mChipSeries) {
                getPreferenceScreen().removePreference(findPreference(KEY_FW_CFG_DOWNLOAD));
            }
        }
        //      otherCategory
        //      .removePreference(findPreference(KEY_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_SENSOR));
        //      otherCategory
        //      .removePreference(findPreference(KEY_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_STUDY));
        otherCategory.removePreference(findPreference(KEY_SUPPORT_SET_SPI_SPEED_IN_TEE));

        updateSafeClassPreference();
    }

    @Override
    protected void onResume() {
        super.onResume();


        mGoodixFingerprintManager.registerTestCmdCallback(mTestCmdCallback);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mGoodixFingerprintManager.unregisterTestCmdCallback(mTestCmdCallback);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSharedPreference.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.d(TAG, "onSharedPreferenceChanged " + key);

        if (key.equals(KEY_SAFE_CLASS)) {
            if (Constants.GF_OSWEGO_M == mGFConfig.mChipSeries) {
                ListPreference listPreference = (ListPreference) findPreference(KEY_SAFE_CLASS);
                listPreference.setEntries(R.array.safe_class_entries_for_oswego_m);
                listPreference.setEntryValues(R.array.safe_class_entry_values_for_oswego_m);
            }
            int safeClass = Integer.valueOf(mSharedPreference.getString(key,
                    String.valueOf(Constants.GF_SAFE_CLASS_MEDIUM)));
            mGoodixFingerprintManager.setSafeClass(safeClass);
            Log.d(TAG, "onSharedPreferenceChanged " + key + " = " + safeClass);

            mGFDevice.mSafeClass = safeClass;
            updateSafeClassPreference();
            return;
        }

        byte[] byteArray = new byte[TestParamEncoder.TEST_ENCODE_SIZEOF_INT32];
        int offset = 0;
        int keyInt = 0;
        int value = 0;

        if (key.equals(KEY_MAX_FINGERS)) {
            keyInt = TestResultParser.TEST_TOKEN_MAX_FINGERS;
        } else if (key.equals(KEY_MAX_FINGERS_PER_USER)) {
            keyInt = TestResultParser.TEST_TOKEN_MAX_FINGERS_PER_USER;
        } else if (key.equals(KEY_SUPPORT_KEY_MODE)) {
            keyInt = TestResultParser.TEST_TOKEN_SUPPORT_KEY_MODE;
        } else if (key.equals(KEY_SUPPORT_FF_MODE)) {
            keyInt = TestResultParser.TEST_TOKEN_SUPPORT_FF_MODE;
        } else if (key.equals(KEY_SUPPORT_POWER_KEY_FREATURE)) {
            keyInt = TestResultParser.TEST_TOKEN_SUPPORT_POWER_KEY_FEATURE;
        } else if (key.equals(KEY_SUPPORT_POWER_KEY_FREATURE)) {
            keyInt = TestResultParser.TEST_TOKEN_SUPPORT_POWER_KEY_FEATURE;
        } else if (key.equals(KEY_FORBIDDEN_UNTRUSTED_ENROLL)) {
            keyInt = TestResultParser.TEST_TOKEN_FORBIDDEN_UNTRUSTED_ENROLL;
        } else if (key.equals(KEY_FORBIDDEN_ENROLL_DUPLICATE_FINGERS)) {
            keyInt = TestResultParser.TEST_TOKEN_FORBIDDEN_ENROLL_DUPLICATE_FINGERS;
        } else if (key.equals(KEY_SUPPORT_BIO_ASSAY)) {
            keyInt = TestResultParser.TEST_TOKEN_SUPPORT_BIO_ASSAY;
        } else if (key.equals(KEY_SUPPORT_PERFORMANCE_DUMP)) {
            keyInt = TestResultParser.TEST_TOKEN_SUPPORT_PERFORMANCE_DUMP;
        } else if (key.equals(KEY_SUPPORT_NAV_MODE)) {
            keyInt = TestResultParser.TEST_TOKEN_SUPPORT_NAV_MODE;
        }  else if (key.equals(KEY_NAV_DOUBLE_CLICK_TIME)) {
            keyInt = TestResultParser.TEST_TOKEN_NAV_DOUBLE_CLICK_TIME;
        } else if (key.equals(KEY_NAV_LONG_PRESS_TIME)) {
            keyInt = TestResultParser.TEST_TOKEN_NAV_LONG_PRESS_TIME;
        } else if (key.equals(KEY_ENROLLING_MIN_TEMPLATES)) {
            keyInt = TestResultParser.TEST_TOKEN_ENROLLING_MIN_TEMPLATES;
        } else if (key.equals(KEY_VALID_IMAGE_QUALITY_THRESHOLD)) {
            keyInt = TestResultParser.TEST_TOKEN_VALID_IMAGE_QUALITY_THRESHOLD;
        } else if (key.equals(KEY_VALID_IMAGE_AREA_THRESHOLD)) {
            keyInt = TestResultParser.TEST_TOKEN_VALID_IMAGE_AREA_THRESHOLD;
        } else if (key.equals(KEY_DUPLICATE_FINGER_OVERLAY_SCORE)) {
            keyInt = TestResultParser.TEST_TOKEN_DUPLICATE_FINGER_OVERLAY_SCORE;
        } else if (key.equals(KEY_INCREATE_RATE_BETWEEN_STITCH_INFO)) {
            keyInt = TestResultParser.TEST_TOKEN_INCREASE_RATE_BETWEEN_STITCH_INFO;
        } else if (key.equals(KEY_SCREEN_ON_AUTHENTICATE_FAIL_RETRY_COUNT)) {
            keyInt = TestResultParser.TEST_TOKEN_SCREEN_ON_AUTHENTICATE_FAIL_RETRY_COUNT;
        } else if (key.equals(KEY_SCREEN_OFF_AUTHENTICATE_FAIL_RETRY_COUNT)) {
            keyInt = TestResultParser.TEST_TOKEN_SCREEN_OFF_AUTHENTICATE_FAIL_RETRY_COUNT;
        } else if (key.equals(KEY_AUTHENTICATE_ORDER)) {
            keyInt = TestResultParser.TEST_TOKEN_AUTHENTICATE_ORDER;
        } else if (key.equals(KEY_REISSUE_KEY_DOWN_WHEN_ENTRY_FF_MODE)) {
            keyInt = TestResultParser.TEST_TOKEN_REISSUE_KEY_DOWN_WHEN_ENTRY_FF_MODE;
        } else if (key.equals(KEY_REISSUE_KEY_DOWN_WHEN_ENTRY_IMAGE_MODE)) {
            keyInt = TestResultParser.TEST_TOKEN_REISSUE_KEY_DOWN_WHEN_ENTRY_IMAGE_MODE;
        } else if (key.equals(KEY_SUPPORT_SENSOR_BROKEN_CHECK)) {
            keyInt = TestResultParser.TEST_TOKEN_SUPPORT_SENSOR_BROKEN_CHECK;
        } else if (key.equals(KEY_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_SENSOR)) {
            keyInt = TestResultParser.TEST_TOKEN_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_SENSOR;
        } else if (key.equals(KEY_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_STUDY)) {
            keyInt = TestResultParser.TEST_TOKEN_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_STUDY;
        } else if (key.equals(KEY_BAD_POINT_TEST_MAX_FRAME_NUMBER)) {
            keyInt = TestResultParser.TEST_TOKEN_BAD_POINT_TEST_MAX_FRAME_NUMBER;
        } else if (key.equals(KEY_REPORT_KEY_EVENT_ONLY_ENROLL_AUTHENTICATE)) {
            keyInt = TestResultParser.TEST_TOKEN_REPORT_KEY_EVENT_ONLY_ENROLL_AUTHENTICATE;
        } else if (key.equals(KEY_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_IMAGE_MODE)) {
            keyInt = TestResultParser.TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_IMAGE_MODE;
        } else if (key.equals(KEY_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_FF_MODE)) {
            keyInt = TestResultParser.TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_FF_MODE;
        } else if (key.equals(KEY_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_KEY_MODE)) {
            keyInt = TestResultParser.TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_KEY_MODE;
        } else if (key.equals(KEY_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_NAV_MODE)) {
            keyInt = TestResultParser.TEST_TOKEN_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_NAV_MODE;
        } else if(key.equals(KEY_SUPPORT_SET_SPI_SPEED_IN_TEE)) {
            keyInt = TestResultParser.TEST_TOKEN_SUPPORT_SET_SPI_SPEED_IN_TEE;
        }

        if (key.equals(KEY_BAD_POINT_AVG_DIFF_VAL)) {
            short diffVal = mTestResultChecker.getThresHold().avgDiffVal;
            mTestResultChecker.getThresHold().avgDiffVal = Short.parseShort(sharedPreferences.getString(key, "" + diffVal));
        } else if (key.equals(KEY_LOCAL_SMALL_BAD_PIXEL)) {
            int lsBadPixel = mTestResultChecker.getThresHold().localSmallBadPixel;
            mTestResultChecker.getThresHold().localSmallBadPixel = Integer.parseInt(sharedPreferences.getString(key, "" + lsBadPixel));
        } else if (key.equals(KEY_LOCAL_BIG_BAD_PIXEL)) {
            int lbBadPixel = mTestResultChecker.getThresHold().localBigBadPixel;
            mTestResultChecker.getThresHold().localBigBadPixel = Integer.parseInt(sharedPreferences.getString(key, "" + lbBadPixel));
        } else if (key.equals(KEY_BAD_POINT_TOTAL_BAD_PIXEL_NUM)) {
            int badPixNumber = mTestResultChecker.getThresHold().badPixelNum;
            mTestResultChecker.getThresHold().badPixelNum = Integer.parseInt(sharedPreferences.getString(key, "" + badPixNumber));
        } else if (key.equals(KEY_BAD_POINT_LOCAL_BAD_PIXEL_NUM)) {
            int lbPixelNum = mTestResultChecker.getThresHold().localBadPixelNum;
            mTestResultChecker.getThresHold().localBadPixelNum = Integer.parseInt(sharedPreferences.getString(key, "" + lbPixelNum));
        } else if (key.equals(KEY_BAD_POINT_LOCAL_WORST_NUM)) {
            short worst = mTestResultChecker.getThresHold().localWorst;
            mTestResultChecker.getThresHold().localWorst = Short.parseShort(sharedPreferences.getString(key, "" + worst));
        }

        if (findPreference(key) instanceof ListPreference) {
            Log.d(TAG, key + " " + mSharedPreference.getString(key, null));
            value = Integer.valueOf(mSharedPreference.getString(key, null));

            if (key.equals(KEY_NAV_DOUBLE_CLICK_TIME)) {
                if ((0 == mGFConfig.mSupportNavMode) || (mGFConfig.mNavLongPressTime > 0 && value > 0
                        && mGFConfig.mNavLongPressTime <= value)) {
                    return;
                }
            } else if (key.equals(KEY_NAV_LONG_PRESS_TIME)) {
                if ((0 == mGFConfig.mSupportNavMode) || (mGFConfig.mNavDoubleClickTime > 0 && value > 0
                        && mGFConfig.mNavDoubleClickTime >= value)) {
                    return;
                }
            } else if (key.equals(KEY_SUPPORT_NAV_MODE)) {
                if (value > 0) {
                    ListPreference navDoubleClickTimePreference = (ListPreference) findPreference(KEY_NAV_DOUBLE_CLICK_TIME);
                    ListPreference navLongPressTimePreference = (ListPreference) findPreference(KEY_NAV_LONG_PRESS_TIME);
                    navDoubleClickTimePreference.setEnabled(true);
                    navLongPressTimePreference.setEnabled(true);
                }
            }
        } else if (findPreference(key) instanceof SwitchPreference) {
            Log.d(TAG, key + " " + mSharedPreference.getBoolean(key, false));
            value = mSharedPreference.getBoolean(key, false) ? 1 : 0;
        } else {
            value = Integer.valueOf(mSharedPreference.getString(key, null));
        }

        offset = TestParamEncoder.encodeInt32(byteArray, offset, keyInt, value);

        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_SET_CONFIG, byteArray);
    }

    private void updatePreference(int chipSeries) {
        Log.d(TAG, "updatePreference chipSeries = " + chipSeries);
        switch (chipSeries) {
            case Constants.GF_OSWEGO_M: {
                mBadPointCategory.removePreference(mBadPointCategory.findPreference(KEY_BAD_POINT_LOCAL_WORST_NUM));
                mBadPointCategory.removePreference(mBadPointCategory.findPreference(KEY_BAD_POINT_TOTAL_BAD_PIXEL_NUM));
                mBadPointCategory.removePreference(mBadPointCategory.findPreference(KEY_BAD_POINT_LOCAL_BAD_PIXEL_NUM));
                break;
            }
            case Constants.GF_MILAN_A_SERIES:
            case Constants.GF_MILAN_F_SERIES: {
                mBadPointCategory.removePreference(mBadPointCategory.findPreference(KEY_BAD_POINT_AVG_DIFF_VAL));
                mBadPointCategory.removePreference(mBadPointCategory.findPreference(KEY_LOCAL_SMALL_BAD_PIXEL));
                mBadPointCategory.removePreference(mBadPointCategory.findPreference(KEY_LOCAL_BIG_BAD_PIXEL));
                break;
            }

        }
    }

    private void updateSafeClassPreference() {
        Log.d(TAG, "updateSafeClassPreference");
        if (mGFDevice == null) {
            return;
        }

        if (mGFDevice.mSafeClass >= Constants.GF_SAFE_CLASS_MEDIUM) {
            findPreference(KEY_SCREEN_ON_AUTHENTICATE_FAIL_RETRY_COUNT).setEnabled(true);
            findPreference(KEY_SCREEN_OFF_AUTHENTICATE_FAIL_RETRY_COUNT).setEnabled(true);
        } else {
            findPreference(KEY_SCREEN_ON_AUTHENTICATE_FAIL_RETRY_COUNT).setEnabled(false);
            findPreference(KEY_SCREEN_OFF_AUTHENTICATE_FAIL_RETRY_COUNT).setEnabled(false);
        }

        ListPreference listPreference = (ListPreference) findPreference(KEY_SAFE_CLASS);
        if (listPreference == null) {
            return;
        }

        if (Constants.GF_OSWEGO_M == mGFConfig.mChipSeries) {
            listPreference.setEntries(R.array.safe_class_entries_for_oswego_m);
            listPreference.setEntryValues(R.array.safe_class_entry_values_for_oswego_m);
        }

        listPreference.setSummary(
                getResources().getStringArray(R.array.safe_class_summary)[mGFDevice.mSafeClass]);
        listPreference.setValue(String.valueOf(mGFDevice.mSafeClass));
    }

    private void upateSwitchPreference(String preferenceKey, int value) {
        SwitchPreference switchPreference = (SwitchPreference) findPreference(preferenceKey);
        if (switchPreference == null) {
            return;
        }

        switchPreference.setChecked(value > 0);
    }

    private void updateListPreference(int token) {
        ListPreference listPreference = null;

        switch (token) {
            case TestResultParser.TEST_TOKEN_MAX_FINGERS: {
                int min = mGFConfig.mMaxFingersPerUser;
                int max = 32;
                CharSequence[] entries = new CharSequence[max - min + 1];
                for (int i = min; i <= max; i++) {
                    entries[i - min] = String.valueOf(i);
                }

                listPreference = (ListPreference) findPreference(KEY_MAX_FINGERS);
                if (listPreference == null) {
                    break;
                }

                listPreference.setEntries(entries);
                listPreference.setEntryValues(entries);
                listPreference.setSummary(String.valueOf(mGFConfig.mMaxFingers));
                listPreference.setValue(String.valueOf(mGFConfig.mMaxFingers));
                break;
            }

            case TestResultParser.TEST_TOKEN_MAX_FINGERS_PER_USER: {
                int min = 1;
                int max = 10;
                CharSequence[] entries = new CharSequence[max - min + 1];
                for (int i = min; i <= max; i++) {
                    entries[i - min] = String.valueOf(i);
                }

                listPreference = (ListPreference) findPreference(KEY_MAX_FINGERS_PER_USER);
                if (listPreference == null) {
                    break;
                }

                listPreference.setEntries(entries);
                listPreference.setEntryValues(entries);
                listPreference.setSummary(String.valueOf(mGFConfig.mMaxFingersPerUser));
                listPreference.setValue(String.valueOf(mGFConfig.mMaxFingersPerUser));
                break;
            }

            case TestResultParser.TEST_TOKEN_SUPPORT_NAV_MODE: {
                if (Constants.GF_OSWEGO_M == mGFConfig.mChipSeries) {
                    listPreference = (ListPreference) findPreference(KEY_SUPPORT_NAV_MODE);
                    listPreference.setSummary(getResources().getStringArray(
                            R.array.support_nav_mode_entries)[mGFConfig.mSupportNavMode]);
                    listPreference.setValue(String.valueOf(mGFConfig.mSupportNavMode));
                    break;
                } else if (Constants.GF_MILAN_F_SERIES == mGFConfig.mChipSeries) {
                    listPreference = (ListPreference) findPreference(KEY_SUPPORT_NAV_MODE);
                    listPreference.setSummary(getResources().getStringArray(
                            R.array.support_nav_mode_entries_milan_f_series)[mGFConfig.mSupportNavMode]);
                    listPreference.setValue(String.valueOf(mGFConfig.mSupportNavMode));
                    break;
                } else if (Constants.GF_MILAN_A_SERIES == mGFConfig.mChipSeries) {
                    break;
                }
            }

            case TestResultParser.TEST_TOKEN_NAV_DOUBLE_CLICK_TIME: {
                listPreference = (ListPreference) findPreference(KEY_NAV_DOUBLE_CLICK_TIME);
                if (0 == mGFConfig.mNavDoubleClickTime) {
                    listPreference.setSummary(getResources().getString(R.string.disable));
                } else {
                    listPreference.setSummary(String.valueOf(mGFConfig.mNavDoubleClickTime));
                }
                listPreference.setValue(String.valueOf(mGFConfig.mNavDoubleClickTime));
                break;
            }

            case TestResultParser.TEST_TOKEN_NAV_LONG_PRESS_TIME: {
                listPreference = (ListPreference) findPreference(KEY_NAV_LONG_PRESS_TIME);
                if (0 == mGFConfig.mNavLongPressTime) {
                    listPreference.setSummary(getResources().getString(R.string.disable));
                } else {
                    listPreference.setSummary(String.valueOf(mGFConfig.mNavLongPressTime));
                }
                listPreference.setValue(String.valueOf(mGFConfig.mNavLongPressTime));
                break;
            }

            case TestResultParser.TEST_TOKEN_ENROLLING_MIN_TEMPLATES: {

                int min = 8;
                int max = 40;

                if (mGFConfig.mChipType >= Constants.GF_CHIP_UNKNOWN || mGFConfig.mChipType < 0) {
                    min = 8;
                    max = 40;
                    Log.d(TAG, "unknownChip");
                } else {
                    min = Constants.ENROLLING_MIN_TEMPLATES_NUM[mGFConfig.mChipType];
                    max = Constants.MAX_TEMPLATES_NUM[mGFConfig.mChipType];
                }

                CharSequence[] entries = new CharSequence[max - min + 1];
                for (int i = min; i <= max; i++) {
                    entries[i - min] = String.valueOf(i);
                }

                listPreference = (ListPreference) findPreference(KEY_ENROLLING_MIN_TEMPLATES);
                listPreference.setEntries(entries);
                listPreference.setEntryValues(entries);
                listPreference.setSummary(String.valueOf(mGFConfig.mEnrollingMinTemplates));
                listPreference.setValue(String.valueOf(mGFConfig.mEnrollingMinTemplates));
                break;
            }

            case TestResultParser.TEST_TOKEN_VALID_IMAGE_QUALITY_THRESHOLD: {
                listPreference = (ListPreference) findPreference(KEY_VALID_IMAGE_QUALITY_THRESHOLD);
                listPreference.setSummary(String.valueOf(mGFConfig.mValidImageQualityThreshold));
                listPreference.setValue(String.valueOf(mGFConfig.mValidImageQualityThreshold));
                break;
            }

            case TestResultParser.TEST_TOKEN_VALID_IMAGE_AREA_THRESHOLD: {
                listPreference = (ListPreference) findPreference(KEY_VALID_IMAGE_AREA_THRESHOLD);
                listPreference.setSummary(String.valueOf(mGFConfig.mValidImageAreaThreshold));
                listPreference.setValue(String.valueOf(mGFConfig.mValidImageAreaThreshold));
                break;
            }

            case TestResultParser.TEST_TOKEN_DUPLICATE_FINGER_OVERLAY_SCORE: {
                int min = 50;
                int max = 100;
                CharSequence[] entries = new CharSequence[max - min + 1];
                for (int i = min; i <= max; i++) {
                    entries[i - min] = String.valueOf(i);
                }

                listPreference = (ListPreference) findPreference(
                        KEY_DUPLICATE_FINGER_OVERLAY_SCORE);
                listPreference.setEntries(entries);
                listPreference.setEntryValues(entries);
                listPreference.setSummary(String.valueOf(mGFConfig.mDuplicateFingerOverlayScore));
                listPreference.setValue(String.valueOf(mGFConfig.mDuplicateFingerOverlayScore));
                break;
            }

            case TestResultParser.TEST_TOKEN_INCREASE_RATE_BETWEEN_STITCH_INFO: {
                int min = 0;
                int max = 30;
                CharSequence[] entries = new CharSequence[max - min + 1];
                for (int i = min; i <= max; i++) {
                    entries[i - min] = String.valueOf(i);
                }

                listPreference = (ListPreference) findPreference(
                        KEY_INCREATE_RATE_BETWEEN_STITCH_INFO);
                listPreference.setEntries(entries);
                listPreference.setEntryValues(entries);
                listPreference.setSummary(String.valueOf(mGFConfig.mIncreaseRateBetweenStitchInfo));
                listPreference.setValue(String.valueOf(mGFConfig.mIncreaseRateBetweenStitchInfo));
                break;
            }

            case TestResultParser.TEST_TOKEN_SCREEN_ON_AUTHENTICATE_FAIL_RETRY_COUNT: {
                listPreference = (ListPreference) findPreference(
                        KEY_SCREEN_ON_AUTHENTICATE_FAIL_RETRY_COUNT);
                listPreference
                .setSummary(String.valueOf(mGFConfig.mScreenOnAuthenticateFailRetryCount));
                listPreference
                .setValue(String.valueOf(mGFConfig.mScreenOnAuthenticateFailRetryCount));
                break;
            }

            case TestResultParser.TEST_TOKEN_SCREEN_OFF_AUTHENTICATE_FAIL_RETRY_COUNT: {
                listPreference = (ListPreference) findPreference(
                        KEY_SCREEN_OFF_AUTHENTICATE_FAIL_RETRY_COUNT);
                listPreference
                .setSummary(String.valueOf(mGFConfig.mScreenOffAuthenticateFailRetryCount));
                listPreference
                .setValue(String.valueOf(mGFConfig.mScreenOffAuthenticateFailRetryCount));
                break;
            }

            case TestResultParser.TEST_TOKEN_AUTHENTICATE_ORDER: {
                listPreference = (ListPreference) findPreference(KEY_AUTHENTICATE_ORDER);
                listPreference
                .setSummary(getResources()
                        .getStringArray(R.array.authenticate_order_summary)[mGFConfig.mAuthenticateOrder]);
                listPreference
                .setValue(String.valueOf(mGFConfig.mAuthenticateOrder));
                break;
            }

            case TestResultParser.TEST_TOKEN_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_SENSOR: {
                listPreference = (ListPreference) findPreference(
                        KEY_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_SENSOR);
                if (listPreference == null) {
                    break;
                }

                listPreference.setSummary(
                        String.valueOf(mGFConfig.mBrokenPixelThresholdForDisableSensor));
                listPreference
                .setValue(String.valueOf(mGFConfig.mBrokenPixelThresholdForDisableSensor));
                break;
            }

            case TestResultParser.TEST_TOKEN_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_STUDY: {
                listPreference = (ListPreference) findPreference(
                        KEY_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_STUDY);
                if (listPreference == null) {
                    break;
                }

                listPreference
                .setSummary(String.valueOf(mGFConfig.mBrokenPixelThresholdForDisableStudy));
                listPreference
                .setValue(String.valueOf(mGFConfig.mBrokenPixelThresholdForDisableStudy));
                break;
            }

            case TestResultParser.TEST_TOKEN_BAD_POINT_TEST_MAX_FRAME_NUMBER: {
                listPreference = (ListPreference) findPreference(
                        KEY_BAD_POINT_TEST_MAX_FRAME_NUMBER);
                listPreference.setSummary(String.valueOf(mGFConfig.mBadPointTestMaxFrameNumber));
                listPreference.setValue(String.valueOf(mGFConfig.mBadPointTestMaxFrameNumber));
                break;

            }

        }
    }

    private void updateEditPreference(int token) {
        EditTextPreference editPreference = null;
        switch (token) {

            case TestResultParser.TEST_TOKEN_AVG_DIFF_VAL: {
                editPreference = (EditTextPreference) findPreference(KEY_BAD_POINT_AVG_DIFF_VAL);
                String badPointAvgDiffVal = mSharedPreference.getString(KEY_BAD_POINT_AVG_DIFF_VAL, null);
                if (editPreference != null) {
                    if (badPointAvgDiffVal != null && !badPointAvgDiffVal.isEmpty()) {
                        editPreference.setSummary(badPointAvgDiffVal);
                        editPreference.setText(badPointAvgDiffVal);
                    } else {
                        editPreference.setSummary(String.valueOf(mTestResultChecker.getThresHold().avgDiffVal));
                        editPreference.setText(String.valueOf(mTestResultChecker.getThresHold().avgDiffVal));
                    }
                }

                break;
            }
            case TestResultParser.TEST_TOKEN_LOCAL_SMALL_BAD_PIXEL_NUM: {
                editPreference = (EditTextPreference) findPreference(KEY_LOCAL_SMALL_BAD_PIXEL);
                String localSmallBadPixel = mSharedPreference.getString(KEY_LOCAL_SMALL_BAD_PIXEL, null);
                if (editPreference != null) {
                    if (localSmallBadPixel != null && !localSmallBadPixel.isEmpty()) {
                        editPreference.setSummary(localSmallBadPixel);
                        editPreference.setText(localSmallBadPixel);
                    } else {
                        editPreference.setSummary(String.valueOf(mTestResultChecker.getThresHold().localSmallBadPixel));
                        editPreference.setText(String.valueOf(mTestResultChecker.getThresHold().localSmallBadPixel));
                    }
                }

                break;
            }
            case TestResultParser.TEST_TOKEN_LOCAL_BIG_BAD_PIXEL_NUM: {
                editPreference = (EditTextPreference) findPreference(KEY_LOCAL_BIG_BAD_PIXEL);
                String localBigBadPixel = mSharedPreference.getString(KEY_LOCAL_BIG_BAD_PIXEL, null);
                if (editPreference != null) {
                    if (localBigBadPixel != null && !localBigBadPixel.isEmpty()) {
                        editPreference.setSummary(localBigBadPixel);
                        editPreference.setText(localBigBadPixel);
                    } else {
                        editPreference.setSummary(String.valueOf(mTestResultChecker.getThresHold().localBigBadPixel));
                        editPreference.setText(String.valueOf(mTestResultChecker.getThresHold().localBigBadPixel));
                    }
                }

                break;
            }
            case TestResultParser.TEST_TOKEN_BAD_PIXEL_NUM: {
                editPreference = (EditTextPreference) findPreference(KEY_BAD_POINT_TOTAL_BAD_PIXEL_NUM);
                String totalBadPixelNum = mSharedPreference.getString(KEY_BAD_POINT_TOTAL_BAD_PIXEL_NUM, null);
                if (editPreference != null) {
                    if (totalBadPixelNum != null && !totalBadPixelNum.isEmpty()) {
                        editPreference.setSummary(totalBadPixelNum);
                        editPreference.setText(totalBadPixelNum);
                    } else {
                        editPreference.setSummary(String.valueOf(mTestResultChecker.getThresHold().badPixelNum));
                        editPreference.setText(String.valueOf(mTestResultChecker.getThresHold().badPixelNum));
                    }
                }

                break;
            }
            case TestResultParser.TEST_TOKEN_LOCAL_BAD_PIXEL_NUM: {
                editPreference = (EditTextPreference) findPreference(KEY_BAD_POINT_LOCAL_BAD_PIXEL_NUM);
                String localBadPixelNum = mSharedPreference.getString(KEY_BAD_POINT_LOCAL_BAD_PIXEL_NUM, null);
                if (editPreference != null) {
                    if (localBadPixelNum != null && !localBadPixelNum.isEmpty()) {
                        editPreference.setSummary(localBadPixelNum);
                        editPreference.setText(localBadPixelNum);
                    } else {
                        editPreference.setSummary(String.valueOf(mTestResultChecker.getThresHold().localBadPixelNum));
                        editPreference.setText(String.valueOf(mTestResultChecker.getThresHold().localBadPixelNum));
                    }
                }

                break;
            }
            case TestResultParser.TEST_TOKEN_LOCAL_WORST: {
                editPreference = (EditTextPreference) findPreference(KEY_BAD_POINT_LOCAL_WORST_NUM);
                String localWorstNum = mSharedPreference.getString(KEY_BAD_POINT_LOCAL_WORST_NUM, null);
                if (editPreference != null) {
                    if (localWorstNum != null && !localWorstNum.isEmpty()) {
                        editPreference.setSummary(localWorstNum);
                        editPreference.setText(localWorstNum);
                    } else {
                        editPreference.setSummary(String.valueOf(mTestResultChecker.getThresHold().localWorst));
                        editPreference.setText(String.valueOf(mTestResultChecker.getThresHold().localWorst));
                    }
                }

                break;
            }
        }
    }

    private void updateView() {
        updateListPreference(TestResultParser.TEST_TOKEN_MAX_FINGERS);
        updateListPreference(TestResultParser.TEST_TOKEN_MAX_FINGERS_PER_USER);

        upateSwitchPreference(KEY_SUPPORT_KEY_MODE, mGFConfig.mSupportKeyMode);
        upateSwitchPreference(KEY_SUPPORT_FF_MODE, mGFConfig.mSupportFFMode);
        upateSwitchPreference(KEY_SUPPORT_POWER_KEY_FREATURE, mGFConfig.mSupportPowerKeyFeature);
        upateSwitchPreference(KEY_FORBIDDEN_UNTRUSTED_ENROLL, mGFConfig.mForbiddenUntrustedEnroll);
        upateSwitchPreference(KEY_FORBIDDEN_ENROLL_DUPLICATE_FINGERS, mGFConfig.mForbiddenEnrollDuplicateFingers);
        upateSwitchPreference(KEY_SUPPORT_BIO_ASSAY, mGFConfig.mSupportBioAssay);
        upateSwitchPreference(KEY_SUPPORT_PERFORMANCE_DUMP, mGFConfig.mSupportPerformanceDump);

        updateListPreference(TestResultParser.TEST_TOKEN_SUPPORT_NAV_MODE);
        updateListPreference(TestResultParser.TEST_TOKEN_NAV_DOUBLE_CLICK_TIME);
        updateListPreference(TestResultParser.TEST_TOKEN_NAV_LONG_PRESS_TIME);

        if (0 == mGFConfig.mSupportNavMode) {
            ListPreference navDoubleClickTimePreference = (ListPreference) findPreference(KEY_NAV_DOUBLE_CLICK_TIME);
            ListPreference navLongPressTimePreference = (ListPreference) findPreference(KEY_NAV_LONG_PRESS_TIME);
            navDoubleClickTimePreference.setEnabled(false);
            navLongPressTimePreference.setEnabled(false);
        }

        updateListPreference(TestResultParser.TEST_TOKEN_ENROLLING_MIN_TEMPLATES);

        updateListPreference(TestResultParser.TEST_TOKEN_VALID_IMAGE_QUALITY_THRESHOLD);
        updateListPreference(TestResultParser.TEST_TOKEN_VALID_IMAGE_AREA_THRESHOLD);
        updateListPreference(TestResultParser.TEST_TOKEN_DUPLICATE_FINGER_OVERLAY_SCORE);
        updateListPreference(TestResultParser.TEST_TOKEN_INCREASE_RATE_BETWEEN_STITCH_INFO);

        updateListPreference(TestResultParser.TEST_TOKEN_SCREEN_ON_AUTHENTICATE_FAIL_RETRY_COUNT);
        updateListPreference(TestResultParser.TEST_TOKEN_SCREEN_OFF_AUTHENTICATE_FAIL_RETRY_COUNT);

        updateListPreference(TestResultParser.TEST_TOKEN_AUTHENTICATE_ORDER);


        updateEditPreference(TestResultParser.TEST_TOKEN_AVG_DIFF_VAL);
        updateEditPreference(TestResultParser.TEST_TOKEN_LOCAL_SMALL_BAD_PIXEL_NUM);
        updateEditPreference(TestResultParser.TEST_TOKEN_LOCAL_BIG_BAD_PIXEL_NUM);
        updateEditPreference(TestResultParser.TEST_TOKEN_BAD_PIXEL_NUM);
        updateEditPreference(TestResultParser.TEST_TOKEN_LOCAL_BAD_PIXEL_NUM);
        updateEditPreference(TestResultParser.TEST_TOKEN_LOCAL_WORST);

        upateSwitchPreference(KEY_REISSUE_KEY_DOWN_WHEN_ENTRY_FF_MODE,
                mGFConfig.mReissueKeyDownWhenEntryFfMode);
        upateSwitchPreference(KEY_REISSUE_KEY_DOWN_WHEN_ENTRY_IMAGE_MODE,
                mGFConfig.mReissueKeyDownWhenEntryImageMode);

        upateSwitchPreference(KEY_SUPPORT_SENSOR_BROKEN_CHECK,
                mGFConfig.mSupportSensorBrokenCheck);
        updateListPreference(TestResultParser.TEST_TOKEN_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_SENSOR);
        updateListPreference(TestResultParser.TEST_TOKEN_BROKEN_PIXEL_THRESHOLD_FOR_DISABLE_STUDY);

        updateListPreference(TestResultParser.TEST_TOKEN_BAD_POINT_TEST_MAX_FRAME_NUMBER);
        upateSwitchPreference(KEY_REPORT_KEY_EVENT_ONLY_ENROLL_AUTHENTICATE,
                mGFConfig.mReportKeyEventOnlyEnrollAuthenticate);

        upateSwitchPreference(KEY_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_IMAGE_MODE,
                mGFConfig.mRequireDownAndUpInPairsForImageMode);
        upateSwitchPreference(KEY_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_FF_MODE,
                mGFConfig.mRequireDownAndUpInPairsForFFMode);
        upateSwitchPreference(KEY_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_KEY_MODE,
                mGFConfig.mRequireDownAndUpInPairsForKeyMode);
        upateSwitchPreference(KEY_REQUIRE_DOWN_AND_UP_IN_PAIRS_FOR_NAV_MODE,
                mGFConfig.mRequireDownAndUpInPairsForNavMode);

        upateSwitchPreference(KEY_SUPPORT_SET_SPI_SPEED_IN_TEE, mGFConfig.mSupportSetSpiSpeedInTEE);
    }

    private TestCmdCallback mTestCmdCallback = new TestCmdCallback() {

        @Override
        public void onTestCmd(final int cmdId, final HashMap<Integer, Object> result) {
            Log.d(TAG, "onTestCmd");

            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    if (cmdId != Constants.CMD_TEST_SET_CONFIG) {
                        return;
                    }

                    mGFConfig = mGoodixFingerprintManager.getConfig();
                    if (0 == mGFConfig.mSupportNavMode) {
                        mGFConfig.mNavDoubleClickTime = 0;
                        mGFConfig.mNavLongPressTime = 0;
                    }

                    updateView();
                }
            });
        }
    };

}
