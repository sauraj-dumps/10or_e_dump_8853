
package com.goodix.gftest;

import java.text.DecimalFormat;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.GFConfig;
import com.goodix.fingerprint.service.GoodixFingerprintManager;
import com.goodix.fingerprint.service.GoodixFingerprintManager.TestCmdCallback;
import com.goodix.fingerprint.utils.TestResultParser;
import com.goodix.gftest.utils.TestResultChecker;
import com.goodix.gftest.utils.TestResultChecker.Checker;
import com.goodix.gftest.utils.TestResultChecker.TestResultCheckerFactory;
import com.goodix.gftest.utils.TestResultChecker.CheckPoint;

public class BadPointTestActivity extends Activity {

    private static final String TAG = "BadPointTestActivity";

    private TextView mResultView = null;
    private View mResultDetailView = null;

    private View mBadPixelNumLayout = null;
    private TextView mBadPixelNumTitle = null;
    private TextView mBadPixelNumView = null;
    private TextView mBadPixelNumTipsView = null;

    private View mLocalBadPixelNumLayout = null;
    private TextView mLocalBadPixelNumTitle = null;
    private TextView mLocalBadPixelNumView = null;
    private TextView mLocalBadPixelNumTipsView = null;

    private View mAvgDiffValLayout = null;
    private TextView mAvgDiffValTitle = null;
    private TextView mAvgDiffValView = null;
    private TextView mAvgDiffValTipsView = null;

    private View mAllTiltAngleLayout = null;
    private TextView mAllTiltAngleTitle = null;
    private TextView mAllTiltAngleView = null;
    private TextView mAllTiltAngleTipsView = null;

    private View mBlockTiltAngleMaxLayout = null;
    private TextView mBlockTiltAngleMaxTitle = null;
    private TextView mBlockTiltAngleMaxView = null;
    private TextView mBlockTiltAngleMaxTipsView = null;

    private View mLocalWorstLayout = null;
    private TextView mLocalWorstTitle = null;
    private TextView mLocalWorstView = null;
    private TextView mLocalWorstTipsView = null;

    private View mSingularLayout = null;
    private TextView mSingularTitle = null;
    private TextView mSingularView = null;
    private TextView mSingularTipsView = null;

    private View mInCircleLayout = null;
    private TextView mInCircleTitle = null;
    private TextView mInCircleView = null;
    private TextView mInCircleTipsView = null;

    private View mSmallBadPixelLayout = null;
    private TextView mSmallBadPixelTitle = null;
    private TextView mSmallBadPixelView = null;
    private TextView mSmallBadPixelTipsView = null;

    private View mBigBadPixelLayout = null;
    private TextView mBigBadPixelTitle = null;
    private TextView mBigBadPixelView = null;
    private TextView mBigBadPixelTipsView = null;

    private DecimalFormat mDecimalFormat = new DecimalFormat("0.000");

    private GoodixFingerprintManager mGoodixFingerprintManager = null;
    private Handler mHandler = new Handler();
    private boolean mIsCanceled = false;
    private GFConfig mConfig = null;
    private Checker mTestResultChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bad_point_test);

        mGoodixFingerprintManager = new GoodixFingerprintManager(BadPointTestActivity.this);
        mConfig = mGoodixFingerprintManager.getConfig();
        mTestResultChecker = TestResultCheckerFactory.getInstance(mConfig.mChipType, mConfig.mChipSeries);

        initView();
    }

    public void initView() {
        setContentView(R.layout.activity_bad_point_test);
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        mResultView = (TextView) findViewById(R.id.test_result);
        mResultView.setVisibility(View.VISIBLE);
        mResultDetailView = findViewById(R.id.test_result_detail);
        mResultDetailView.setVisibility(View.GONE);

        mBadPixelNumLayout = findViewById(R.id.bad_pixel_num_layout);
        mBadPixelNumTitle = (TextView) mBadPixelNumLayout.findViewById(R.id.title);
        mBadPixelNumView = (TextView) mBadPixelNumLayout.findViewById(R.id.result);
        mBadPixelNumTipsView = (TextView) mBadPixelNumLayout.findViewById(R.id.success_tips);

        mLocalBadPixelNumLayout = findViewById(R.id.local_bad_pixel_num_layout);
        mLocalBadPixelNumTitle = (TextView) mLocalBadPixelNumLayout.findViewById(R.id.title);
        mLocalBadPixelNumView = (TextView) mLocalBadPixelNumLayout.findViewById(R.id.result);
        mLocalBadPixelNumTipsView = (TextView) mLocalBadPixelNumLayout
                .findViewById(R.id.success_tips);

        mAvgDiffValLayout = findViewById(R.id.avg_diff_val_layout);
        mAvgDiffValTitle = (TextView) mAvgDiffValLayout.findViewById(R.id.title);
        mAvgDiffValView = (TextView) mAvgDiffValLayout.findViewById(R.id.result);
        mAvgDiffValTipsView = (TextView) mAvgDiffValLayout.findViewById(R.id.success_tips);

        mAllTiltAngleLayout = findViewById(R.id.all_tilt_angle_layout);
        mAllTiltAngleTitle = (TextView) mAllTiltAngleLayout.findViewById(R.id.title);
        mAllTiltAngleView = (TextView) mAllTiltAngleLayout.findViewById(R.id.result);
        mAllTiltAngleTipsView = (TextView) mAllTiltAngleLayout.findViewById(R.id.success_tips);

        mBlockTiltAngleMaxLayout = findViewById(R.id.block_tilt_angle_max_layout);
        mBlockTiltAngleMaxTitle = (TextView) mBlockTiltAngleMaxLayout.findViewById(R.id.title);
        mBlockTiltAngleMaxView = (TextView) mBlockTiltAngleMaxLayout.findViewById(R.id.result);
        mBlockTiltAngleMaxTipsView = (TextView) mBlockTiltAngleMaxLayout
                .findViewById(R.id.success_tips);

        mLocalWorstLayout = findViewById(R.id.local_worst_layout);
        mLocalWorstTitle = (TextView) mLocalWorstLayout.findViewById(R.id.title);
        mLocalWorstView = (TextView) mLocalWorstLayout.findViewById(R.id.result);
        mLocalWorstTipsView = (TextView) mLocalWorstLayout.findViewById(R.id.success_tips);

        mSingularLayout = findViewById(R.id.singular_layout);
        mSingularTitle = (TextView) mSingularLayout.findViewById(R.id.title);
        mSingularView = (TextView) mSingularLayout.findViewById(R.id.result);
        mSingularTipsView = (TextView) mSingularLayout.findViewById(R.id.success_tips);

        mInCircleLayout = findViewById(R.id.incircle_layout);
        mInCircleTitle = (TextView) mInCircleLayout.findViewById(R.id.title);
        mInCircleView = (TextView) mInCircleLayout.findViewById(R.id.result);
        mInCircleTipsView = (TextView) mInCircleLayout.findViewById(R.id.success_tips);

        mSmallBadPixelLayout = findViewById(R.id.local_small_bad_pixel_layout);
        mSmallBadPixelTitle = (TextView) mSmallBadPixelLayout.findViewById(R.id.title);
        mSmallBadPixelView = (TextView) mSmallBadPixelLayout.findViewById(R.id.result);
        mSmallBadPixelTipsView = (TextView) mSmallBadPixelLayout.findViewById(R.id.success_tips);

        mBigBadPixelLayout = findViewById(R.id.local_big_bad_pixel_layout);
        mBigBadPixelTitle = (TextView) mBigBadPixelLayout.findViewById(R.id.title);
        mBigBadPixelView = (TextView) mBigBadPixelLayout.findViewById(R.id.result);
        mBigBadPixelTipsView = (TextView) mBigBadPixelLayout.findViewById(R.id.success_tips);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoodixFingerprintManager.registerTestCmdCallback(mTestCmdCallback);
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_BAD_POINT, null);

        mIsCanceled = false;
        mHandler.postDelayed(mTimeoutRunnable, Constants.TEST_TIMEOUT_MS);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mHandler.removeCallbacks(mTimeoutRunnable);
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);
        mGoodixFingerprintManager.unregisterTestCmdCallback(mTestCmdCallback);

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Runnable mTimeoutRunnable = new Runnable() {

        @Override
        public void run() {
            mIsCanceled = true;
            mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);

            showTimeoutUI();
        }

    };

    private void showTimeoutUI() {
        mResultView.setVisibility(View.VISIBLE);
        mResultView.setText(R.string.timeout);
        mResultView.setTextColor(getResources().getColor(R.color.test_failed_color));
        AlertDialog timeoutDialog = new AlertDialog.Builder(this)
                .setMessage(BadPointTestActivity.this.getString(R.string.timeout_before_press,
                        Constants.TEST_TIMEOUT_MS / 1000))
                .setTitle(BadPointTestActivity.this.getString(R.string.sytem_info))
                .setPositiveButton(BadPointTestActivity.this.getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                BadPointTestActivity.this.finish();
                            }
                        })
                .create();
        timeoutDialog.setCancelable(false);
        timeoutDialog.show();
    }

    private void onTestBadPoint(final HashMap<Integer, Object> result) {

        for (Object key : result.keySet()) {
            Log.d(TAG, key + " : " + result.get(key));
        }

        TestResultChecker.Threshold threshold = mTestResultChecker.getThresHold();
        if (threshold == null) {
            Log.e(TAG, "failed to get threshold");
            return;
        }

        CheckPoint checkPoint = new CheckPoint();
        checkPoint.mErrorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
        checkPoint.mAvgDiffVal = 0;
        checkPoint.mBadPixelNum = 0;
        checkPoint.mLocalBadPixelNum = 0;
        checkPoint.mAllTiltAngle = 0;
        checkPoint.mBlockTiltAngleMax = 0;
        checkPoint.mLocalWorst = 0;
        checkPoint.mSingular = 0;
        checkPoint.mInCircle = 0;
        checkPoint.mSmallBadPixel = 0;
        checkPoint.mBigBadPixel = 0;

        mResultDetailView.setVisibility(View.VISIBLE);

        // bad pixelNum layout
        if (result.containsKey(TestResultParser.TEST_TOKEN_BAD_PIXEL_NUM)) {
            checkPoint.mBadPixelNum = (Integer) result.get(TestResultParser.TEST_TOKEN_BAD_PIXEL_NUM);
            mBadPixelNumLayout.setVisibility(View.VISIBLE);

            mBadPixelNumTitle.setText(R.string.bad_pixel_num);
            mBadPixelNumView.setText(String.valueOf(checkPoint.mBadPixelNum));
            mBadPixelNumTipsView.setText(getResources().getString(
                    R.string.test_success_tips, " < ", threshold.badPixelNum));

            if (checkPoint.mBadPixelNum < threshold.badPixelNum) {
                mBadPixelNumView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                mBadPixelNumView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }
        } else {
            mBadPixelNumLayout.setVisibility(View.GONE);
        }

        // local badPixelNum layout
        if (result.containsKey(TestResultParser.TEST_TOKEN_LOCAL_BAD_PIXEL_NUM)) {
            checkPoint.mLocalBadPixelNum = (Integer) result
                    .get(TestResultParser.TEST_TOKEN_LOCAL_BAD_PIXEL_NUM);
            mLocalBadPixelNumLayout.setVisibility(View.VISIBLE);

            mLocalBadPixelNumTitle.setText(R.string.local_bad_pixel);
            mLocalBadPixelNumView.setText(String.valueOf(checkPoint.mLocalBadPixelNum));
            mLocalBadPixelNumTipsView.setText(getResources().getString(
                    R.string.test_success_tips, " < ", threshold.localBadPixelNum));

            if (checkPoint.mLocalBadPixelNum < threshold.localBadPixelNum) {
                mLocalBadPixelNumView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                mLocalBadPixelNumView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }
        } else {
            mLocalBadPixelNumLayout.setVisibility(View.GONE);
        }

        // avg diffVal Layout
        if (result.containsKey(TestResultParser.TEST_TOKEN_AVG_DIFF_VAL)
                && threshold.avgDiffVal != 0) {
            checkPoint.mAvgDiffVal = (Short) result
                    .get(TestResultParser.TEST_TOKEN_AVG_DIFF_VAL);
            mAvgDiffValLayout.setVisibility(View.VISIBLE);

            if (checkPoint.mAvgDiffVal > threshold.avgDiffVal) {
                mAvgDiffValView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                mAvgDiffValView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

            mAvgDiffValTitle.setText(R.string.avg_diff_val);
            mAvgDiffValView.setText(String.valueOf(checkPoint.mAvgDiffVal));
            mAvgDiffValTipsView.setText(getResources().getString(R.string.test_success_tips,
                    " > ", threshold.avgDiffVal));

        } else {
            mAvgDiffValLayout.setVisibility(View.GONE);
        }

        // allTiltAngle Layout
        if (result.containsKey(TestResultParser.TEST_TOKEN_ALL_TILT_ANGLE)
                && threshold.allTiltAngle != 0) {
            checkPoint.mAllTiltAngle = (Float) result
                    .get(TestResultParser.TEST_TOKEN_ALL_TILT_ANGLE);
            mAllTiltAngleLayout.setVisibility(View.VISIBLE);

            if (checkPoint.mAllTiltAngle < threshold.allTiltAngle) {
                mAllTiltAngleView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                mAllTiltAngleView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }
            mAllTiltAngleTitle.setText(R.string.all_tilt_angle);
            mAllTiltAngleTipsView.setText(getResources().getString(
                    R.string.test_success_tips_float, " < ",
                    mDecimalFormat.format(threshold.allTiltAngle)));

            mAllTiltAngleView.setText(mDecimalFormat.format(checkPoint.mAllTiltAngle));

        } else {
            mAllTiltAngleLayout.setVisibility(View.GONE);
        }

        // blockTitlAngleMax Layout
        if (result.containsKey(TestResultParser.TEST_TOKEN_BLOCK_TILT_ANGLE_MAX)
                && threshold.blockTiltAngleMax != 0) {
            checkPoint.mBlockTiltAngleMax = (Float) result
                    .get(TestResultParser.TEST_TOKEN_BLOCK_TILT_ANGLE_MAX);
            mBlockTiltAngleMaxLayout.setVisibility(View.VISIBLE);

            mBlockTiltAngleMaxTitle.setText(R.string.block_tilt_angle_max);
            mBlockTiltAngleMaxTipsView.setText(getResources().getString(
                    R.string.test_success_tips_float, " < ",
                    mDecimalFormat.format(threshold.blockTiltAngleMax)));

            mBlockTiltAngleMaxView.setText(mDecimalFormat.format(checkPoint.mBlockTiltAngleMax));
            if (checkPoint.mBlockTiltAngleMax < Constants.Oswego.TEST_BAD_POINT_BLOCK_TILT_ANGLE_MAX) {
                mBlockTiltAngleMaxView.setTextColor(getResources()
                        .getColor(R.color.test_succeed_color));
            } else {
                mBlockTiltAngleMaxView.setTextColor(getResources()
                        .getColor(R.color.test_failed_color));
            }

        } else {
            mBlockTiltAngleMaxLayout.setVisibility(View.GONE);
        }

        // localWorstLayout
        if (result.containsKey(TestResultParser.TEST_TOKEN_LOCAL_WORST)
                && threshold.localWorst != 0) {
            checkPoint.mLocalWorst = (Short) result.get(TestResultParser.TEST_TOKEN_LOCAL_WORST);
            mLocalWorstLayout.setVisibility(View.VISIBLE);

            // localWorst
            mLocalWorstTitle.setText(R.string.local_worst);
            mLocalWorstView.setText(String.valueOf(checkPoint.mLocalWorst));
            mLocalWorstTipsView.setText(getResources().getString(R.string.test_success_tips,
                    " < ", threshold.localWorst));
            if (checkPoint.mLocalWorst < threshold.localWorst) {
                mLocalWorstView.setTextColor(getResources().getColor(
                        R.color.test_succeed_color));
            } else {
                mLocalWorstView.setTextColor(getResources().getColor(
                        R.color.test_failed_color));
            }
        } else {
            mLocalWorstLayout.setVisibility(View.GONE);
        }

        // singularLayout
        if (result.containsKey(TestResultParser.TEST_TOKEN_SINGULAR) && threshold.singular != 0) {
            checkPoint.mSingular = (Integer) result.get(TestResultParser.TEST_TOKEN_SINGULAR);
            mSingularLayout.setVisibility(View.VISIBLE);

            mSingularTitle.setText(R.string.singular);
            mSingularView.setText(String.valueOf(checkPoint.mSingular));
            mSingularTipsView.setText(getResources().getString(R.string.test_success_tips, " < ",
                    threshold.singular));
            if (checkPoint.mSingular < threshold.singular) {
                mSingularView.setTextColor(getResources().getColor(
                        R.color.test_succeed_color));
            } else {
                mSingularView.setTextColor(getResources().getColor(
                        R.color.test_failed_color));
            }
        } else {
            mSingularLayout.setVisibility(View.GONE);
        }

        // inCircle Layout
        if (result.containsKey(TestResultParser.TEST_TOKEN_IN_CIRCLE) && threshold.inCircle != 0) {
            checkPoint.mInCircle = (Short) result.get(TestResultParser.TEST_TOKEN_IN_CIRCLE);
            mInCircleLayout.setVisibility(View.VISIBLE);

            mInCircleTitle.setText(R.string.incircle);
            mInCircleView.setText(String.valueOf(checkPoint.mInCircle));
            mInCircleTipsView.setText(getResources().getString(R.string.test_success_tips, " < ",
                    threshold.inCircle));
            if (checkPoint.mInCircle < threshold.inCircle) {
                mInCircleView.setTextColor(getResources().getColor(
                        R.color.test_succeed_color));
            } else {
                mInCircleView.setTextColor(getResources().getColor(
                        R.color.test_failed_color));
            }
        } else {
            mInCircleLayout.setVisibility(View.GONE);
        }

        // small bad pixel Layout
        if (result.containsKey(TestResultParser.TEST_TOKEN_LOCAL_SMALL_BAD_PIXEL_NUM) && threshold.localSmallBadPixel != 0) {
            checkPoint.mSmallBadPixel = (Integer) result.get(TestResultParser.TEST_TOKEN_LOCAL_SMALL_BAD_PIXEL_NUM);
            mSmallBadPixelLayout.setVisibility(View.VISIBLE);

            mSmallBadPixelTitle.setText(R.string.local_small_bad_pixel);
            mSmallBadPixelView.setText(String.valueOf(checkPoint.mSmallBadPixel));
            mSmallBadPixelTipsView.setText(getResources().getString(R.string.test_success_tips, " < ",
                    threshold.localSmallBadPixel));
            if (checkPoint.mSmallBadPixel < threshold.localSmallBadPixel) {
                mSmallBadPixelView.setTextColor(getResources().getColor(
                        R.color.test_succeed_color));
            } else {
                mSmallBadPixelView.setTextColor(getResources().getColor(
                        R.color.test_failed_color));
            }
        } else {
            mSmallBadPixelLayout.setVisibility(View.GONE);
        }

        // big bad pixel Layout
        if (result.containsKey(TestResultParser.TEST_TOKEN_LOCAL_BIG_BAD_PIXEL_NUM) && threshold.localBigBadPixel != 0) {
            checkPoint.mBigBadPixel = (Integer) result.get(TestResultParser.TEST_TOKEN_LOCAL_BIG_BAD_PIXEL_NUM);
            mBigBadPixelLayout.setVisibility(View.VISIBLE);

            mBigBadPixelTitle.setText(R.string.local_big_bad_pixel);
            mBigBadPixelView.setText(String.valueOf(checkPoint.mBigBadPixel));
            mBigBadPixelTipsView.setText(getResources().getString(R.string.test_success_tips, " < ",
                    threshold.localBigBadPixel));
            if (checkPoint.mBigBadPixel < threshold.localBigBadPixel) {
                mBigBadPixelView.setTextColor(getResources().getColor(
                        R.color.test_succeed_color));
            } else {
                mBigBadPixelView.setTextColor(getResources().getColor(
                        R.color.test_failed_color));
            }
        } else {
            mBigBadPixelLayout.setVisibility(View.GONE);
        }

        // error code
        if (result.containsKey(TestResultParser.TEST_TOKEN_ERROR_CODE)) {
            checkPoint.mErrorCode = (Integer) result
                    .get(TestResultParser.TEST_TOKEN_ERROR_CODE);
        }

        if (mTestResultChecker.checkBadPointTestResult(checkPoint)) {
            mResultView.setText(R.string.test_succeed);
            mResultView.setTextColor(getResources().getColor(
                    R.color.test_succeed_color));
        } else {
            mResultView.setText(R.string.test_failed);
            mResultView.setTextColor(getResources().getColor(
                    R.color.test_failed_color));
        }
    }

    private TestCmdCallback mTestCmdCallback = new TestCmdCallback() {
        @Override
        public void onTestCmd(int cmdId, final HashMap<Integer, Object> result) {
            Log.d(TAG, "onTestCmd cmdId = " + cmdId);

            if (result == null || cmdId != Constants.CMD_TEST_BAD_POINT) {
                return;
            }

            mHandler.removeCallbacks(mTimeoutRunnable);
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    if (mIsCanceled) {
                        return;
                    }

                    onTestBadPoint(result);

                }

            });

        }
    };
}
