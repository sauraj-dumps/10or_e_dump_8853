
package com.goodix.gftest;

import java.util.ArrayList;

import com.goodix.fingerprint.Constants;
import com.goodix.gftest.service.DumpService;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.TextView;

public class DumpActivity extends Activity implements OnClickListener {

    private static final String TAG = "DumpActivity";

    public static final String KEY_DUMP_PATH = "dump_path";
    private View mDumpTemplatesDivider = null;
    private View mDumpTemplates = null;
    private View mDumpPathDivider = null;
    private View mDumpPath = null;
    private TextView mDumpPathDetail = null;
    private SharedPreferences mSharedPreferences = null;
    private View mDumpBaseFrame = null;
    private View mDumpBaseFrameDivider = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dump);

        mSharedPreferences = getSharedPreferences(DumpActivity.this.getPackageName(),
                Context.MODE_PRIVATE);

        InitView();
    }

    public void InitView() {
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        Switch dumpData = (Switch) findViewById(R.id.dump_data);
        mDumpTemplatesDivider = findViewById(R.id.dump_templates_divider);
        mDumpTemplates = findViewById(R.id.dump_templates);
        mDumpPathDivider = findViewById(R.id.dump_path_divider);
        mDumpPath = findViewById(R.id.dump_path);
        mDumpPathDetail = (TextView) findViewById(R.id.dump_path_detail);
        mDumpBaseFrame = findViewById(R.id.dump_base_frame);
        mDumpBaseFrameDivider = findViewById(R.id.dump_base_frame_divider);

        // if the service is running, the switch should keep checked
        if (isServiceRunning(getApplicationContext(), DumpService.class.getName().toString())) {
            dumpData.setChecked(true);
            onDumpCheckedChanged(true);
        } else {
            dumpData.setChecked(false);
            onDumpCheckedChanged(false);
        }

        final int index = mSharedPreferences.getInt(KEY_DUMP_PATH, Constants.DUMP_PATH_SDCARD);
        String[] dumpPathArray = getResources().getStringArray(R.array.dump_path_array);
        mDumpPathDetail.setText(dumpPathArray[index]);

        dumpData.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Intent intent = new Intent(DumpActivity.this, DumpService.class);
                    intent.putExtra(KEY_DUMP_PATH, index);
                    intent.setAction(DumpService.ACTION_DUMP_DATA);
                    startService(intent);
                } else {
                    Intent intent = new Intent(DumpActivity.this, DumpService.class);
                    stopService(intent);

                }
                onDumpCheckedChanged(isChecked);
            }
        });

        mDumpTemplates.setOnClickListener(DumpActivity.this);
        mDumpBaseFrame.setOnClickListener(DumpActivity.this);
        mDumpPath.setOnClickListener(DumpActivity.this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dump_templates: {
                Intent intent = new Intent(DumpActivity.this, DumpService.class);
                intent.setAction(DumpService.ACTION_DUMP_TEMPLATES);
                startService(intent);
                break;
            }

            case R.id.dump_path: {
                int index = mSharedPreferences.getInt(KEY_DUMP_PATH, Constants.DUMP_PATH_SDCARD);
                Dialog dialog = new AlertDialog.Builder(DumpActivity.this)
                        .setTitle(R.string.dump_path)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }

                        })
                        .setSingleChoiceItems(R.array.dump_path_array, index,
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        String[] dumpPathArray = getResources()
                                                .getStringArray(R.array.dump_path_array);
                                        mDumpPathDetail.setText(dumpPathArray[which]);
                                        mSharedPreferences.edit().putInt(KEY_DUMP_PATH, which)
                                                .commit();

                                        Intent intent = new Intent(DumpActivity.this,
                                                DumpService.class);
                                        intent.setAction(DumpService.ACTION_DUMP_PATH);
                                        intent.putExtra(KEY_DUMP_PATH, which);
                                        startService(intent);
                                    }

                                })
                        .create();
                dialog.show();
                break;
            }

            case R.id.dump_base_frame: {
                Intent intent = new Intent(DumpActivity.this, DumpService.class);
                intent.setAction(DumpService.ACTION_DUMP_BASE_FRAME);
                startService(intent);
                break;
            }
        }
    }

    private void onDumpCheckedChanged(boolean isChecked) {
        mDumpTemplates.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        mDumpTemplatesDivider.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        mDumpPath.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        mDumpPathDivider.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        mDumpBaseFrame.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        mDumpBaseFrameDivider.setVisibility(isChecked ? View.VISIBLE : View.GONE);
    }

    /**
     * Determining whether a Service is running.
     *
     * @param context the Context which contain the input Service
     * @param serviceName the Service to be judged
     * @return if it's true, the service is running. else, the service is closed.
     */
    private boolean isServiceRunning(Context context, String serviceName) {
        if (!TextUtils.isEmpty(serviceName) && context != null) {
            ActivityManager activityManager = (ActivityManager) context
                    .getSystemService(Context.ACTIVITY_SERVICE);
            ArrayList<RunningServiceInfo> runningServiceInfoList = (ArrayList<RunningServiceInfo>) activityManager
                    .getRunningServices(Integer.MAX_VALUE);
            for (RunningServiceInfo serviceInfo : runningServiceInfoList) {
                if (serviceName.equals(serviceInfo.service.getClassName().toString())) {
                    return true;
                }
            }
        }
        return false;
    }

}
