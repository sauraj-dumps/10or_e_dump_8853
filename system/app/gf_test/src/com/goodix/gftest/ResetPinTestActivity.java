
package com.goodix.gftest;

import java.util.HashMap;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.goodix.fingerprint.Constants;
import com.goodix.fingerprint.service.GoodixFingerprintManager;
import com.goodix.fingerprint.service.GoodixFingerprintManager.TestCmdCallback;
import com.goodix.fingerprint.utils.TestResultParser;

public class ResetPinTestActivity extends Activity {

    private static final String TAG = "ResetPinTestActivity";

    private TextView mResultView = null;
    private ProgressBar mProgressBar = null;

    private GoodixFingerprintManager mGoodixFingerprintManager = null;
    private Handler mHandler = new Handler();
    private boolean mIsCanceled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pin_test);

        mGoodixFingerprintManager = new GoodixFingerprintManager(ResetPinTestActivity.this);

        initView();
    }

    public void initView() {
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        mResultView = (TextView) findViewById(R.id.test_result);

        mProgressBar = (ProgressBar) findViewById(R.id.testing);
        mResultView.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mGoodixFingerprintManager.registerTestCmdCallback(mTestCmdCallback);
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_RESET_PIN, null);

        mIsCanceled = false;
        mHandler.postDelayed(mTimeoutRunnable, Constants.TEST_TIMEOUT_MS);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mHandler.removeCallbacks(mTimeoutRunnable);
        mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);
        mGoodixFingerprintManager.unregisterTestCmdCallback(mTestCmdCallback);

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showTimeOutUI() {
        mResultView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);
        mResultView.setText(R.string.timeout);
        mResultView
                .setTextColor(getResources().getColor(R.color.test_failed_color));
    }

    private Runnable mTimeoutRunnable = new Runnable() {

        @Override
        public void run() {
            mIsCanceled = true;
            mGoodixFingerprintManager.testCmd(Constants.CMD_TEST_CANCEL, null);
            showTimeOutUI();
            Log.e(TAG, "time out,reset pin test is canceled.");
        }

    };

    private TestCmdCallback mTestCmdCallback = new TestCmdCallback() {
        @Override
        public void onTestCmd(int cmdId, final HashMap<Integer, Object> result) {
            Log.d(TAG, "onTestCmd cmdId = " + cmdId);

            if (result == null || cmdId != Constants.CMD_TEST_RESET_PIN) {
                return;
            }

            mHandler.removeCallbacks(mTimeoutRunnable);
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    if (mIsCanceled) {
                        return;
                    }

                    mResultView.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.INVISIBLE);

                    int errorCode = Constants.FINGERPRINT_ERROR_VENDOR_BASE;
                    if (result.containsKey(TestResultParser.TEST_TOKEN_ERROR_CODE)) {
                        errorCode = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_ERROR_CODE);
                    }
                    int resetFlag = 0;
                    if (result.containsKey(TestResultParser.TEST_TOKEN_RESET_FLAG)) {
                        resetFlag = (Integer) result
                                .get(TestResultParser.TEST_TOKEN_RESET_FLAG);
                    }

                    if (errorCode == 0 && resetFlag > 0) {
                        mResultView.setText(R.string.test_succeed);
                        mResultView.setTextColor(getResources()
                                .getColor(R.color.test_succeed_color));
                    } else {
                        mResultView.setText(R.string.test_failed);
                        mResultView
                                .setTextColor(getResources().getColor(R.color.test_failed_color));
                    }
                }

            });

        }
    };
}
