#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:30041388:44d8f17541aad548fe1388b0522ce7f116a8ddd1; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:28044584:22088cbd8808a13cec0418ac347932d4066efc34 EMMC:/dev/block/bootdevice/by-name/recovery 44d8f17541aad548fe1388b0522ce7f116a8ddd1 30041388 22088cbd8808a13cec0418ac347932d4066efc34:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
