#!/bin/bash

cat system/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> system/priv-app/Velvet/Velvet.apk
rm -f system/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat system/priv-app/Velvet/oat/arm64/Velvet.odex.* 2>/dev/null >> system/priv-app/Velvet/oat/arm64/Velvet.odex
rm -f system/priv-app/Velvet/oat/arm64/Velvet.odex.* 2>/dev/null
cat system/app/Kindle-kfa-release/Kindle-kfa-release.apk.* 2>/dev/null >> system/app/Kindle-kfa-release/Kindle-kfa-release.apk
rm -f system/app/Kindle-kfa-release/Kindle-kfa-release.apk.* 2>/dev/null
cat system/app/Kindle-kfa-release/oat/arm/Kindle-kfa-release.odex.* 2>/dev/null >> system/app/Kindle-kfa-release/oat/arm/Kindle-kfa-release.odex
rm -f system/app/Kindle-kfa-release/oat/arm/Kindle-kfa-release.odex.* 2>/dev/null
cat system/app/YouTube/oat/arm/YouTube.odex.* 2>/dev/null >> system/app/YouTube/oat/arm/YouTube.odex
rm -f system/app/YouTube/oat/arm/YouTube.odex.* 2>/dev/null
cat system/app/YouTube/oat/arm64/YouTube.odex.* 2>/dev/null >> system/app/YouTube/oat/arm64/YouTube.odex
rm -f system/app/YouTube/oat/arm64/YouTube.odex.* 2>/dev/null
cat system/app/Chrome/Chrome.apk.* 2>/dev/null >> system/app/Chrome/Chrome.apk
rm -f system/app/Chrome/Chrome.apk.* 2>/dev/null
cat system/app/Gmail2/oat/arm64/Gmail2.odex.* 2>/dev/null >> system/app/Gmail2/oat/arm64/Gmail2.odex
rm -f system/app/Gmail2/oat/arm64/Gmail2.odex.* 2>/dev/null
cat system/app/Photos/oat/arm64/Photos.odex.* 2>/dev/null >> system/app/Photos/oat/arm64/Photos.odex
rm -f system/app/Photos/oat/arm64/Photos.odex.* 2>/dev/null
cat system/app/Maps/oat/arm64/Maps.odex.* 2>/dev/null >> system/app/Maps/oat/arm64/Maps.odex
rm -f system/app/Maps/oat/arm64/Maps.odex.* 2>/dev/null
